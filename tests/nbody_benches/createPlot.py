from operator import itemgetter
import matplotlib.pyplot as plt
timeDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

max_speedup = 1
entries = []
#min_speedup = 1
with open("log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l.rstrip()            
        else:
            if (name in timeDict):
                ourTime = float(l)
                reticTime = timeDict[name]
                #print("{0} & {1} & {2} & {3}\\\ ".format(name, reticTime, ourTime, reticTime/ourTime))
                #if (reticTime / ourTime) > max_speedup:
                #    max_speedup = reticTime / ourTime
                entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime, reticTime/ourTime))
                #if timeDict[name] >= float(l):
                #    fasterCount += 1
                #    noRepeat = False
            else:                
                timeDict[name] = float(l)
                if (noRepeat):
                    totalCount += 1

#current_improvement = .1
times = sorted(entries, key=itemgetter(3))
max_speedup = times[-1][4]
speedup_dict = {}
percentage_list = []
for i in range(1, int(max_speedup)):
    faster_list = list(filter(lambda x: x[4] >= i, times))
    speedup_dict[i] = faster_list
    percentage_list.append((len(faster_list) / len(times)) * 100)

#fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True)
fR,ax = plt.subplots()#list(range(1,int(max_speedup))), percentage_list, 'b-', label="speedup")
ax.fill_between(list(range(1,int(max_speedup))), percentage_list)


#plt.axis([1, int(max_speedup), 0, 100])
#plt.axes().yaxis.grid(True)
#font = {'family' : 'normal',
#                'weight' : 'bold',
#                'size'   : 22}

#plt.rc('font', **font)
plt.show()


