from retic.runtime import *
from retic.guarded import *
from retic.typing import *
import random
import time
from array import array
import math
import perf
from six.moves import xrange
log2Calls = 0

def int_log2(n):
    global log2Calls
    log2Calls = (log2Calls + 1)
    k = retic_cast(1, Int, Dyn, '\nfft.py:15:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    log = retic_cast(0, Int, Dyn, '\nfft.py:16:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    while (k < (n + 0)):
        k = (k * 2)
        log = (log + 1)
    if (n != (1 << log)):
        raise retic_cast(Exception, Dyn, Function(AnonymousParameters([InfoTop]), Dyn), "\nfft.py:21:14: Expected function of type Function(['InfoTop'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(('FFT: Data length is not a power of 2: %s' % n))
    return retic_cast(log, Dyn, Int, '\nfft.py:22:4: A return value of type Int was expected but a value %s was returned instead. (code RETURN_ERROR)')
int_log2 = int_log2

def int_log2_fast(n):
    global log2Calls
    log2Calls = (log2Calls + 1)
    k = 1
    log = 0
    while (k < (n + 0)):
        k = (k * 2)
        log = (log + 1)
    if (n != (1 << log)):
        raise retic_cast(Exception, Dyn, Function(AnonymousParameters([InfoTop]), Dyn), "\nfft.py:21:14: Expected function of type Function(['InfoTop'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(('FFT: Data length is not a power of 2: %s' % n))
    return log
int_log2_fast = int_log2_fast

def FFT_num_flops(N):
    return ((((5.0 * N) - 2) * int_log2(retic_cast(N, Dyn, Int, '\nfft.py:27:27: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)'))) + (2 * (N + 1)))
FFT_num_flops = FFT_num_flops

def FFT_num_flops_fast(N):
    return retic_cast(((((5.0 * N) - 2) * int_log2_fast(N)) + (2 * (N + 1))), Float, Dyn, '\nfft.py:27:4: A return value of type TypeVar(217) was expected but a value %s was returned instead. (code RETURN_ERROR)')
FFT_num_flops_fast = FFT_num_flops_fast

def FFT_transform_internal(N, data, direction):
    n = (N // 2)
    bit = retic_cast(0, Int, Dyn, '\nfft.py:34:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    dual = retic_cast(1, Int, Dyn, '\nfft.py:35:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    if (n == 1):
        return None
    logn = retic_cast(int_log2(retic_cast(n, Dyn, Int, '\nfft.py:39:11: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)')), Int, Dyn, '\nfft.py:39:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    if (N == 0):
        return None
    FFT_bitreverse(retic_cast(N, Dyn, Int, '\nfft.py:42:4: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)'), data)
    while (bit < logn):
        w_real = retic_cast(1.0, Float, Dyn, '\nfft.py:49:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        w_imag = retic_cast(0.0, Float, Dyn, '\nfft.py:50:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        theta = (((2.0 * direction) * math.pi) / (2.0 * float(dual)))
        s = retic_cast(math.sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:52:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(theta)
        t = retic_cast(math.sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:53:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((theta / 2.0))
        s2 = ((2.0 * t) * t)
        for b in range(retic_cast(0, Int, Dyn, '\nfft.py:55:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), n, (2 * dual)):
            i = (2 * b)
            j = (2 * (b + dual))
            wd_real = data[j]
            wd_imag = data[(j + 1)]
            data[j] = (data[i] - wd_real)
            data[(j + 1)] = (data[(i + 1)] - wd_imag)
            data[i] = (data[i] + wd_real)
            data[(i + 1)] = (data[(i + 1)] + wd_imag)
        for a in xrange(retic_cast(1, Int, Dyn, '\nfft.py:64:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), dual):
            tmp_real = ((w_real - (s * w_imag)) - (s2 * w_real))
            tmp_imag = ((w_imag + (s * w_real)) - (s2 * w_imag))
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(retic_cast(0, Int, Dyn, '\nfft.py:69:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), n, (2 * dual)):
                i = (2 * (b + a))
                j = (2 * ((b + a) + dual))
                z1_real = data[j]
                z1_imag = data[(j + 1)]
                wd_real = ((w_real * z1_real) - (w_imag * z1_imag))
                wd_imag = ((w_real * z1_imag) + (w_imag * z1_real))
                data[j] = (data[i] - wd_real)
                data[(j + 1)] = (data[(i + 1)] - wd_imag)
                data[i] = (data[i] + wd_real)
                data[(i + 1)] = (data[(i + 1)] + wd_imag)
        bit = (bit + 1)
        dual = (dual * 2)
        return None
FFT_transform_internal = FFT_transform_internal

def FFT_transform_internal_fast(N, data, direction):
    n = (N // 2)
    bit = 0
    dual = 1
    if (n == 1):
        return None
    logn = int_log2_fast(n)
    if (N == 0):
        return None
    FFT_bitreverse_fast(N, data)
    while (bit < logn):
        w_real = 1.0
        w_imag = 0.0
        theta = (((2.0 * direction) * math.pi) / (2.0 * float(retic_cast(dual, Int, Dyn, '\nfft.py:51:51: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))))
        s = retic_cast(math.sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:52:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(theta)
        t = retic_cast(math.sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:53:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((theta / 2.0))
        s2 = ((2.0 * t) * t)
        for b in range(retic_cast(0, Int, Dyn, '\nfft.py:55:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(n, Int, Dyn, '\nfft.py:55:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast((2 * dual), Int, Dyn, '\nfft.py:55:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
            i = (2 * b)
            j = retic_cast((2 * (b + dual)), Int, Dyn, '\nfft.py:57:12: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
            wd_real = data[j]
            wd_imag = data[(j + 1)]
            data[j] = (data[i] - wd_real)
            data[(j + 1)] = (data[(i + 1)] - wd_imag)
            data[i] = (data[i] + wd_real)
            data[(i + 1)] = (data[(i + 1)] + wd_imag)
        for a in xrange(retic_cast(1, Int, Dyn, '\nfft.py:64:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(dual, Int, Dyn, '\nfft.py:64:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
            tmp_real = retic_cast(((w_real - (s * w_imag)) - (s2 * w_real)), Dyn, Float, '\nfft.py:65:12: Right hand side of assignment was expected to be of type Float, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
            tmp_imag = retic_cast(((w_imag + (s * w_real)) - (s2 * w_imag)), Dyn, Float, '\nfft.py:66:12: Right hand side of assignment was expected to be of type Float, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(retic_cast(0, Int, Dyn, '\nfft.py:69:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(n, Int, Dyn, '\nfft.py:69:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast((2 * dual), Int, Dyn, '\nfft.py:69:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
                i = (2 * (b + a))
                j = retic_cast((2 * ((b + a) + dual)), Int, Dyn, '\nfft.py:71:16: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
                z1_real = data[j]
                z1_imag = data[(j + 1)]
                wd_real = ((w_real * z1_real) - (w_imag * z1_imag))
                wd_imag = ((w_real * z1_imag) + (w_imag * z1_real))
                data[j] = (data[i] - wd_real)
                data[(j + 1)] = (data[(i + 1)] - wd_imag)
                data[i] = (data[i] + wd_real)
                data[(i + 1)] = (data[(i + 1)] + wd_imag)
        bit = (bit + 1)
        dual = (dual * 2)
        return None
FFT_transform_internal_fast = FFT_transform_internal_fast

def FFT_bitreverse(N, data):
    n = retic_cast((N // 2), Int, Dyn, '\nfft.py:89:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    nm1 = (n - 1)
    j = retic_cast(0, Int, Dyn, '\nfft.py:91:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for i in range(nm1):
        ii = (i << 1)
        jj = (j << 1)
        k = (n >> 1)
        if (i < j):
            tmp_real = data[ii]
            tmp_imag = data[(ii + 1)]
            data[ii] = data[jj]
            data[(ii + 1)] = data[(jj + 1)]
            data[jj] = tmp_real
            data[(jj + 1)] = tmp_imag
        while (k <= j):
            j = (j - k)
            k = (k >> 1)
        j = (j + k)
        return None
FFT_bitreverse = FFT_bitreverse

def FFT_bitreverse_fast(N, data):
    n = (N // 2)
    nm1 = (n - 1)
    j = 0
    for i in range(retic_cast(nm1, Int, Dyn, '\nfft.py:92:13: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
        ii = retic_cast((i << 1), Int, Dyn, '\nfft.py:93:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        jj = (j << 1)
        k = (n >> 1)
        if (i < j):
            tmp_real = data[ii]
            tmp_imag = data[(ii + 1)]
            data[ii] = data[jj]
            data[(ii + 1)] = data[(jj + 1)]
            data[jj] = tmp_real
            data[(jj + 1)] = tmp_imag
        while (k <= j):
            j = (j - k)
            k = (k >> 1)
        j = (j + k)
        return None
FFT_bitreverse_fast = FFT_bitreverse_fast

def FFT_transform(N, data):
    FFT_transform_internal(N, data, (- 1))
    return None
FFT_transform = FFT_transform

def FFT_transform_fast(N, data):
    FFT_transform_internal_fast(N, data, (- 1))
    return None
FFT_transform_fast = FFT_transform_fast

def FFT_inverse(N, data):
    n = (N / 2)
    norm = retic_cast(0.0, Float, Dyn, '\nfft.py:119:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    FFT_transform_internal(N, data, 1)
    norm = retic_cast((1 / float(n)), Float, Dyn, '\nfft.py:121:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for i in xrange(N):
        data[i] = (data[i] * norm)
    return None
FFT_inverse = FFT_inverse

def FFT_inverse_fast(N, data):
    n = retic_cast((N / 2), Float, Dyn, '\nfft.py:118:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    norm = 0.0
    FFT_transform_internal_fast(N, data, 1)
    norm = (1 / float(n))
    for i in xrange(retic_cast(N, Int, Dyn, '\nfft.py:122:13: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
        data[i] = (data[i] * norm)
    return None
FFT_inverse_fast = FFT_inverse_fast

def bench_FFT(loops, N, cycles):
    twoN = retic_cast((2 * N), Int, Dyn, '\nfft.py:128:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    init_vec = retic_cast(array, Dyn, Function(AnonymousParameters([String, List(Dyn)]), Dyn), "\nfft.py:129:15: Expected function of type Function(['String', 'List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d', [retic_cast(random.random, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:129:27: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')() for i in retic_cast(range(twoN), Iterable(Int), Dyn, '\nfft.py:129: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')])
    range_it = retic_cast(xrange(loops), Iterable(Int), Dyn, '\nfft.py:130:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for _ in range_it:
        x = retic_cast(array, Dyn, Function(AnonymousParameters([String]), Dyn), "\nfft.py:134:12: Expected function of type Function(['String'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d')
        x[:] = init_vec[:]
        for i in xrange(cycles):
            FFT_transform(twoN, x)
            FFT_inverse(twoN, x)
    return None
bench_FFT = bench_FFT

def bench_FFT_fast(loops, N, cycles):
    twoN = (2 * N)
    init_vec = retic_cast(array, Dyn, Function(AnonymousParameters([String, List(Dyn)]), Dyn), "\nfft.py:129:15: Expected function of type Function(['String', 'List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d', [retic_cast(random.random, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:129:27: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')() for i in retic_cast(range(retic_cast(twoN, Int, Dyn, '\nfft.py:129:52: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')), Iterable(Int), Dyn, '\nfft.py:129: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')])
    range_it = xrange(loops)
    for _ in range_it:
        x = retic_cast(array, Dyn, Function(AnonymousParameters([String]), Dyn), "\nfft.py:134:12: Expected function of type Function(['String'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d')
        x[:] = init_vec[:]
        for i in xrange(cycles):
            FFT_transform_fast(twoN, x)
            FFT_inverse_fast(twoN, x)
    return None
bench_FFT_fast = bench_FFT_fast
t0 = retic_cast(time.time, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:145:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
bench_FFT_fast(retic_cast(100, Int, Dyn, '\nfft.py:146:0: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), 1024, retic_cast(50, Int, Dyn, '\nfft.py:146:0: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
t1 = retic_cast(time.time, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:147:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
retic_cast(print, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:148:0: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((t1 - t0))
retic_cast(print, Dyn, Function(AnonymousParameters([String, Int]), Dyn), "\nfft.py:153:0: Expected function of type Function(['String', 'Int'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('log2Calls is: ', log2Calls)
