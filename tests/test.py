def foo(x:Float):
    return x * 2

def bar(y):
    return foo(y)

def baz(z):
    return bar(z)

baz(2)
