from operator import itemgetter
timeDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

entries = []
print("\\begin{tabular}{ l | c | c | c }")
print("Name & Reticulated & DT & Improvement\\\ \\hline")
with open("log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l.rstrip()            
        else:
            if (name in timeDict):
                ourTime = float(l)
                reticTime = timeDict[name]
                #print("{0} & {1} & {2} & {3}\\\ ".format(name, reticTime, ourTime, reticTime/ourTime))
                entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime))
                #if timeDict[name] >= float(l):
                #    fasterCount += 1
                #    noRepeat = False
            else:                
                timeDict[name] = float(l)
                if (noRepeat):
                    totalCount += 1

current_improvement = .1
for name, retic_time, our_time, improvement in sorted(entries, key=itemgetter(3)):
    if (round2(improvement) >= round2(current_improvement) + .1):        
        current_improvement = round2(improvement) 
        print("\\hline {0} & {1} & {2} & {3}\\\ ".format(name, round2(retic_time), round2(our_time), round2(improvement)))
    else:
        print("{0} & {1} & {2} & {3}\\\ ".format(name, round2(retic_time), round2(our_time), round2(improvement)))
    

print("\end{tabular}")
#print ("Our approach was faster for: ", fasterCount, "/", totalCount)
