import random
header = """

import array
import math
import time
import perf
import pdb
from six.moves import xrange


DEFAULT_WIDTH = 100
DEFAULT_HEIGHT = 100
EPSILON = 0.00001
ZERO = (0, 0, 0)#vector(0, 0, 0)
RIGHT = (1, 0, 0)#vector(1, 0, 0)
UP = (0, 1, 0)#vector(0, 1, 0)
OUT = (0, 0, 1)#vector(0, 0, 1)

"""

vector_headers = ["def vector(initx, inity, initz)->Tuple(Dyn,Dyn,Dyn):"]

vector_body = """
    return (initx, inity, initz)

"""

#def vectorstr(vec):
#    (x,y,z) = vec
#    return '(%s,%s,%s)' % (x, y, z)

#def __repr__(vec):
#    (x,y,z) = vec
#    return 'Vector(%s,%s,%s)' % (x, y, z)

dot_headers = ["def dot(vec, other)->float:","def dot(vec:Tuple(Dyn,Dyn,Dyn), other)->float:","def dot(vec, other:Tuple(Dyn,Dyn,Dyn))->float:","def dot(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->float:"]

dot_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1, y1, z1) = other
    return (x * x1 * 1.0) + (y * y1 * 1.0) + (z * z1 * 1.0)

"""

magnitude_headers = ["def magnitude(vec)->float:","def magnitude(vec:Tuple(Dyn,Dyn,Dyn))->float:"]
magnitude_body = """
    return (math.sqrt(dot(vec, vec)) + 0.0)

"""

add_headers = ["def add(vec, other)-> Tuple(Dyn,Dyn,Dyn):","def add(vec:Tuple(Dyn,Dyn,Dyn), other)-> Tuple(Dyn,Dyn,Dyn):", "def add(vec, other:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):", "def add(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):"]
add_body = """
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (x+x1, y+y1, z+z1)
    #if other.isPoint():
        #return Point(self.x + other.x, self.y + other.y, self.z + other.z)
    #else:
        #return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

"""
sub_headers = ["def sub(vec, other)->Tuple(Dyn,Dyn,Dyn):","def sub(vec:Tuple(Dyn,Dyn,Dyn), other)->Tuple(Dyn,Dyn,Dyn):","def sub(vec, other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):","def sub(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
sub_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return vector(x - x1, y - y1, z - z1)

"""

scale_headers = ["def scale(vec, factor)->Tuple(Dyn,Dyn,Dyn):","def scale(vec:Tuple(Dyn,Dyn,Dyn), factor)->Tuple(Dyn,Dyn,Dyn):"]
scale_body = """
    x,y,z = vec
    return (factor * x, factor * y, factor * z)

"""

    
cross_headers = ["def cross(vec, other)->Tuple(Dyn,Dyn,Dyn):","def cross(vec:Tuple(Dyn,Dyn,Dyn), other)->Tuple(Dyn,Dyn,Dyn):","def cross(vec, other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):","def cross(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
cross_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (y * z1 - z * y1,
                      z * x1 - x * z1,
                      x * y1 - y * x1)

"""

normalized_headers = ["def normalized(vec)-> Tuple(Dyn,Dyn,Dyn):","def normalized(vec:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):"]
normalized_body = """
    return scale(vec, 1.0 / magnitude(vec))

"""

negated_headers = ["def negated(vec)->Tuple(Dyn,Dyn,Dyn):","def negated(vec:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
negated_body = """
    return scale(vec, -1)

"""

eq_headers = ["def eq(vec, other)->Bool:","def eq(vec:Tuple(Dyn,Dyn,Dyn), other)->Bool:","def eq(vec, other:Tuple(Dyn,Dyn,Dyn))->Bool:","def eq(vec, other):","def eq(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Bool:"]
eq_body = """
    x,y,z = vec
    x1,y1,z1 = other
    return (x == x1) and (y == y1) and (z == z1)

"""

#def isVector(self):
#    return True

#def isPoint(self):
#    return False

#def mustBeVector(self):
#    return self

#def mustBePoint(self):
#    raise 'Vectors are not points!'

reflectThrough_headers = ["def reflectThrough(vec, normal)->Tuple(Dyn,Dyn,Dyn):",
                          "def reflectThrough(vec:Tuple(Dyn,Dyn,Dyn), normal)->Tuple(Dyn,Dyn,Dyn):",
                          "def reflectThrough(vec, normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):",
                          "def reflectThrough(vec:Tuple(Dyn,Dyn,Dyn), normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
reflectThrough_body = """
    d = scale(normal, dot(vec, normal))
    return sub(vec, scale(d,2))

"""




#assert Vector.RIGHT.reflectThrough(Vector.UP) == Vector.RIGHT
#assert Vector(-1, -1, 0).reflectThrough(Vector.UP) == Vector(-1, 1, 0)


#class Point(object):

#    def __init__(self, initx, inity, initz):
#        self.x = initx
#        self.y = inity
#        self.z = initz

#    def __str__(self):
#        return '(%s,%s,%s)' % (self.x, self.y, self.z)

#    def __repr__(self):
#        return 'Point(%s,%s,%s)' % (self.x, self.y, self.z)

#    def __add__(self, other):
#        other.mustBeVector()
#        return Point(self.x + other.x, self.y + other.y, self.z + other.z)

#    def __sub__(self, other):
#        if other.isPoint():
#            return Vector(self.x - other.x, self.y - other.y, self.z - other.z)
#        else:
#            return Point(self.x - other.x, self.y - other.y, self.z - other.z)

#    def isVector(self):
#        return False

#    def isPoint(self):
#        return True

#    def mustBeVector(self):
#        raise 'Points are not vectors!'

#    def mustBePoint(self):
#        return self


#class Sphere(object):

sphere_headers = ["def sphere(centre, radius)->Tuple(Dyn,Dyn):"]
sphere_body = """
    #centre.mustBePoint()
    #self.centre = centre
    #self.radius = radius
    return (centre, radius)
"""

#def __repr__(self):
#    return 'Sphere(%s,%s)' % (repr(self.centre), self.radius)

#class Ray(object):

ray_headers = ["def ray(point, vect):","def ray(point, vect:Tuple(Dyn,Dyn,Dyn)):"]
ray_body = """
    #self.point = point
    #self.vector = vector.normalized()
    return (point, normalized(vect))

"""

#def __repr__(self):
#    return 'Ray(%s,%s)' % (repr(self.point), repr(self.vector))

pointAtTime_headers = ["def pointAtTime(ray, t)->Tuple(Dyn,Dyn,Dyn):"]
pointAtTime_body = """
    point, vector = ray
    return add(point, scale(vector, t))

"""

intersectionTime_headers = ["def intersectionTime(s, ra):",
                            "def intersectionTime(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), ra):",
                            "def intersectionTime(s, ra:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):",
                            "def intersectionTime(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), ra:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):"]
intersectionTime_body = """
    (centre, radius) = s
    (point, vect) = ra
    cp = sub(centre, point)
    v = dot(cp, vect)
    discriminant = (radius * radius) - (dot(cp, cp) - v * v)
    if discriminant < 0:
        return None
    else:
        return v - math.sqrt(discriminant)

"""

normalAt_headers = ["def normalAt(s, p)->Tuple(Dyn,Dyn,Dyn):",
                    "def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p)->Tuple(Dyn,Dyn,Dyn):",
                    "def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
normalAt_body = """
    (centre, radius) = s
    return normalized((sub(p,  centre)))

"""


#class Halfspace(object):

halfspace_headers = ["def halfspace(point, normal)->Tuple(Dyn, Tuple(Dyn,Dyn,Dyn)):",
                     "def halfspace(point, normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn, Tuple(Dyn,Dyn,Dyn)):"]
halfspace_body = """
    #self.point = point
    #self.normal = normal.normalized()
    return (point, normalized(normal))
"""

#def __repr__(self):
#    return 'Halfspace(%s,%s)' % (repr(self.point), repr(self.normal))

intersectionTime1_headers = ["def intersectionTime1(hs, ray)->float:",
                            "def intersectionTime1(hs:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)), ray)->float:",
                            "def intersectionTime1(hs, ray:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))->float:",
                            "def intersectionTime1(hs:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)), ray:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))->float:"]
intersectionTime1_body = """
    point, vector = ray
    (point, normal) = hs
    v = dot(vector, normal)
    if v:
        return 1 / -v
    return -1.0

"""

normalAt1_headers = ["def normalAt1(hs, p):",
                    "def normalAt1(hs:Tuple(Dyn,Dyn), p):"]
normalAt1_body = """
    (p1, normal) = hs
    return normal

"""





#Point.ZERO = Point(0, 0, 0)


#class Canvas(object):

canvas_headers = ["def canvas(width, height)->Tuple(Dyn,Dyn,int):"]
canvas_body = """
    byts = array.array('B', [0] * (width * height * 3))
    #byts = [0] * (width * height * 3)
    for i in xrange(width * height):
        #0
        byts[i * 3 + 2] = 255
    #self.width = width
    #self.height = height
    return (byts, width, height)

"""

plot_headers = ["def plot(canv, x, y, r, g, b):",
                "def plot(canv:Tuple(Dyn,Dyn,Dyn), x, y, r, g, b):",
                "def plot(canv, x:int, y, r, g, b):",
                "def plot(canv, x, y:int, r, g, b):",
                "def plot(canv, x, y, r:int, g, b):",
                "def plot(canv, x, y, r, g:int, b):",
                "def plot(canv, x, y, r, g, b:int):",
                "def plot(canv:Tuple(Dyn,Dyn,Dyn), x:int, y:int, r:int, g:int, b:int):"]
plot_body = """
    (byts, width, height) = canv
    i = ((height - y - 1) * width + x) * 3
    byts[i] = max(0, min(255, int(r * 255)))
    byts[i + 1] = max(0, min(255, int(g * 255)))
    byts[i + 2] = max(0, min(255, int(b * 255)))
    return None

"""

#def write_ppm(self, filename):
#    header = 'P6 %d %d 255\n' % (self.width, self.height)
#    with open(filename, "wb") as fp:
#        fp.write(header.encode('ascii'))
#        fp.write(self.bytes.tostring())

firstIntersection_headers = ["def firstIntersection(intersections):",
                             "def firstIntersection(intersections:List(Dyn)):"]
firstIntersection_body = """
    result = None
    for i in intersections:
        candidateT = i[1]
        if candidateT is not None and candidateT > -EPSILON:
            if result is None or candidateT < result[1]:
                result = i
    return result

"""


#class Scene(object):
scene_headers = ["def scene()->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Int):"]
scene_body = """
    objects = []
    lightPoints = []
    position = vector(0.0, 1.8, 10.0)
    lookingAt = (0.0,0.0,0.0)#Point.ZERO
    fieldOfView = 45
    recursionDepth = 0
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""

moveTo_headers = ["def moveTo(sc, p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):",
                  "def moveTo(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
moveTo_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    position = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""

lookAt_headers = ["def lookAt(sc, p):","def lookAt(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
lookAt_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lookingAt = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""

addObject_headers = ["def addObject(sc, object, surface):","def addObject(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), object, surface)-> Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
addObject_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    objs = [] + objects
    objs.append((object, surface))
    return (objs, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""

addLight_headers = ["def addLight(sc, p):","def addLight(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
addLight_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lps = [] + lightPoints
    lps.append(p)
    return (objects, lps, position, lookingAt, fieldOfView, recursionDepth)

"""

addColours_headers = ["def addColours(a, scale, b)->Tuple(Dyn,Dyn,Dyn):"]
addColours_body = """
    return (a[0] + scale * b[0],
            a[1] + scale * b[1],
            a[2] + scale * b[2])

"""

baseColourAt_headers = ["def baseColourAt(ss, p):","def baseColourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), p):"]
baseColourAt_body = """
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient) = ss
    return baseColour

"""

colourAt_headers = ["def colourAt(ss, scene1, ray1, p1, normal1):",
                    "def colourAt(ss, scene1, ray1, p1:Tuple(Dyn,Dyn,Dyn), normal1):",
                    "def colourAt(ss, scene1, ray1, p1, normal1:Tuple(Dyn,Dyn,Dyn)):"
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1, p1, normal1):",
                    "def colourAt(ss, scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1):",
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1):",
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1:Tuple(Dyn,Dyn,Dyn)):"]
colourAt_body = """
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)
    b = baseColourAt(ss, p)
    (p,v) = ray1

    c = (0, 0, 0)
    if specularCoefficient > 0:
        reflectedRay = ray(p1, reflectThrough(v, vnormal))
        reflectedColour = rayColour(scene1,reflectedRay)
        c = addColours(c, specularCoefficient, reflectedColour)

    if lambertCoefficient > 0:
        lambertAmount = 0
        for lightPoint in visibleLights(scene1, p1):
            contribution = dot(normalized((sub(lightPoint, p1))), normal)
            if contribution > 0:
                lambertAmount = lambertAmount + contribution
        lambertAmount = min(1, lambertAmount)
        c = addColours(c, lambertCoefficient * lambertAmount, b)

    if ambientCoefficient > 0:
        c = addColours(c, ambientCoefficient, b)

    return c

"""


rayColour_headers = ["def rayColour(sc, ry):",
                     "def rayColour(sc, ry:Tuple(Dyn,Dyn,Dyn)):"]
rayColour_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    if recursionDepth > 3:
        return (0.0, 0.0, 0.0)
    try:
        recursionDepth = recursionDepth + 1
        intersections = [(o, intersectionTime(o,ry), s)
                             for (o, s) in objects]
        i = firstIntersection(intersections)
        if i is None:
            return (0.0, 0.0, 0.0)  # the background colour
        else:
            (o, t, s) = i
            p = pointAtTime(ry, t)
            return colourAt(s, sc, ry, p, o.normalAt(p))
    finally:
        recursionDepth = recursionDepth - 1
    return (0.0,0.0,0.0)

"""

render_headers = ["def render(sc, canvas1):",
                  "def render(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), canvas1):"]
render_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    fovRadians = math.pi * (fieldOfView / 2.0) / 180.0
    halfWidth = math.tan(fovRadians)
    halfHeight = 0.75 * halfWidth
    width = halfWidth * 2
    height = halfHeight * 2
    bytes, w, h = canvas1
    pixelWidth = width / (w - 1)
    pixelHeight = height / (h - 1)

    eye = ray(position, sub(lookingAt, position))
    p, v = eye    
    vpRight = normalized(cross(v, UP))
    vpUp = normalized(cross(vpRight, v))

    for y in xrange(int(height)):
        for x in xrange(int(width)):
            xcomp = scale(vpRight, x * pixelWidth - halfWidth)
            ycomp = scale(vpUp, y * pixelHeight - halfHeight)
            r = ray(p, add(add(v, xcomp), ycomp))
            #colour = rayColour(sc, r)
            #plot(canvas1, x, y, *colour)

    return None

"""

lightIsVisible_headers = ["def lightIsVisible(sc, l, p):",
                          "def lightIsVisible(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), l, p):"]
lightIsVisible_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    for (o, s) in objects:
        t = intersectionTime(o, ray(p, sub(l, p)))
        if t is not None and t > EPSILON:
            return False
    return True

"""
visibleLights_headers = ["def visibleLights(sc, p):","def visibleLights(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p):"]
visibleLights_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    result = []
    for l in lightPoints:
        if lightIsVisible(sc, l, p):
            result.append(l)
    return result

"""




#class SimpleSurface(object):

simpleSurface_headers = ["def simpleSurface(baseColour)->Tuple(Dyn,Float,Float,Dyn):"]
simpleSurface_body = """
    #baseColour = kwargs.get('baseColour', (1, 1, 1))
    specularCoefficient = 0.2
    lambertCoefficient =  0.6
    ambientCoefficient = 1.0 - specularCoefficient - lambertCoefficient
    return (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)

"""


#class CheckerboardSurface(SimpleSurface):

#    def __init__(self, **kwargs):
#        SimpleSurface.__init__(self, **kwargs)
#        self.otherColour = kwargs.get('otherColour', (0, 0, 0))
#        self.checkSize = kwargs.get('checkSize', 1)

#    def baseColourAt(self, p):
#        v = p - Point.ZERO
#        v.scale(1.0 / self.checkSize)
#        if (int(abs(v.x) + 0.5) +
#            int(abs(v.y) + 0.5) +
#            int(abs(v.z) + 0.5)) \
#           % 2:
#            return self.otherColour
#        else:
#            return self.baseColour

bench_raytrace_headers = ["def bench_raytrace(loops, width, height, filename):"]
bench_raytrace_body = """
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for i in range_it:
        #canvas1 = canvas(width, height)
        s = scene()
        addLight(s, vector(30, 30, 10))
        addLight(s, vector(-10, 100, 30))
        lookAt(s, vector(0, 3, 0))
        addObject(s, sphere(vector(1, 3, -10), 2),
                    simpleSurface((1, 1, 0)))
        for y in xrange(6):
            addObject(s,sphere(vector(-3 - y * 0.4, 2.3, -5), 0.4),
                        simpleSurface((y / 6.0, 1 - y / 6.0, 0.5)))
            scale(normalized(vector(10,23,19)), y * 11)
        #s.addObject(Halfspace(Point(0, 0, 0), Vector.UP),
        #            CheckerboardSurface())
        #render(s,canvas1)
    return None

"""

rest = """

    #dt = perf.perf_counter() - t0

    #if filename:
    #    canvas.write_ppm(filename)
    #return dt
def main():
    t0 = time.time()
    bench_raytrace(100, DEFAULT_WIDTH, DEFAULT_HEIGHT, "raytrace.ppm")
    t1 = time.time()
    print(t1-t0)

main()

"""

for i in range(100):
    f = open("raytrace" + str(i) + ".py","w+")
    vector = vector_headers[random.randrange(0,len(vector_headers))] + vector_body
    dot = dot_headers[random.randrange(0,len(dot_headers))] + dot_body
    magnitude = magnitude_headers[random.randrange(0,len(magnitude_headers))] + magnitude_body
    add = add_headers[random.randrange(0,len(add_headers))] + add_body
    sub = sub_headers[random.randrange(0,len(sub_headers))] + sub_body
    scale = scale_headers[random.randrange(0,len(scale_headers))] + scale_body
    cross = cross_headers[random.randrange(0,len(cross_headers))] + cross_body
    normalized = normalized_headers[random.randrange(0,len(normalized_headers))] + normalized_body
    negated = negated_headers[random.randrange(0,len(negated_headers))] + negated_body
    eq = eq_headers[random.randrange(0,len(eq_headers))] + eq_body
    reflectThrough = reflectThrough_headers[random.randrange(0,len(reflectThrough_headers))] + reflectThrough_body
    sphere = sphere_headers[random.randrange(0,len(sphere_headers))] + sphere_body
    ray = ray_headers[random.randrange(0,len(ray_headers))] + ray_body
    pointAtTime = pointAtTime_headers[random.randrange(0,len(pointAtTime_headers))] + pointAtTime_body
    intersectionTime = intersectionTime_headers[random.randrange(0,len(intersectionTime_headers))] + intersectionTime_body
    normalAt = normalAt_headers[random.randrange(0,len(normalAt_headers))] + normalAt_body
    halfspace = halfspace_headers[random.randrange(0,len(halfspace_headers))] + halfspace_body
    intersectionTime1 = intersectionTime1_headers[random.randrange(0,len(intersectionTime1_headers))] + intersectionTime1_body
    normalAt1 = normalAt1_headers[random.randrange(0,len(normalAt1_headers))] + normalAt1_body
    canvas = canvas_headers[random.randrange(0,len(canvas_headers))] + canvas_body
    plot = plot_headers[random.randrange(0,len(plot_headers))] + plot_body
    firstIntersection = firstIntersection_headers[random.randrange(0,len(firstIntersection_headers))] + firstIntersection_body
    scene = scene_headers[random.randrange(0,len(scene_headers))] + scene_body
    moveTo = moveTo_headers[random.randrange(0,len(moveTo_headers))] + moveTo_body
    lookAt = lookAt_headers[random.randrange(0,len(lookAt_headers))] + lookAt_body
    addObject = addObject_headers[random.randrange(0,len(addObject_headers))] + addObject_body
    addLight = addLight_headers[random.randrange(0,len(addLight_headers))] + addLight_body
    addColours = addColours_headers[random.randrange(0,len(addColours_headers))] + addColours_body
    baseColourAt = baseColourAt_headers[random.randrange(0,len(baseColourAt_headers))] + baseColourAt_body
    #colourAt = colourAt_headers[random.randrange(0,len(colourAt_headers))] + colourAt_body
    #rayColour = rayColour_headers[random.randrange(0,len(rayColour_headers))] + rayColour_body
    render = render_headers[random.randrange(0,len(render_headers))] + render_body
    lightIsVisible = lightIsVisible_headers[random.randrange(0,len(lightIsVisible_headers))] + lightIsVisible_body
    visibleLights = visibleLights_headers[random.randrange(0,len(visibleLights_headers))] + visibleLights_body
    simpleSurface = simpleSurface_headers[random.randrange(0,len(simpleSurface_headers))] + simpleSurface_body
    bench_raytrace = bench_raytrace_headers[random.randrange(0,len(bench_raytrace_headers))] + bench_raytrace_body
    #main = main_headers[random.randrange(0,len(main_headers))] + main_body

    out = header + vector + dot + magnitude + add + sub + scale + cross + normalized + negated + eq + reflectThrough + sphere + ray + pointAtTime + intersectionTime + normalAt + halfspace + intersectionTime + normalAt + canvas + plot + firstIntersection + scene + moveTo + lookAt + addObject + addLight + addColours + baseColourAt + render + lightIsVisible + visibleLights + simpleSurface + bench_raytrace + rest
    f.write(out)
    f.close()
