timeList = []
with open("log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            continue
        else:
            timeList.append(eval(l))    
    HerderTime = timeList[0]
    topThree = False
    for j, time in enumerate(sorted(timeList)):
        if (HerderTime <= time and j < 3):
            topThree = True
        if (j >= 3):
            break
    if (topThree):
        print ("Herder's time is %s and it is in the top three configurations." % HerderTime)
    else:
        print ("Herder's time is %s and it is not in the top three configurations." % HerderTime)
    
