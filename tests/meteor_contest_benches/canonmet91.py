from __future__ import division, print_function, absolute_import
from retic.runtime import *
from retic.guarded import *
from retic.typing import *
from bisect import bisect
from six.moves import xrange
import time
SOLVE_ARG = 60
(WIDTH, HEIGHT) = (5, 10)
DIR_NO = 6
(S, E) = retic_cast(((WIDTH * HEIGHT), 2), Tuple(Int, Int), Tuple(Dyn, Int), '\nmet91.py:15:0: Right hand side of assignment was expected to be of type Tuple(Dyn,Int), but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
SE = (S + (E / 2))
SW = (SE - E)
(W, NW, NE) = retic_cast(((- E), (- SE), (- SW)), Tuple(Int, Dyn, Dyn), Tuple(Dyn, Dyn, Dyn), '\nmet91.py:18:0: Right hand side of assignment was expected to be of type Tuple(Dyn,Dyn,Dyn), but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
SOLUTIONS = ['00001222012661126155865558633348893448934747977799', '00001222012771127148774485464855968596835966399333', '00001222012771127148774485494855998596835966366333', '00001222012771127148774485994855948596835966366333', '00001222012771127166773863384653846538445584959999', '00001222012771127183778834833348555446554666969999', '00001222012771127183778834833348555446554966699996', '00001223012331123166423564455647456775887888979999', '00001555015541144177484726877268222683336689399993', '00001555015541144177484728677286222863338669399993', '00001599015591159148594482224827748276837766366333', '00001777017871184155845558449984669662932629322333', '00004222042664426774996879687759811598315583153331', '00004222042774427384773183331866118615586555969999', '00004223042334423784523785771855718566186611969999', '00004227042874428774528735833155831566316691169999', '00004333045534455384175781777812228116286662969999', '00004333045934459384559185991866118612286727267772', '00004333047734427384277182221866118615586555969999', '00004555045514411224172621768277368736683339899998', '00004555045514411224172721777299998966836688368333', '00004555045534413334132221177266172677886888969999', '00004555045534473334739967792617926192661882211888', '00004555045564466694699992288828811233317273177731', '00004555045564466694699997773172731233312881122888', '00004555045564496664999962288828811233317273177731', '00004555045564496664999967773172731233312881122888', '00004555045584411884171187722866792679236992369333', '00004555045584411884191189999832226333267372677766', '00004555045584411884191189999866222623336727367773', '13335138551389511895778697869947762446624022240000', '13777137271333211882888226999946669446554055540000', '13777137271333211882888229999649666446554055540000', '27776272768221681166819958195548395443954033340000', '33322392623926696648994485554855148117871077710000', '33366366773867284772842228449584195119551099510000', '33366366953869584955849958447784172117721022210000', '33366366953869589955849458447784172117721022210000', '33386388663866989999277712727142211441554055540000', '33396329963297629766822778117148811448554055540000', '33399366953869586955846458447784172117721022210000', '37776372763332622266899998119148811448554055540000', '39999396683336822268277682748477144114551055510000', '39999398663338622286277862748477144114551055510000', '66777627376233362223899998119148811448554055540000', '69999666945564455584333843887738172117721022210000', '88811228816629162971629776993743337443554055540000', '88822118821333213727137776999946669446554055540000', '88822118821333213727137779999649666446554055540000', '89999893338663786377286712627142211441554055540000', '99777974743984439884333685556855162116621022210000', '99995948554483564835648336837766172117721022210000', '99996119661366513855133853782547782447824072240000', '99996911668166581755817758732548732443324032240000', '99996926668261182221877718757148355443554033340000', '99996955568551681166812228177248372443774033340000', '99996955568551681166813338137748372447724022240000', '99996966645564455584333843887738172117721022210000', '99996988868877627166277112223143331443554055540000', '99997988878857765474655446532466132113321032210000']

def rotate(ido):
    for _ in ido:
        break
    rd = {E: NE, NE: NW, NW: W, W: SW, SW: SE, SE: E, }
    return [rd[o] for o in ido]
rotate = rotate

def flip(ido):
    for _ in ido:
        break
    fd = {E: E, NE: SE, NW: SW, W: W, SW: NW, SE: NE, }
    return [fd[o] for o in ido]
flip = flip

def permute(ido, r_ido):
    ps = []
    ps.append(retic_cast(ido, List(Dyn), Dyn, '\nmet91.py:101:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    for r in xrange((DIR_NO - 1)):
        ps.append(retic_cast(rotate(ps[(- 1)]), List(Dyn), Dyn, '\nmet91.py:107:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
        for i in range(len(retic_cast(ps, List(Dyn), Dyn, '\nmet91.py:110:23: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))):
            pp = ps[retic_cast(i, Dyn, Int, '\nmet91.py:111: Cannot use a value %s as an index into a List(Dyn), use a value of type Int instead. (code BAD_INDEX)')]
            ps.append(retic_cast(flip(pp), List(Dyn), Dyn, '\nmet91.py:112:12: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    return ps
permute = permute

def convert(ido):
    out = [0.0]
    for o in ido:
        out.append(retic_cast((out[(- 1)] + o), Dyn, Float, '\nmet91.py:120:8: Expected argument of type Float but value %s was provided instead. (code ARG_ERROR)'))
    return list(retic_cast(set, Dyn, Function(AnonymousParameters([List(Float)]), Dyn), "\nmet91.py:121:16: Expected function of type Function(['List(Float)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(out))
convert = convert

def get_footprints(board, cti, pieces):
    for _ in pieces:
        break
    fps = [[[] for p in retic_cast(xrange(len(retic_cast(pieces, List(Dyn), Dyn, '\nmet91.py:126:31: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))), Iterable(Dyn), Dyn, '\nmet91.py:126: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')] for ci in retic_cast(xrange(len(retic_cast(board, List(Dyn), Dyn, '\nmet91.py:126:62: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))), Iterable(Dyn), Dyn, '\nmet91.py:126: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')]
    for c in board:
        for (pi, p) in retic_cast(enumerate, Dyn, Function(AnonymousParameters([List(Dyn)]), Dyn), "\nmet91.py:128:21: Expected function of type Function(['List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(pieces):
            for pp in p:
                fp = retic_cast(frozenset, Dyn, Function(AnonymousParameters([List(Dyn)]), Dyn), "\nmet91.py:130:21: Expected function of type Function(['List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")([cti[(c + o)] for o in pp if ((c + o) in cti)])
                if (len(fp) == 5):
                    fps[retic_cast(retic_cast(min, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:132:24: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(fp), Dyn, Int, '\nmet91.py:132: Cannot use a value %s as an index into a List(List(List(Dyn))), use a value of type Int instead. (code BAD_INDEX)')][retic_cast(pi, Dyn, Int, '\nmet91.py:132: Cannot use a value %s as an index into a List(List(Dyn)), use a value of type Int instead. (code BAD_INDEX)')].append(fp)
    return fps
get_footprints = get_footprints

def get_senh(board, cti):
    se_nh = []
    nh = [E, SW, SE]
    for c in board:
        se_nh.append(retic_cast(frozenset, Dyn, Function(AnonymousParameters([List(Dyn)]), Dyn), "\nmet91.py:140:21: Expected function of type Function(['List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")([cti[(c + o)] for o in retic_cast(nh, List(Dyn), Dyn, '\nmet91.py:140: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)') if ((c + o) in cti)]))
    return se_nh
get_senh = get_senh

def get_puzzle(width, height):
    board = [(((E * x) + (S * y)) + (y % 2)) for y in retic_cast(xrange((height + 0)), Iterable(Dyn), Dyn, '\nmet91.py:144: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)') for x in retic_cast(xrange((width + 0)), Iterable(Dyn), Dyn, '\nmet91.py:144: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')]
    cti = retic_cast(dict, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:147:10: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(((board[retic_cast(i, Dyn, Int, '\nmet91.py:147: Cannot use a value %s as an index into a List(Dyn), use a value of type Int instead. (code BAD_INDEX)')], i) for i in retic_cast(xrange(len(retic_cast(board, List(Dyn), Dyn, '\nmet91.py:147:45: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))), Iterable(Dyn), Dyn, '\nmet91.py:147: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')))
    idos = [[E, E, E, SE], [SE, SW, W, SW], [W, W, SW, SE], [E, E, SW, SE], [NW, W, NW, SE, SW], [E, E, NE, W], [NW, NE, NE, W], [NE, SE, E, NE], [SE, SE, E, SE], [E, NW, NW, NW]]
    perms = (permute(p, retic_cast(idos[3], List(Dyn), Dyn, '\nmet91.py:162:13: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')) for p in retic_cast(idos, List(List(Dyn)), Dyn, '\nmet91.py:162: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)'))
    p = list(perms)
    pieces = [[convert(pp) for pp in p[0]]]
    return retic_cast((board, cti, pieces), Tuple(List(Dyn), Dyn, List(List(List(Dyn)))), Tuple(Dyn, Dyn, List(Dyn)), '\nmet91.py:166:4: A return value of type Tuple(Dyn,Dyn,List(Dyn)) was expected but a value %s was returned instead. (code RETURN_ERROR)')
get_puzzle = get_puzzle

def solve(n, i_min, free, curr_board, pieces_left, solutions, fps, se_nh):
    fp_i_cands = fps[i_min]
    for p in pieces_left:
        fp_cands = fp_i_cands[p]
        for fp in fp_cands:
            if (fp <= free):
                n_curr_board = curr_board[:]
                for ci in fp:
                    n_curr_board[ci] = p
                if (len(pieces_left) > 1):
                    n_free = (free - fp)
                    n_i_min = retic_cast(min, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:183:30: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(n_free)
                    if (len((n_free & se_nh[n_i_min])) > 0):
                        n_pieces_left = pieces_left[:]
                        retic_cast(retic_cast(n_pieces_left, Dyn, Object('', {'remove': Dyn, }), '\nmet91.py:186:24: Accessing nonexistant object attribute remove from value %s. (code WIDTH_DOWNCAST)').remove, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:186:24: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(p)
                        solve(n, n_i_min, n_free, n_curr_board, n_pieces_left, solutions, fps, se_nh)
                else:
                    s = retic_cast(''.join, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:190:24: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(retic_cast(map, Dyn, Function(AnonymousParameters([Dyn, Dyn]), Dyn), "\nmet91.py:190:32: Expected function of type Function(['Dyn', 'Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(str, n_curr_board))
                    retic_cast(retic_cast(solutions, Dyn, Object('', {'insert': Dyn, }), '\nmet91.py:191:20: Accessing nonexistant object attribute insert from value %s. (code WIDTH_DOWNCAST)').insert, Dyn, Function(AnonymousParameters([Dyn, Dyn]), Dyn), "\nmet91.py:191:20: Expected function of type Function(['Dyn', 'Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(retic_cast(bisect, Dyn, Function(AnonymousParameters([Dyn, Dyn]), Dyn), "\nmet91.py:191:37: Expected function of type Function(['Dyn', 'Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(solutions, s), s)
                    rs = s[::(- 1)]
                    retic_cast(retic_cast(solutions, Dyn, Object('', {'insert': Dyn, }), '\nmet91.py:193:20: Accessing nonexistant object attribute insert from value %s. (code WIDTH_DOWNCAST)').insert, Dyn, Function(AnonymousParameters([Dyn, Dyn]), Dyn), "\nmet91.py:193:20: Expected function of type Function(['Dyn', 'Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(retic_cast(bisect, Dyn, Function(AnonymousParameters([Dyn, Dyn]), Dyn), "\nmet91.py:193:37: Expected function of type Function(['Dyn', 'Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(solutions, rs), rs)
                    if (len(solutions) >= n):
                        return None
        if (len(solutions) >= n):
            return None
solve = solve

def bench_meteor_contest(loops, board, pieces, solve_arg, fps, se_nh):
    range_it = xrange(loops)
    for _ in range_it:
        free = retic_cast(frozenset, Dyn, Function(AnonymousParameters([Iterable(Dyn)]), Dyn), "\nmet91.py:206:15: Expected function of type Function(['Iterable(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(xrange(len(board)))
        curr_board = ([(- 1)] * len(board))
        pieces_left = list(retic_cast(range(len(pieces)), Iterable(Dyn), Dyn, '\nmet91.py:208:22: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
        solutions = []
        solve(solve_arg, 0, free, retic_cast(curr_board, List(Int), Dyn, '\nmet91.py:210:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(pieces_left, List(Dyn), Dyn, '\nmet91.py:210:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(solutions, List(Dyn), Dyn, '\nmet91.py:210:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), fps, se_nh)
    return None
bench_meteor_contest = bench_meteor_contest

def main():
    (board, cti, pieces) = get_puzzle(WIDTH, HEIGHT)
    fps = get_footprints(retic_cast(board, Dyn, List(Dyn), '\nmet91.py:227:10: Expected argument of type List(Dyn) but value %s was provided instead. (code ARG_ERROR)'), cti, pieces)
    se_nh = get_senh(retic_cast(board, Dyn, List(Dyn), '\nmet91.py:228:12: Expected argument of type List(Dyn) but value %s was provided instead. (code ARG_ERROR)'), cti)
    solve_arg = SOLVE_ARG
    bench_meteor_contest(4, board, retic_cast(pieces, List(Dyn), Dyn, '\nmet91.py:231:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), solve_arg, retic_cast(fps, List(List(List(Dyn))), Dyn, '\nmet91.py:231:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(se_nh, List(Dyn), Dyn, '\nmet91.py:231:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
main = main
t0 = retic_cast(retic_cast(time, Dyn, Object('', {'time': Dyn, }), '\nmet91.py:235:5: Accessing nonexistant object attribute time from value %s. (code WIDTH_DOWNCAST)').time, Dyn, Function(AnonymousParameters([]), Dyn), '\nmet91.py:235:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
main()
t1 = retic_cast(retic_cast(time, Dyn, Object('', {'time': Dyn, }), '\nmet91.py:237:5: Accessing nonexistant object attribute time from value %s. (code WIDTH_DOWNCAST)').time, Dyn, Function(AnonymousParameters([]), Dyn), '\nmet91.py:237:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
retic_cast(print, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nmet91.py:238:0: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((t1 - t0))
