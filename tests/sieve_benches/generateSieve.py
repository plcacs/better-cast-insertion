import random
from functools import reduce
# Simple streams library.
# For building and using infinite lists.


# # A stream is a cons of a value and a thunk that computes the next value when applied
#(struct: stream ([first : Natural] [rest : (-> stream)]))
start = """
import time

class Stream:
    first = None
    rest = None
    def __init__(self, first, rest):
        self.first = first
        self.rest = rest
#--------------------------------------------------------------------------------------------------
"""

stream_first_headers = ["def stream_first(st):", "def stream_first(st:Stream) -> Int:"]
stream_first_body = """
    return st.first
"""

stream_rest_headers = ["def stream_rest(st):", "def stream_rest(st:Stream)->Function([], Stream):"]
stream_rest_body = """
    return st.rest
"""

#(: make-stream (-> Natural (-> stream) stream))
make_stream_headers = ["def make_stream (hd, thunk):", "def make_stream (hd:Int, thunk)->Stream:",
                       "def make_stream (hd, thunk:Function([],Stream))->Stream:",
                       "def make_stream (hd:Int, thunk:Function([],Stream))->Stream:"]
make_stream_body = """
    return Stream(hd, thunk)
"""

# Destruct a stream into its first value and the new stream produced by de-thunking the tail
#(: stream-unfold (-> stream (values Natural stream)))
stream_unfold_headers = ["def stream_unfold(st):", "def stream_unfold(st:Stream) -> (Int, Stream):"]
stream_unfold_body = """
    return (stream_first(st), stream_rest(st)())
"""

# [stream-get st i] Get the [i]-th element from the stream [st]
#(: stream-get (-> stream Natural Natural))
stream_get_headers = ["def stream_get(st, i):", "def stream_get(st:Stream, i):", "def stream_get(st, i:Int):", "def stream_get(st:Stream, i:Int)->Int:"]
stream_get_body = """
    (hd, tl) = stream_unfold(st)
    return (hd if i == 0 else stream_get(tl, i-1))
"""

# [stream-take st n] Collect the first [n] elements of the stream [st].
#(: stream-take (-> stream Natural (Listof Natural)))
stream_take_headers = ["def stream_take(st, n):", "def stream_take(st, n:Int):", "def stream_take(st:Stream, n):", "def stream_take(st:Stream, n:Int) -> List(Int):"]
stream_take_body = """
     if n == 0:
         return []
     else:
         (hd, tl) = stream-unfold(st)
         return [hd] + stream_take(tl, n-1)
"""

#lang typed/racket/base

# Use the partner file "streams.rkt" to implement the Sieve of Eratosthenes.
# Then compute and print the 10,000th prime number.
"""
(require benchmark-util)

(require/typed/check "streams.rkt"
  [#:struct stream ([first : Natural]
                    [rest : (-> stream)])]
  [make-stream (-> Natural (-> stream) stream)]
  [stream-unfold (-> stream (values Natural stream))]
  [stream-get (-> stream Natural Natural)]
  [stream-take (-> stream Natural (Listof Natural))])
"""

#--------------------------------------------------------------------------------------------------

# `count-from n` Build a stream of integers starting from `n` and iteratively adding 1
#(: count-from (-> Natural stream))
count_from_headers = ["def count_from(n):", "def count_from(n:Int) -> Stream:"]
count_from_body = """
  return make_stream(n, lambda: count_from(n+1))
"""

# `sift n st` Filter all elements in `st` that are equal to `n`.
# Return a new stream.
#(: sift (-> Natural stream stream))
sift_headers =  ["def sift(n, st):", "def sift(n:Int, st):", "def sift(n, st:Stream):", "def sift(n:Int, st:Stream) -> Stream:"]
sift_body = """
    (hd, tl) = stream_unfold(st)
    return (sift(n, tl) if hd % n == 0 else make_stream(hd, (lambda: sift(n, tl))))
"""

# `sieve st` Sieve of Eratosthenes
#(: sieve (-> stream stream))
sieve_headers = ["def sieve(st):", "def sieve(st:Stream) -> Stream:"]
sieve_body = """
    (hd, tl) = stream_unfold(st)
    return make_stream(hd, (lambda: sieve(sift(hd, tl))))
"""

rest = """
# stream of prime numbers
#(: primes stream)
def primes():
    return sieve(count_from(2))

# Compute the 10,000th prime number
#(: N-1 Natural)
N_1 = 9

#(: main (-> Void))
def main():
  stream_get(primes(), N_1)

t0 = time.time()
for i in range(10):
    main()
t1 = time.time()
print(t1-t0)
            
#(time (main))
"""
def random_element(collection):
    return collection[random.randrange(0, len(collection))]
i = 0
name = "sieve"
while i < 1024:
    stream_first = random_element(stream_first_headers) + stream_first_body 
    stream_rest = random_element(stream_rest_headers) + stream_rest_body
    make_stream = random_element(make_stream_headers) + make_stream_body
    stream_unfold = random_element(stream_unfold_headers) + stream_unfold_body
    stream_get = random_element(stream_get_headers) + stream_get_body
    stream_take = random_element(stream_take_headers) + stream_take_body
    count_from = random_element(count_from_headers) + count_from_body
    sift = random_element(sift_headers) + sift_body
    sieve = random_element(sieve_headers) + sieve_body
    prog = reduce(lambda x, y: x + y, [start,
                                       stream_first,
                                       stream_rest,
                                       make_stream,
                                       stream_unfold,
                                       stream_get,
                                       stream_take,
                                       count_from,
                                       sift,
                                       sieve,
                                       rest], "")
    i+=1
    with open(name + str(i) + ".py", 'w+') as f:
        f.write(prog)
