from operator import itemgetter
import matplotlib.pyplot as plt
import itertools
import numpy as np
import math

timeDict = {}
IAODict = {}
pypy_dict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

max_speedup = 1
reticulated_ratios = []
dt_ratios = []
entries = []
IAO_ratios = []
retic_pypy_ratios = []
dt_pypy_ratios = []
dynamic_time = 0.540015459060669
with open("fft_log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l.rstrip()            
        else:
            if (name in IAODict):
                IAO_time = float(l)
                IAO_ratio = IAO_time / dynamic_time
                IAO_ratios.append(IAO_ratio)
            elif (name in timeDict):
                ourTime = float(l)
                reticTime = timeDict[name]
                dt_ratio = ourTime / dynamic_time
                dt_ratios.append(dt_ratio)
                entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime, reticTime/ourTime))
                IAODict[name] = ourTime
            else:
                retic_time = float(l)
                retic_ratio = retic_time / dynamic_time
                reticulated_ratios.append(retic_ratio)
                timeDict[name] = retic_time
                if (noRepeat):
                    totalCount += 1
with open("pypy_fft.txt", 'r') as log:
    for line_num, line in enumerate(log.readlines()):
        if line_num % 2 == 0:
            name = line.rstrip()
        else:
            if name in pypy_dict:
                dt_pypy_time = float(line)                
                dt_pypy_ratio = dt_pypy_time / dynamic_time
                dt_pypy_ratios.append(dt_pypy_ratio)

            else:
                retic_pypy_time = float(line)
                retic_pypy_ratio = retic_pypy_time / dynamic_time
                retic_pypy_ratios.append(retic_pypy_ratio)
                pypy_dict[name] = retic_pypy_time
                
sorted_retic_ratios = sorted(reticulated_ratios)
sorted_dt_ratios = sorted(dt_ratios)
sorted_IAO_ratios = sorted(IAO_ratios)
sorted_retic_pypy_ratios = sorted(retic_pypy_ratios)
sorted_dt_pypy_ratios = sorted(dt_pypy_ratios)
max_slowdown = max(sorted_retic_ratios[-1], sorted_dt_ratios[-1], sorted_IAO_ratios[-1], sorted_retic_pypy_ratios[-1], sorted_dt_pypy_ratios[-1])

retic_percentage_list = []
dt_percentage_list = []
IAO_percentage_list = []
retic_pypy_percentage_list = []
dt_pypy_percentage_list = []
retic_length = len(sorted_retic_ratios)
dt_length = len(sorted_dt_ratios)
IAO_length = len(sorted_IAO_ratios)
retic_pypy_length = len(retic_pypy_ratios)
dt_pypy_length = len(dt_pypy_ratios)
for i in np.linspace(1, math.ceil(max_slowdown) + 1, 100):
    retic_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_retic_ratios))
    dt_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_dt_ratios))
    IAO_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_IAO_ratios))
    retic_pypy_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_retic_pypy_ratios))
    dt_pypy_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_dt_pypy_ratios))
    
    #speedup_dict[i] = faster_list
    retic_percentage_list.append((len(retic_number_slower) / retic_length) * 100)
    dt_percentage_list.append((len(dt_number_slower) / dt_length) * 100)
    IAO_percentage_list.append((len(IAO_number_slower) / IAO_length) * 100)
    retic_pypy_percentage_list.append((len(retic_pypy_number_slower) / retic_pypy_length) * 100)
    dt_pypy_percentage_list.append((len(dt_pypy_number_slower) / dt_pypy_length) * 100)


fR, ax = plt.subplots()
ax.plot(np.linspace(1, math.ceil(max_slowdown) + 1, 100), dt_percentage_list, 'r-^', label="DT", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), IAO_percentage_list, 'b-o', label="IAO", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), retic_percentage_list, 'g-s', label="Retic", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), retic_pypy_percentage_list, 'y-*', label="Retic_PyPy", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), dt_pypy_percentage_list, 'm-P', label="DT_PyPy", markevery=5, linewidth=3, markersize=10)
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), dt_percentage_list, facecolor="#f7f1b4", edgecolor="black")
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), IAO_percentage_list, facecolor="#c3f799", edgecolor = "black")
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), retic_percentage_list, facecolor="#72a4d4", edgecolor = "black")
ax.set_xlim(left=1, right=math.ceil(max_slowdown))



plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
name="../../plots/fftRatios"
ax.set_title("fft (128 configurations)", fontdict={'fontsize':20})
plt.savefig(name + '.pdf', bbox_inches='tight')
plt.show()
