from operator import itemgetter
import matplotlib.pyplot as plt
import numpy as np
import itertools
import math
import pdb

timeDict = {}
IAODict = {}
proxyLessDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

max_speedup = 1
reticulated_ratios = []
proxyless_ratios = []
dt_ratios = []
entries = []
IAO_ratios = []
#dynamic_time = 0.12360095977783203
with open("even_cast_log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l.rstrip()            
        else:
            if (name in proxyLessDict):
                proxyless_time = int(l)
                proxyless_ratios.append(proxyless_time)
            elif (name in IAODict):
                IAO_time = int(l)
                IAO_ratios.append(IAO_time)
                proxyLessDict[name] = IAO_time
            elif (name in timeDict):
                ourTime = int(l)
                reticTime = timeDict[name]                
                dt_ratios.append(ourTime)
                #entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime, reticTime/ourTime))
                IAODict[name] = ourTime
            else:
                retic_time = int(l)
                reticulated_ratios.append(retic_time)
                timeDict[name] = retic_time
                if (noRepeat):
                    totalCount += 1

sorted_retic_ratios = sorted(map(lambda x: x / 1000, reticulated_ratios))
sorted_dt_ratios = sorted(map(lambda x: x / 1000, dt_ratios))
sorted_IAO_ratios = sorted(map(lambda x: x / 1000, IAO_ratios))
sorted_proxyless_ratios = sorted(map(lambda x: x / 1000, proxyless_ratios))
max_slowdown = max(sorted_retic_ratios[-1], sorted_dt_ratios[-1], sorted_IAO_ratios[-1], sorted_proxyless_ratios[-1])

retic_percentage_list = []
dt_percentage_list = []
IAO_percentage_list = []
proxyless_percentage_list = []
retic_length = len(sorted_retic_ratios)
dt_length = len(sorted_dt_ratios)
IAO_length = len(sorted_IAO_ratios)
proxyless_length = len(sorted_proxyless_ratios)
for i in np.linspace(1, math.ceil(max_slowdown) + 1, 100):
    retic_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_retic_ratios))
    dt_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_dt_ratios))
    IAO_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_IAO_ratios))
    proxyless_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_proxyless_ratios))
    #speedup_dict[i] = faster_list
    retic_percentage_list.append((len(retic_number_slower) / retic_length) * 100)
    dt_percentage_list.append((len(dt_number_slower) / dt_length) * 100)
    IAO_percentage_list.append((len(IAO_number_slower) / IAO_length) * 100)
    proxyless_percentage_list.append((len(proxyless_number_slower) / proxyless_length) * 100)

fR, ax = plt.subplots()
ax.plot(np.linspace(1, math.ceil(max_slowdown) + 1, 100), proxyless_percentage_list, 'r-^', label="DT", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), IAO_percentage_list, 'b-o', label="IAO", markevery=5, linewidth=3, markersize=10)
ax.plot(np.linspace(1,math.ceil(max_slowdown) + 1, 100), retic_percentage_list, 'g-s', label="Retic", markevery=5, linewidth=3, markersize=10)
#ax.plot(
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), dt_percentage_list, facecolor="#f7f1b4", edgecolor="black")
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), IAO_percentage_list, facecolor="#c3f799", edgecolor = "black")
#ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), retic_percentage_list, facecolor="#72a4d4", edgecolor = "black")
ax.set_xlim(left=1, right=math.ceil(max_slowdown))



plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
name="../../plots/evenCasts"
ax.set_title("even (16 configurations)", fontdict={'fontsize':20})
plt.savefig(name + '.pdf', bbox_inches='tight')
#plt.show()


