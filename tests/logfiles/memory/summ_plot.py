import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys

file_name = sys.argv[1]
data = pd.read_csv(file_name).as_matrix()

print data.mean(axis = 0)
print data.max(axis = 0) - data.min(axis = 0)

labels = ["Reticulated", "DT", "IAO"]
name = file_name.split('_memory_log')[0]

plt.boxplot(data, labels = labels, sym = "o")
plt.title(name, fontsize = 'large')
plt.savefig(name + '.pdf', format = 'pdf')
