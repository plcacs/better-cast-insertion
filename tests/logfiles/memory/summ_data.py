import sys, collections, csv

file_name = sys.argv[1]
hashmap = collections.defaultdict(list)

with open(file_name) as f:
	while True:
		config = f.readline().strip()
		if not config:
			break

		val = f.readline().strip()

		hashmap[config].append(val)

outfile = file_name.split('.')[0] + '.csv'
print outfile
with open(outfile, mode = 'w') as f:
	outfile_writer = csv.writer(f, delimiter = ',')
	for v in hashmap.values():
		outfile_writer.writerow(v)


