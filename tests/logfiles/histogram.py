import pdb
from operator import itemgetter
import matplotlib.pyplot as plt
import itertools
timeDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

max_speedup = 1
#reticulated_ratios = []
#dt_ratios = []
configuration_ratios = []
entries = []
#dynamic_time = 0.12360095977783203
for n in ["chaos_log.txt", "even_log.txt", "fft_log.txt",  "go_log.txt", "monte_carlo_log.txt",  "nbody_log.txt", "raytrace_log.txt", "float_log.txt",  "meteor_log.txt",  "pidigits_log.txt",  "spectral_log.txt", "sci_mark_log.txt"]:
    with open(n, 'r') as f:
        for i,l in enumerate(f.readlines()):
            if (i % 2 == 0):
                name = l.rstrip()            
            else:
                if (name in timeDict):
                    ourTime = float(l)
                    reticTime = timeDict[name]
                    dt_ratio = ourTime / reticTime
                    entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime, reticTime/ourTime))
                    configuration_ratios.append(reticTime/ourTime)
                else:
                    retic_time = float(l)
                    timeDict[name] = retic_time



sorted_ratios = sorted(configuration_ratios)
total_configs = len(sorted_ratios)
slower = list(itertools.takewhile(lambda x: x <= 1, sorted_ratios))
less_than_1 = len(slower)
slower_ratios = sorted([1/x for x in slower])
slower_distributions = [len(list(filter(lambda x: x <= 1 + (i+2)/100  and x > 1 + i/100, slower_ratios))) for i in range(0, 12, 2)]
distributions = [len(list(filter(lambda x: x<= i+3  and x > i, sorted_ratios))) for i in range(4, 25, 3)]
between_1_and_2 = len(list(filter(lambda x: x <= 2  and x > 1, sorted_ratios)))
between_2_and_3 = len(list(filter(lambda x: x <= 3  and x > 2, sorted_ratios)))
between_3_and_4 = len(list(filter(lambda x: x <= 4  and x > 3, sorted_ratios)))
    

greater_than_40 = len(list(filter(lambda x: x >= 40, sorted_ratios)))
final_distributions =  [between_1_and_2, between_2_and_3, between_3_and_4] + distributions + [greater_than_40]
x_vals = [-5, -2, 1] + list(range(4, 25, 3)) + [40]
x_labels = ["1-2", "2-3", "3-4"] + [str(i) + "-" + str(i+3) for i in range(4,25,3)] + [">40"]

slow_x_labels = [str(1+i/100) + "-" + str(1+(i+2)/100) for i in range(0, 12, 2)]

fR, (ax) = plt.subplots(figsize=(8,6))
ax.hist(list(itertools.takewhile(lambda x: x <= 40, sorted_ratios)), bins=20)
#ax1.bar(list(range(0, 12, 2)), slower_distributions, width=1.5, linewidth=2, edgecolor="black")

#ax.bar(x_vals, height=final_distributions, width=2, linewidth=2, edgecolor="black")
ax.set_ylim(bottom=0, top=1400)
plt.xticks(x_vals, x_labels)
plt.xticks(rotation=60)

plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
#ylocs, ylabels = plt.yticks()
#plt.yticks(ylocs, [" " for _ in range(8)])
#ax.get_yaxis().set_visible(False)
name = "../plots/histogram"
plt.ylabel(" ", fontsize=18)
plt.xlabel("Speedup of DT over Reticulated", fontsize=18)

average_configs = sum(distributions)
small_configs = sum([between_1_and_2, between_2_and_3, between_3_and_4])
print("The total number of configurations is: {0}".format(total_configs))
print("The percentage of configurations with a slowdown are: {0}".format(less_than_1/total_configs))
print("The percentage of configurations with a 1-4x speedup are: {0}".format(small_configs/total_configs))
print("The percentage of configurations with 4-25x speedup are: {0}".format(average_configs/total_configs))
print("The percentage of configurations with a >40x speedup is: {0}".format(greater_than_40/total_configs))

#plt.savefig(name + '.pdf', bbox_inches='tight')

plt.show()


