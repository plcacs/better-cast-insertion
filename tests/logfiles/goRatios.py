from operator import itemgetter
import matplotlib.pyplot as plt
import itertools
import math
import pdb

timeDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0

def round2(x): return round(x, 2)

max_speedup = 1
reticulated_ratios = []
dt_ratios = []
entries = []
dynamic_time = 4.478355169296265
with open("go_log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l.rstrip()            
        else:
            if (name in timeDict):
                ourTime = float(l)
                reticTime = timeDict[name]
                dt_ratio = ourTime / dynamic_time
                dt_ratios.append(dt_ratio)
                entries.append((name, reticTime, ourTime, (reticTime-ourTime)/reticTime, reticTime/ourTime))

            else:
                retic_time = float(l)
                retic_ratio = retic_time / dynamic_time
                reticulated_ratios.append(retic_ratio)
                timeDict[name] = retic_time
                if (noRepeat):
                    totalCount += 1

sorted_retic_ratios = sorted(reticulated_ratios)
sorted_dt_ratios = sorted(dt_ratios)
max_slowdown = max(sorted_retic_ratios[-1], sorted_dt_ratios[-1])

retic_percentage_list = []
dt_percentage_list = []
retic_length = len(sorted_retic_ratios)
dt_length = len(sorted_dt_ratios)
for i in range(1, math.ceil(max_slowdown) + 1):
    retic_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_retic_ratios))
    dt_number_slower = list(itertools.takewhile(lambda x: x <= i, sorted_dt_ratios))
    #speedup_dict[i] = faster_list
    retic_percentage_list.append((len(retic_number_slower) / retic_length) * 100)
    dt_percentage_list.append((len(dt_number_slower) / dt_length) * 100)


fR, ax = plt.subplots() 
ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), dt_percentage_list, facecolor="#f7f1b4", edgecolor="black")
ax.fill_between(list(range(1,math.ceil(max_slowdown) + 1)), retic_percentage_list, facecolor="#72a4d4", edgecolor = "black")



plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
name="../plots/goRatios"
ax.set_title("go (1024 configurations)", fontdict={'fontsize':20})
plt.savefig(name + '.pdf', bbox_inches='tight')
#plt.show()


