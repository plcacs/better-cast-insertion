
import time
def ident(x):
    return x
def even(n:Float, k):
    return k(True) if (n <= 0) else odd(n-1, k)
def odd(n, k):
    return k(False) if (n <= 0) else even(n-1, k)

def main():    
        for i in range(1000):
            even(50, ident)

start = time.time()
main()
end = time.time()
print(end-start)
