start = """
import time
"""
ident_headers = ["def ident(x):", "def ident(x:Bool) -> Bool:"]
ident_body = """
    return x
"""

even_headers = ["def even(n, k):","def even(n:Float, k):", "def even(n, k:Function([Bool],Bool)):", "def even(n:Float, k:Function([Bool],Bool)):"]
even_body = """
    return k(True) if (n <= 0) else odd(n-1, k)
"""

odd_headers = ["def odd(n, k):", "def odd(n:Float, k):", "def odd(n, k:Function([Bool],Bool)):", "def odd(n:Float, k:Function([Bool],Bool)):"]
odd_body = """
    return k(False) if (n <= 0) else even(n-1, k)
"""
rest = """
def main():    
        for i in range(1000):
            even(50, ident)

start = time.time()
main()
end = time.time()
print(end-start)
"""
index = 0
for i in ident_headers:
    for j in even_headers:
        for k in odd_headers:
            index += 1
            prog = start + i + ident_body + j + even_body + k + odd_body + rest
            name = "even" + str(index) + ".py"
            with open(name, 'w+') as f:
                f.write(prog)
