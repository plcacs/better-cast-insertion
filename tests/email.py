def width(fixed,numChrs:Int,wdF, adjF):
      if not fixed:
            return wdF(fixed)
      else:
            return wdF(numChrs)+adjF(numChrs)

def succ(x:Int):
    return x + 1

width("",3,succ,succ)
