timeDict = {}
fasterCount = 0
noRepeat = True
totalCount = 0
with open("log.txt", 'r') as f:
    for i,l in enumerate(f.readlines()):
        if (i % 2 == 0):
            name = l            
        else:
            if (name in timeDict):
                if timeDict[name] >= float(l):
                    fasterCount += 1
                    noRepeat = False
            else:                
                timeDict[name] = float(l)
                if (noRepeat):
                    totalCount += 1



print ("Our approach was faster for: ", fasterCount, "/", totalCount)
