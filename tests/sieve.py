import time

# Simple streams library.
# For building and using infinite lists.


# # A stream is a cons of a value and a thunk that computes the next value when applied
#(struct: stream ([first : Natural] [rest : (-> stream)]))
class Stream:
    first = None
    rest = None
    def __init__(self, first, rest):
        self.first = first
        self.rest = rest
#--------------------------------------------------------------------------------------------------

def stream_first(st):
    return st.first

def stream_rest(st):
    return st.rest

#(: make-stream (-> Natural (-> stream) stream))
def make_stream (hd, thunk):
    return Stream(hd, thunk)

# Destruct a stream into its first value and the new stream produced by de-thunking the tail
#(: stream-unfold (-> stream (values Natural stream)))
def stream_unfold(st):
    return (stream_first(st), stream_rest(st)())

# [stream-get st i] Get the [i]-th element from the stream [st]
#(: stream-get (-> stream Natural Natural))
def stream_get(st, i):
    (hd, tl) = stream_unfold(st)
    return (hd if i == 0 else stream_get(tl, i-1))

# [stream-take st n] Collect the first [n] elements of the stream [st].
#(: stream-take (-> stream Natural (Listof Natural)))
def stream_take(st, n):
     if n == 0:
         return []
     else:
         (hd, tl) = stream-unfold(st)
         return [hd] + stream_take(tl, n-1)

#lang typed/racket/base

# Use the partner file "streams.rkt" to implement the Sieve of Eratosthenes.
# Then compute and print the 10,000th prime number.
"""
(require benchmark-util)

(require/typed/check "streams.rkt"
  [#:struct stream ([first : Natural]
                    [rest : (-> stream)])]
  [make-stream (-> Natural (-> stream) stream)]
  [stream-unfold (-> stream (values Natural stream))]
  [stream-get (-> stream Natural Natural)]
  [stream-take (-> stream Natural (Listof Natural))])
"""

#--------------------------------------------------------------------------------------------------

# `count-from n` Build a stream of integers starting from `n` and iteratively adding 1
#(: count-from (-> Natural stream))
def count_from(n):
  return make_stream(n, lambda: count_from(n+1))

# `sift n st` Filter all elements in `st` that are equal to `n`.
# Return a new stream.
#(: sift (-> Natural stream stream))
def sift(n, st):
    (hd, tl) = stream_unfold(st)
    return (sift(n, tl) if hd % n == 0 else make_stream(hd, (lambda: sift(n, tl))))

# `sieve st` Sieve of Eratosthenes
#(: sieve (-> stream stream))
def sieve(st):
    (hd, tl) = stream_unfold(st)
    return make_stream(hd, (lambda: sieve(sift(hd, tl))))

# stream of prime numbers
#(: primes stream)
def primes():
    return sieve(count_from(2))

# Compute the 10,000th prime number
#(: N-1 Natural)
N_1 = 19

#(: main (-> Void))
def main():
  stream_get(primes(), N_1)

t0 = time.time()
for i in range(10):
    main()
t1 = time.time()
print(t1-t0)
            
#(time (main))
