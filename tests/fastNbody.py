from retic.runtime import *
from retic.guarded import *
from retic.typing import *
from six.moves import xrange
import pdb
from itertools import islice
import time
__contact__ = 'collinwinter@google.com (Collin Winter)'
DEFAULT_ITERATIONS = 10000
DEFAULT_REFERENCE = 'sun'

def combinations(l):
    result = retic_cast([], List(Dyn), Dyn, '\nnbody.py:21:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for x in xrange(retic_cast((len(retic_cast(l, List(Dyn), Dyn, '\nnbody.py:22:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')) - 1), Int, Dyn, '\nnbody.py:22:13: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')):
        ls = retic_cast(islice, Dyn, Function(AnonymousParameters([List(Dyn), Dyn, Int]), Dyn), "\nnbody.py:23:13: Expected function of type Function(['List(Dyn)', 'Dyn', 'Int'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(l, (x + 1), len(retic_cast(l, List(Dyn), Dyn, '\nnbody.py:23:26: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)')))
        for y in ls:
            retic_cast(result.append, Dyn, Function(AnonymousParameters([Tuple(Dyn, Dyn)]), Dyn), "\nnbody.py:25:12: Expected function of type Function(['Tuple(Dyn,Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((l[retic_cast(x, Dyn, Int, '\nnbody.py:25: Cannot use a value %s as an index into a List(Dyn), use a value of type Int instead. (code BAD_INDEX)')], y))
    return retic_cast(result, Dyn, List(Dyn), '\nnbody.py:26:4: A return value of type List(Dyn) was expected but a value %s was returned instead. (code RETURN_ERROR)')
combinations = combinations

PI = 3.141592653589793
SOLAR_MASS = ((4 * PI) * PI)
DAYS_PER_YEAR = 365.24
BODIES = {'sun': ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], SOLAR_MASS), 'jupiter': ([4.841431442464721, (- 1.1603200440274284), (- 0.10362204447112311)], [(0.001660076642744037 * DAYS_PER_YEAR), (0.007699011184197404 * DAYS_PER_YEAR), ((- 6.90460016972063e-05) * DAYS_PER_YEAR)], (0.0009547919384243266 * SOLAR_MASS)), 'saturn': ([8.34336671824458, 4.124798564124305, (- 0.4035234171143214)], [((- 0.002767425107268624) * DAYS_PER_YEAR), (0.004998528012349172 * DAYS_PER_YEAR), (2.3041729757376393e-05 * DAYS_PER_YEAR)], (0.0002858859806661308 * SOLAR_MASS)), 'uranus': ([12.894369562139131, (- 15.111151401698631), (- 0.22330757889265573)], [(0.002964601375647616 * DAYS_PER_YEAR), (0.0023784717395948095 * DAYS_PER_YEAR), ((- 2.965895685402b3756e-05) * DAYS_PER_YEAR)], (4.366244043351563e-05 * SOLAR_MASS)), 'neptune': ([15.379697114850917, (- 25.919314609987964), 0.17925877295037118], [(0.0026806777249038932 * DAYS_PER_YEAR), (0.001628241700382423 * DAYS_PER_YEAR), ((- 9.515922545197159e-05) * DAYS_PER_YEAR)], (5.1513890204661145e-05 * SOLAR_MASS)), }
SYSTEM = list(retic_cast(retic_cast(BODIES.values, Dyn, Function(AnonymousParameters([]), Dyn), '\nnbody.py:69:14: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')(), Function(AnonymousParameters([]), Dyn), Dyn, '\nnbody.py:69:9: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
PAIRS = combinations(SYSTEM)

def advance(dt, n, bodies, pairs):
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1), ([x2, y2, z2], v2, m2)) in pairs:
            dx = ((0.0 + x1) - x2)
            dy = ((0.0 + y1) - y2)
            dz = ((0.0 + z1) - z2)
            mag = ((1.0 * dt) * (((((1.0 * dx) * dx) + ((1.0 * dy) * dy)) + ((1.0 * dz) * dz)) ** (- 1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] = (v1[0] - (dx * b2m))
            v1[1] = (v1[1] - (dy * b2m))
            v1[2] = (v1[2] - (dz * b2m))
            v2[0] = (v2[0] + (dx * b1m))
            v2[1] = (v2[1] + (dy * b1m))
            v2[2] = (v2[2] + (dz * b1m))
        for (r, [vx, vy, vz], m) in bodies:
            r[0] = (r[0] + (dt * vx))
            r[1] = (r[1] + (dt * vy))
            r[2] = (r[2] + (dt * vz))
    return None
advance = advance

def report_energy(bodies, pairs, e):
    for (((x1, y1, z1), v1, m1), ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e = (e - ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz)) ** 0.5)))
    for (r, [vx, vy, vz], m) in bodies:
        e = (e + ((m * (((vx * vx) + (vy * vy)) + (vz * vz))) / 2.0))
    return e
report_energy = report_energy

def offset_momentum(ref, bodies, px, py, pz):
    for (r, [vx, vy, vz], m) in bodies:
        px = (px - (vx * m))
        py = (py - (vy * m))
        pz = (pz - (vz * m))
    (r, v, m) = retic_cast(ref, Dyn, Tuple(Dyn, Dyn, Dyn), '\nnbody.py:134:4: Right hand side of assignment was expected to be of type Tuple(Dyn,Dyn,Dyn), but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    a = retic_cast([((0.0 + px) / m), ((0.0 + py) / m), ((0.0 + pz) / m)], List(Dyn), Dyn, '\nnbody.py:135:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    return None
offset_momentum = offset_momentum

def bench_nbody(loops, reference, iterations):
    offset_momentum(retic_cast(BODIES[retic_cast(reference, Dyn, String, '\nnbody.py:146: Cannot use a value %s as an index into a Dict(String, Tuple(List(Float),List(Float),Float)), use a value of type String instead. (code BAD_INDEX)')], Tuple(List(Float), List(Float), Float), Dyn, '\nnbody.py:146:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(SYSTEM, List(Dyn), Dyn, '\nnbody.py:146:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(0.0, Float, Dyn, '\nnbody.py:146:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(0.0, Float, Dyn, '\nnbody.py:146:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(0.0, Float, Dyn, '\nnbody.py:146:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    range_it = retic_cast(xrange(loops), Iterable(Int), Dyn, '\nnbody.py:148:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for _ in range_it:
        report_energy(retic_cast(SYSTEM, List(Dyn), Dyn, '\nnbody.py:151:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(PAIRS, List(Dyn), Dyn, '\nnbody.py:151:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(0.0, Float, Dyn, '\nnbody.py:151:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
        advance(retic_cast(0.01, Float, Dyn, '\nnbody.py:152:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), iterations, retic_cast(SYSTEM, List(Dyn), Dyn, '\nnbody.py:152:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(PAIRS, List(Dyn), Dyn, '\nnbody.py:152:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
        report_energy(retic_cast(SYSTEM, List(Dyn), Dyn, '\nnbody.py:153:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(PAIRS, List(Dyn), Dyn, '\nnbody.py:153:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(0.0, Float, Dyn, '\nnbody.py:153:8: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    return None
bench_nbody = bench_nbody

def main():
    bench_nbody(retic_cast(1, Int, Dyn, '\nnbody.py:157:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(DEFAULT_REFERENCE, String, Dyn, '\nnbody.py:157:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(DEFAULT_ITERATIONS, Int, Dyn, '\nnbody.py:157:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
main = main

t0 = retic_cast(time.time, Dyn, Function(AnonymousParameters([]), Dyn), '\nnbody.py:160:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
main()
t1 = retic_cast(time.time, Dyn, Function(AnonymousParameters([]), Dyn), '\nnbody.py:162:5: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
retic_cast(print, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nnbody.py:163:0: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((t1 - t0))
