import random
from functools import reduce
start = """
import math
import random
import time
import perf


SIZE = 9
GAMES = 2#200
KOMI = 7.5
EMPTY, WHITE, BLACK = 0, 1, 2
SHOW = {EMPTY: '.', WHITE: 'o', BLACK: 'x'}
PASS = -1
MAXMOVES = SIZE * SIZE * 3
TIMESTAMP = 0
MOVES = 0

"""
to_pos_headers = ["def to_pos(x, y):", "def to_pos(x:int,y)->int:", "def to_pos(x,y:int)->int:",  "def to_pos(x:int,y:int)->int:"]
to_pos_body = """
    return y * SIZE + x
"""


to_xy_headers = ["def to_xy(pos):", "def to_xy(pos:int)->(int, int):"]
to_xy_body = """
    y, x = divmod(pos, SIZE)
    return x, y
"""


square = """class Square:
"""

square_init_headers = ["def __init__(self, board, pos):", "def __init__(self, board:Board, pos):", "def __init__(self, board:Board, pos):", "def __init__(self, board:Board, pos:int):"]
square_init_body = """
        self.board = board
        self.pos = pos
        self.timestamp = TIMESTAMP
        self.removestamp = TIMESTAMP
        self.zobrist_strings = [random.randrange(9223372036854775807)
                                for i in range(3)]
"""
    
square_set_neighbors_headers = ["def set_neighbours(self):"]
square_set_neighbors_body = """
        x, y = self.pos % SIZE, self.pos // SIZE
        self.neighbours = []
        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            newx, newy = x + dx, y + dy
            if 0 <= newx < SIZE and 0 <= newy < SIZE:
                self.neighbours.append(self.board.squares[to_pos(newx, newy)])
"""
        
square_move_headers = ["def move(self, color):", "def move(self, color:int):"]
square_move_body = """
        global TIMESTAMP, MOVES
        TIMESTAMP += 1
        MOVES += 1
        self.board.zobrist.update(self, color)
        self.color = color
        self.reference = self
        self.ledges = 0
        self.used = True
        for neighbour in self.neighbours:
            neighcolor = neighbour.color
            if neighcolor == EMPTY:
                self.ledges += 1
            else:
                neighbour_ref = neighbour.find(update=True)
                if neighcolor == color:
                    if neighbour_ref.reference.pos != self.pos:
                        self.ledges += neighbour_ref.ledges
                        neighbour_ref.reference = self
                    self.ledges -= 1
                else:
                    neighbour_ref.ledges -= 1
                    if neighbour_ref.ledges == 0:
                        neighbour.remove(neighbour_ref)
        self.board.zobrist.add()
"""

square_remove_headers = ["def remove(self, reference, update=True):", "def remove(self, reference:Square, update=True):"]
square_remove_body = """
        self.board.zobrist.update(self, EMPTY)
        self.removestamp = TIMESTAMP
        if update:
            self.color = EMPTY
            self.board.emptyset.add(self.pos)
#            if color == BLACK:
#                self.board.black_dead += 1
#            else:
#                self.board.white_dead += 1
        for neighbour in self.neighbours:
            if neighbour.color != EMPTY and neighbour.removestamp != TIMESTAMP:
                neighbour_ref = neighbour.find(update)
                if neighbour_ref.pos == reference.pos:
                    neighbour.remove(reference, update)
                else:
                    if update:
                        neighbour_ref.ledges += 1
"""

square_find_headers = ["def find(self, update=False):", "def find(self, update=False)->Square:"]
square_find_body = """
        reference = self.reference
        if reference.pos != self.pos:
            reference = reference.find(update)
            if update:
                self.reference = reference
        return reference
"""

square_repr_headers = ["def __repr__(self):"]
square_repr_body = """
        return repr(to_xy(self.pos))
"""


empty_set = """class EmptySet:
"""

empty_set_init_headers = ["def __init__(self, board):", "def __init__(self, board:{'useful':Function(NamedParameters([('pos',int)]), int)}):"]
empty_set_init_body = """
        self.board = board
        self.empties = list(range(SIZE * SIZE))
        self.empty_pos = list(range(SIZE * SIZE))
"""

empty_set_random_choice_headers = ["def random_choice(self):", "def random_choice(self:EmptySet)->int:"]
empty_set_random_choice_body = """
        choices = len(self.empties)
        while choices:
            i = int(random.random() * choices)
            pos = self.empties[i]
            if self.board.useful(pos):
                return pos
            choices -= 1
            self.setter(i, self.empties[choices])
            self.setter(choices, pos)
        return PASS
"""

empty_set_add_headers = ["def add(self, pos):", "def add(self:EmptySet, pos):", "def add(self, pos:int):"]
empty_set_add_body = """
        self.empty_pos[pos] = len(self.empties)
        self.empties.append(pos)
"""

empty_set_remove_headers = ["def remove(self, pos):", "def remove(self:EmptySet, pos):", "def remove(self, pos:int):"]
empty_set_remove_body = """
        self.setter(self.empty_pos[pos], self.empties[len(self.empties) - 1])
        self.empties.pop()
"""

empty_set_setter_headers = ["def setter(self, i, pos):"
                               , "def setter(self:EmptySet, i, pos):"
                               , "def setter(self, i:int, pos):"
                               , "def setter(self, i, pos:int):"
                               , "def setter(self:EmptySet, i:int, pos):"
                               , "def setter(self:EmptySet, i, pos:int):"
                               , "def setter(self, i:int, pos:int):"
                               ,"def setter(self:EmptySet, i:int, pos:int):"]
empty_set_setter_body = """
        self.empties[i] = pos
        self.empty_pos[pos] = i
"""


zobrist_hash = """class ZobristHash:
"""

zobrist_hash_init_headers = ["def __init__(self, board):", "def __init__(self, board:{'squares':List(Square)}):"]
zobrist_hash_init_body = """
        self.board = board
        self.hash_set = set()
        self.hash = 0
        for square in self.board.squares:
            self.hash ^= square.zobrist_strings[EMPTY]
        self.hash_set.clear()
        self.hash_set.add(self.hash)
"""

zobrist_hash_update_headers = ["def update(self, square, color):"
                           , "def update(self:ZobristHash, square, color):"
                           , "def update(self, square:Square, color):"
                           , "def update(self, square, color:int):"
                           , "def update(self:ZobristHash, square:Square, color):"
                           , "def update(self:ZobristHash, square, color:int):"
                           , "def update(self, square:Square, color:int):"
                           , "def update(self:ZobristHash, square:Square, color:int):"]
zobrist_hash_update_body = """
        self.hash ^= square.zobrist_strings[square.color]
        self.hash ^= square.zobrist_strings[color]
"""

zobrist_hash_add_headers = ["def add(self):", "def add(self:ZobristHash):"]
zobrist_hash_add_body = """
        self.hash_set.add(self.hash)
"""
zobrist_hash_dupe_headers = ["def dupe(self):", "def dupe(self:ZobristHash)->bool:"]
zobrist_hash_dupe_body = """
        return self.hash in self.hash_set
"""


board = """class Board:
"""

board_init_headers = ["def __init__(self):"]
board_init_body = """
        self.squares = [Square(self, pos) for pos in range(SIZE * SIZE)]
        for square in self.squares:
            square.set_neighbours()
        self.reset()
"""

board_reset_headers = ["def reset(self):"]
board_reset_body = """
        for square in self.squares:
            square.color = EMPTY
            square.used = False
        self.emptyset = EmptySet(self)
        self.zobrist = ZobristHash(self)
        self.color = BLACK
        self.finished = False
        self.lastmove = -2
        self.history = []
        self.white_dead = 0
        self.black_dead = 0
"""

board_move_headers = ["def move(self, pos):", "def move(self:Board, pos):", "def move(self, pos:int):", "def move(self:Board, pos:int):"]
board_move_body = """
        square = self.squares[pos]
        if pos != PASS:
            square.move(self.color)
            self.emptyset.remove(square.pos)
        elif self.lastmove == PASS:
            self.finished = True
        if self.color == BLACK:
            self.color = WHITE
        else:
            self.color = BLACK
        self.lastmove = pos
        self.history.append(pos)
"""

board_random_move_headers = ["def random_move(self):", "def random_move(self:Board)->int:"]
board_random_move_body = """
        return self.emptyset.random_choice()
"""

board_useful_fas_headers = ["def useful_fas(self, square):"
                                , "def useful_fas(self:Board, square)->bool:"
                                , "def useful_fas(self, square:Square)->bool:"
                                , "def useful_fas(self:Board, square:Square)->bool:"]
board_useful_fas_body = """
        if not square.used:
            for neighbour in square.neighbours:
                if neighbour.color == EMPTY:
                    return True
        return False
"""

board_useful_headers = ["def useful(self, pos):"
                            , "def useful(self:Board, pos)->int:"
                            , "def useful(self, pos:int)->int:"
                            , "def useful(self:Board, pos:int)->int:"]
board_useful_body = """
        global TIMESTAMP
        TIMESTAMP += 1
        square = self.squares[pos]
        if self.useful_fas(square):
            return True
        old_hash = self.zobrist.hash
        self.zobrist.update(square, self.color)
        empties = opps = weak_opps = neighs = weak_neighs = 0
        for neighbour in square.neighbours:
            neighcolor = neighbour.color
            if neighcolor == EMPTY:
                empties += 1
                continue
            neighbour_ref = neighbour.find()
            if neighbour_ref.timestamp != TIMESTAMP:
                if neighcolor == self.color:
                    neighs += 1
                else:
                    opps += 1
                neighbour_ref.timestamp = TIMESTAMP
                neighbour_ref.temp_ledges = neighbour_ref.ledges
            neighbour_ref.temp_ledges -= 1
            if neighbour_ref.temp_ledges == 0:
                if neighcolor == self.color:
                    weak_neighs += 1
                else:
                    weak_opps += 1
                    neighbour_ref.remove(neighbour_ref, update=False)
        dupe = self.zobrist.dupe()
        self.zobrist.hash = old_hash
        strong_neighs = neighs - weak_neighs
        strong_opps = opps - weak_opps
        return not dupe and \
            (empties or weak_opps or (strong_neighs and (strong_opps or weak_neighs)))
"""

board_useful_moves_headers = ["def useful_moves(self):"
                                  , "def useful_moves(self:Board)->List(int):"]
board_useful_moves_body = """
        return [pos for pos in self.emptyset.empties if self.useful(pos)]
"""

board_replay_headers =  ["def replay(self, history):"
                                  , "def replay(self:Board, history):"
                                  , "def replay(self, history:List(int)):"
                                  , "def replay(self:Board, history:List(int)):"]
board_replay_body = """
        for pos in history:
            self.move(pos)
"""

board_score_headers = ["def score(self, color):"
                           , "def score(self:Board, color)->float:"
                           , "def score(self, color:int)->float:"]
board_score_body = """
        if color == WHITE:
            count = KOMI + self.black_dead
        else:
            count = self.white_dead
        for square in self.squares:
            squarecolor = square.color
            if squarecolor == color:
                count += 1
            elif squarecolor == EMPTY:
                surround = 0
                for neighbour in square.neighbours:
                    if neighbour.color == color:
                        surround += 1
                if surround == len(square.neighbours):
                    count += 1
        return count
"""

board_check_headers =  ["def check(self):", "def check(self:Board):"]
board_check_body = """
        for square in self.squares:
            if square.color == EMPTY:
                continue

            members1 = set([square])
            changed = True
            while changed:
                changed = False
                for member in members1.copy():
                    for neighbour in member.neighbours:
                        if neighbour.color == square.color and neighbour not in members1:
                            changed = True
                            members1.add(neighbour)
            ledges1 = 0
            for member in members1:
                for neighbour in member.neighbours:
                    if neighbour.color == EMPTY:
                        ledges1 += 1

            root = square.find()

            # print 'members1', square, root, members1
            # print 'ledges1', square, ledges1

            members2 = set()
            for square2 in self.squares:
                if square2.color != EMPTY and square2.find() == root:
                    members2.add(square2)

            ledges2 = root.ledges
            # print 'members2', square, root, members1
            # print 'ledges2', square, ledges2

            assert members1 == members2
            assert ledges1 == ledges2, ('ledges differ at %r: %d %d' % (
                square, ledges1, ledges2))

            set(self.emptyset.empties)

            empties2 = set()
            for square in self.squares:
                if square.color == EMPTY:
                    empties2.add(square.pos)
"""

board_repr_headers = ["def __repr__(self):", "def __repr__(self:Board):"]
board_repr_body = """
        result = []
        for y in range(SIZE):
            start = to_pos(0, y)
            result.append(''.join(
                [SHOW[square.color] + ' ' for square in self.squares[start:start + SIZE]]))
        return '\\n'.join(result)
"""

UCTNode = """class UCTNode:
"""

UCTNode_init_headers = ["def __init__(self):"]
UCTNode_init_body = """
        self.bestchild = None
        self.pos = -1
        self.wins = 0
        self.losses = 0
        self.pos_child = [None for x in range(SIZE * SIZE)]
        self.parent = None
"""

UCTNode_play_headers =  ["def play(self, board):", "def play(self:UCTNode, board):", "def play(self, board:Board):", "def play(self:UCTNode, board:Board):"]
UCTNode_play_body = """
        color = board.color
        node = self
        path = [node]
        while True:
            pos = node.select(board)
            if pos == PASS:
                break
            board.move(pos)
            child = node.pos_child[pos]
            if not child:
                child = node.pos_child[pos] = UCTNode()
                child.unexplored = board.useful_moves()
                child.pos = pos
                child.parent = node
                path.append(child)
                break
            path.append(child)
            node = child
        self.random_playout(board)
        self.update_path(board, color, path)
"""

UCTNode_select_headers = ["def select(self, board):", "def select(self:UCTNode, board)->int:", "def select(self:UCTNode, board)->int:", "def select(self:UCTNode, board:Board)->int:"]
UCTNode_select_body = """
        if self.unexplored:
            i = random.randrange(len(self.unexplored))
            pos = self.unexplored[i]
            self.unexplored[i] = self.unexplored[len(self.unexplored) - 1]
            self.unexplored.pop()
            return pos
        elif self.bestchild:
            return self.bestchild.pos
        else:
            return PASS
"""

UCTNode_random_playout_headers = ["def random_playout(self, board):"
                                     , "def random_playout(self:UCTNode, board):"
                                     , "def random_playout(self, board:Board):"
                                     , "def random_playout(self:UCTNode, board:Board):"]
UCTNode_random_playout_body = """
        for x in range(MAXMOVES):  # XXX while not self.finished?
            if board.finished:
                break
            board.move(board.random_move())
"""

UCTNode_update_path_headers =  ["def update_path(self, board, color, path):"
                                   , "def update_path(self:UCTNode, board, color, path):"
                                   , "def update_path(self, board:Board, color, path):"
                                   , "def update_path(self, board, color:int, path):"
                                   , "def update_path(self, board, color, path:List(UCTNode)):"
                                   , "def update_path(self:UCTNode, board:Board, color, path):"
                                   , "def update_path(self:UCTNode, board, color:int, path):"
                                   , "def update_path(self:UCTNode, board, color, path:List(UCTNode)):"
                                   , "def update_path(self, board:Board, color:int, path):"
                                   , "def update_path(self, board:Board, color, path:List(UCTNode)):"
                                   , "def update_path(self, board, color:int, path:List(UCTNode)):"
                                   , "def update_path(self:UCTNode, board:Board, color:int, path):"
                                   , "def update_path(self:UCTNode, board, color:int, path:List(UCTNode)):"
                                   , "def update_path(self:UCTNode, board:Board, color, path:List(UCTNode)):"
                                   , "def update_path(self, board:Board, color:int, path:List(UCTNode)):"
                                   , "def update_path(self:UCTNode, board:Board, color:int, path:List(UCTNode)):"]
UCTNode_update_path_body = """
        wins = board.score(BLACK) >= board.score(WHITE)
        for node in path:
            if color == BLACK:
                color = WHITE
            else:
                color = BLACK
            if wins == (color == BLACK):
                node.wins += 1
            else:
                node.losses += 1
            if node.parent:
                node.parent.bestchild = node.parent.best_child()
"""

UCTNode_score_headers = ["def score(self):", "def score(self:UCTNode)->float:"]
UCTNode_score_body = """
        winrate = self.wins / float(self.wins + self.losses)
        parentvisits = self.parent.wins + self.parent.losses
        if not parentvisits:
            return winrate
        nodevisits = self.wins + self.losses
        return winrate + math.sqrt((math.log(parentvisits)) / (5 * nodevisits))
"""

UCTNode_best_child_headers = ["def best_child(self):", "def best_child(self:UCTNode)->UCTNode:"]
UCTNode_best_child_body = """
        maxscore = -1
        maxchild = None
        for child in self.pos_child:
            if child and child.score() > maxscore:
                maxchild = child
                maxscore = child.score()
        return maxchild
"""

UCTNode_best_visited_headers = ["def best_visited(self):", "def best_visited(self:UCTNode)->UCTNode:"]
UCTNode_best_visited_body = """
        maxvisits = -1
        maxchild = None
        for child in self.pos_child:
            #            if child:
            # print to_xy(child.pos), child.wins, child.losses, child.score()
            if child and (child.wins + child.losses) > maxvisits:
                maxvisits, maxchild = (child.wins + child.losses), child
        return maxchild
"""


# def user_move(board):
#     while True:
#         text = six.moves.input('?').strip()
#         if text == 'p':
#             return PASS
#         if text == 'q':
#             raise EOFError
#         try:
#             x, y = [int(i) for i in text.split()]
#         except ValueError:
#             continue
#         if not (0 <= x < SIZE and 0 <= y < SIZE):
#             continue
#         pos = to_pos(x, y)
#         if board.useful(pos):
#             return pos


computer_move_headers = ["def computer_move(board):"
                         , "def computer_move(board:{'useful_moves':Function([], List(int)), 'random_move':Function([], int), 'history':List(int)})->int:"]
computer_move_body = """
    pos = board.random_move()
    if pos == PASS:
        return PASS
    tree = UCTNode()
    tree.unexplored = board.useful_moves()
    nboard = Board()
    for game in range(GAMES):
        node = tree
        nboard.reset()
        nboard.replay(board.history)
        node.play(nboard)
    return tree.best_visited().pos
"""

rest = """
def versus_cpu():
    random.seed(1)
    board = Board()
    return computer_move(board)


if __name__ == "__main__":
    kw = {}
    #if perf.python_has_jit():
        # PyPy needs to compute more warmup values to warmup its JIT
    #    kw['warmups'] = 50
    t0 = time.time()
    versus_cpu()
    t1 = time.time()
    print(t1-t0)
    #runner = perf.Runner(**kw)
    #runner.metadata['description'] = "Test the performance of the Go benchmark"
    #runner.bench_func('go', versus_cpu)


"""
def random_element(collection):
    return collection[random.randrange(0, len(collection))]
i = 0
while i < 1024:
    to_pos_header = random_element(to_pos_headers)
    to_xy_header = random_element(to_xy_headers)
    square_init_header = "  " + random_element(square_init_headers)
    square_set_neighbors_header = "  " + random_element(square_set_neighbors_headers)
    square_move_header = "  " + random_element(square_move_headers)
    square_remove_header = "  " + random_element(square_remove_headers)
    square_find_header = "  " + random_element(square_find_headers)
    square_repr_header = "  " + random_element(square_repr_headers)
    empty_set_init_header = "  " + random_element(empty_set_init_headers)
    empty_set_random_choice_header = "  " + random_element(empty_set_random_choice_headers)
    empty_set_add_header = "  " + random_element(empty_set_add_headers)
    empty_set_remove_header = "  " + random_element(empty_set_remove_headers)
    empty_set_setter_header = "  " + random_element(empty_set_setter_headers)
    zobrist_hash_init_header = "  " + random_element(zobrist_hash_init_headers)
    zobrist_hash_update_header = "  " + random_element(zobrist_hash_update_headers)
    zobrist_hash_add_header = "  " + random_element(zobrist_hash_add_headers)
    zobrist_hash_dupe_header = "  " + random_element(zobrist_hash_dupe_headers)
    board_init_header = "  " + random_element(board_init_headers)
    board_reset_header = "  " + random_element(board_reset_headers)
    board_move_header = "  " + random_element(board_move_headers)
    board_random_move_header = "  " + random_element(board_random_move_headers)
    board_useful_fas_header = "  " + random_element(board_useful_fas_headers)
    board_useful_header = "  " + random_element(board_useful_headers)
    board_useful_moves_header = "  " + random_element(board_useful_moves_headers)
    board_replay_header = "  " + random_element(board_replay_headers)
    board_score_header = "  " + random_element(board_score_headers)
    board_check_header = "  " + random_element(board_check_headers)
    board_repr_header = "  " + random_element(board_repr_headers)
    UCTNode_init_header = "  " + random_element(UCTNode_init_headers)
    UCTNode_play_header = "  " + random_element(UCTNode_play_headers)
    UCTNode_select_header = "  " + random_element(UCTNode_select_headers)
    UCTNode_random_playout_header = "  " + random_element(UCTNode_random_playout_headers)
    UCTNode_update_path_header = "  " + random_element(UCTNode_update_path_headers)
    UCTNode_score_header = "  " + random_element(UCTNode_score_headers)
    UCTNode_best_child_header = "  " + random_element(UCTNode_best_child_headers)
    UCTNode_best_visited_header = "  " + random_element(UCTNode_best_visited_headers)
    computer_move_header = random_element(computer_move_headers)
    to_pos = to_pos_header + to_pos_body
    to_xy =             to_xy_header +             to_xy_body
    square_init =             square_init_header +             square_init_body
    square_set_neighbors =             square_set_neighbors_header +             square_set_neighbors_body
    square_move =             square_move_header +             square_move_body
    square_remove =             square_remove_header +             square_remove_body
    square_find =             square_find_header +             square_find_body
    square_repr =             square_repr_header +             square_repr_body
    empty_set_init =             empty_set_init_header +             empty_set_init_body
    empty_set_random_choice =             empty_set_random_choice_header +             empty_set_random_choice_body
    empty_set_add =             empty_set_add_header +             empty_set_add_body
    empty_set_remove =             empty_set_remove_header +             empty_set_remove_body
    empty_set_setter =             empty_set_setter_header +             empty_set_setter_body
    zobrist_hash_init =             zobrist_hash_init_header +             zobrist_hash_init_body
    zobrist_hash_update =             zobrist_hash_update_header +             zobrist_hash_update_body
    zobrist_hash_add =             zobrist_hash_add_header +             zobrist_hash_add_body
    zobrist_hash_dupe =             zobrist_hash_dupe_header +             zobrist_hash_dupe_body
    board_init =             board_init_header +             board_init_body
    board_reset =             board_reset_header +             board_reset_body
    board_move =             board_move_header +             board_move_body
    board_random_move =             board_random_move_header +             board_random_move_body
    board_useful_fas =             board_useful_fas_header +             board_useful_fas_body
    board_useful =             board_useful_header +             board_useful_body
    board_useful_moves =             board_useful_moves_header +             board_useful_moves_body
    board_replay =             board_replay_header +             board_replay_body
    board_score =             board_score_header +             board_score_body
    board_check =             board_check_header +             board_check_body
    board_repr =             board_repr_header +             board_repr_body
    UCTNode_init =             UCTNode_init_header +             UCTNode_init_body
    UCTNode_play =             UCTNode_play_header +             UCTNode_play_body
    UCTNode_select =             UCTNode_select_header +             UCTNode_select_body
    UCTNode_random_playout =             UCTNode_random_playout_header +             UCTNode_random_playout_body
    UCTNode_update_path =             UCTNode_update_path_header +             UCTNode_update_path_body
    UCTNode_score =             UCTNode_score_header +             UCTNode_score_body
    UCTNode_best_child =             UCTNode_best_child_header +             UCTNode_best_child_body
    UCTNode_best_visited =             UCTNode_best_visited_header +             UCTNode_best_visited_body
    computer_move =             computer_move_header +             computer_move_body

    prog = reduce(lambda x, y: x+y, [start,
                                     to_pos,
            to_xy,
            square,
            square_init,
            square_set_neighbors,
            square_move,
            square_remove,
            square_find,
            square_repr,
            empty_set,
            empty_set_init,
            empty_set_random_choice,
            empty_set_add,
            empty_set_remove,
            empty_set_setter,
            zobrist_hash,
            zobrist_hash_init,
            zobrist_hash_update,
            zobrist_hash_add,
            zobrist_hash_dupe,
            board,
            board_init,
            board_reset,
            board_move,
            board_random_move,
            board_useful_fas,
            board_useful,
            board_useful_moves,
            board_replay,
            board_score,
            board_check,
            board_repr,
            UCTNode,
            UCTNode_init,
            UCTNode_play,
            UCTNode_select,
            UCTNode_random_playout,
            UCTNode_update_path,
            UCTNode_score,
            UCTNode_best_child,
            UCTNode_best_visited,
            computer_move,
            rest
    ], "")
    programName = "go" + str(i) + ".py"
    with open(programName, 'w+') as f:
        f.write(prog)
    i += 1
