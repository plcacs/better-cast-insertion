from retic.runtime import *
from retic.guarded import *
from retic.typing import *
import random
import time
from array import array
import math
import perf
from six.moves import xrange

def int_log2(n):
    k = retic_cast(1, Int, Dyn, '\nfft.py:13:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    log = retic_cast(0, Int, Dyn, '\nfft.py:14:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    while (k < (n + 0)):
        k = (k * 2)
        log = (log + 1)
    if (n != (1 << log)):
        raise retic_cast(Exception, Dyn, Function(AnonymousParameters([String]), Dyn), "\nfft.py:19:14: Expected function of type Function(['String'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(('FFT: Data length is not a power of 2: %s' % n))
    return log
int_log2 = int_log2

def FFT_num_flops(N):
    return ((((5.0 * N) - 2) * int_log2(retic_cast(N, Dyn, Int, '\nfft.py:25:27: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)'))) + (2 * (N + 1)))
FFT_num_flops = FFT_num_flops

def FFT_transform_internal(N, data, direction):
    n = (N // 2)
    bit = retic_cast(0, Int, Dyn, '\nfft.py:32:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    dual = retic_cast(1, Int, Dyn, '\nfft.py:33:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    if (n == 1):
        return None
    logn = int_log2(retic_cast(n, Dyn, Int, '\nfft.py:37:11: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)'))
    if (N == 0):
        return None
    FFT_bitreverse(retic_cast(N, Dyn, Int, '\nfft.py:40:4: Expected argument of type Int but value %s was provided instead. (code ARG_ERROR)'), data)
    bit = retic_cast(0, Int, Dyn, '\nfft.py:44:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    while (bit < logn):
        w_real = retic_cast(1.0, Float, Dyn, '\nfft.py:46:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        w_imag = retic_cast(0.0, Float, Dyn, '\nfft.py:47:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        theta = (((2.0 * direction) * retic_cast(math, Dyn, Object('', {'pi': Dyn, }), '\nfft.py:48:34: Accessing nonexistant object attribute pi from value %s. (code WIDTH_DOWNCAST)').pi) / (2.0 * float(dual)))
        s = retic_cast(retic_cast(math, Dyn, Object('', {'sin': Dyn, }), '\nfft.py:49:12: Accessing nonexistant object attribute sin from value %s. (code WIDTH_DOWNCAST)').sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:49:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")(theta)
        t = retic_cast(retic_cast(math, Dyn, Object('', {'sin': Dyn, }), '\nfft.py:50:12: Accessing nonexistant object attribute sin from value %s. (code WIDTH_DOWNCAST)').sin, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:50:12: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((theta / 2.0))
        s2 = ((2.0 * t) * t)
        for b in range(retic_cast(0, Int, Dyn, '\nfft.py:52:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), n, (2 * dual)):
            i = (2 * b)
            j = (2 * (b + dual))
            wd_real = data[j]
            wd_imag = data[(j + 1)]
            data[j] = (data[i] - wd_real)
            data[(j + 1)] = (data[(i + 1)] - wd_imag)
            data[i] = (data[i] + wd_real)
            data[(i + 1)] = (data[(i + 1)] + wd_imag)
        for a in xrange(retic_cast(1, Int, Dyn, '\nfft.py:61:17: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), dual):
            tmp_real = ((w_real - (s * w_imag)) - (s2 * w_real))
            tmp_imag = ((w_imag + (s * w_real)) - (s2 * w_imag))
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(retic_cast(0, Int, Dyn, '\nfft.py:66:21: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), n, (2 * dual)):
                i = (2 * (b + a))
                j = (2 * ((b + a) + dual))
                z1_real = data[j]
                z1_imag = data[(j + 1)]
                wd_real = ((w_real * z1_real) - (w_imag * z1_imag))
                wd_imag = ((w_real * z1_imag) + (w_imag * z1_real))
                data[j] = (data[i] - wd_real)
                data[(j + 1)] = (data[(i + 1)] - wd_imag)
                data[i] = (data[i] + wd_real)
                data[(i + 1)] = (data[(i + 1)] + wd_imag)
        bit = (bit + 1)
        dual = (dual * 2)
        return None
FFT_transform_internal = FFT_transform_internal

def FFT_bitreverse(N, data):
    n = (N // 2)
    nm1 = retic_cast((n - 1), Int, Dyn, '\nfft.py:85:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    j = retic_cast(0, Int, Dyn, '\nfft.py:86:4: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
    for i in range(nm1):
        ii = (i << 1)
        jj = (j << 1)
        k = retic_cast((n >> 1), Int, Dyn, '\nfft.py:90:8: Right hand side of assignment was expected to be of type Dyn, but value %s provided instead. (code SINGLE_ASSIGN_ERROR)')
        if (i < j):
            tmp_real = data[ii]
            tmp_imag = data[(ii + 1)]
            data[ii] = data[jj]
            data[(ii + 1)] = data[(jj + 1)]
            data[jj] = tmp_real
            data[(jj + 1)] = tmp_imag
        while (k <= j):
            j = (j - k)
            k = (k >> 1)
        j = (j + k)
        return None
FFT_bitreverse = FFT_bitreverse

def FFT_transform(N, data):
    FFT_transform_internal(N, data, retic_cast((- 1), Int, Dyn, '\nfft.py:107:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    return None
FFT_transform = FFT_transform

def FFT_inverse(N, data):
    n = (N / 2)
    norm = 0.0
    FFT_transform_internal(N, data, retic_cast((+ 1), Int, Dyn, '\nfft.py:115:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    norm = (1 / float(n))
    for i in xrange(N):
        data[i] = (data[i] * norm)
    return None
FFT_inverse = FFT_inverse

def bench_FFT(loops, N, cycles):
    twoN = (2 * N)
    init_vec = retic_cast(array, Dyn, Function(AnonymousParameters([String, List(Dyn)]), Dyn), "\nfft.py:124:15: Expected function of type Function(['String', 'List(Dyn)'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d', [retic_cast(retic_cast(random, Dyn, Object('', {'random': Dyn, }), '\nfft.py:124:27: Accessing nonexistant object attribute random from value %s. (code WIDTH_DOWNCAST)').random, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:124:27: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')() for i in retic_cast(range(twoN), Iterable(Dyn), Dyn, '\nfft.py:124: Iteration target was expected to be of type Dyn, but value %s was provided instead. (code ITER_ERROR)')])
    range_it = xrange(loops)
    for _ in range_it:
        x = retic_cast(array, Dyn, Function(AnonymousParameters([String]), Dyn), "\nfft.py:129:12: Expected function of type Function(['String'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")('d')
        x[:] = init_vec[:]
        for i in xrange(cycles):
            FFT_transform(twoN, x)
            FFT_inverse(twoN, x)
    return None
bench_FFT = bench_FFT

def main():
    t0 = retic_cast(retic_cast(time, Dyn, Object('', {'time': Dyn, }), '\nfft.py:140:9: Accessing nonexistant object attribute time from value %s. (code WIDTH_DOWNCAST)').time, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:140:9: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
    bench_FFT(retic_cast(10, Int, Dyn, '\nfft.py:141:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(1024, Int, Dyn, '\nfft.py:141:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'), retic_cast(50, Int, Dyn, '\nfft.py:141:4: Expected argument of type Dyn but value %s was provided instead. (code ARG_ERROR)'))
    t1 = retic_cast(retic_cast(time, Dyn, Object('', {'time': Dyn, }), '\nfft.py:142:9: Accessing nonexistant object attribute time from value %s. (code WIDTH_DOWNCAST)').time, Dyn, Function(AnonymousParameters([]), Dyn), '\nfft.py:142:9: Expected function of type Function([], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)')()
    retic_cast(print, Dyn, Function(AnonymousParameters([Dyn]), Dyn), "\nfft.py:143:4: Expected function of type Function(['Dyn'], Dyn) at call site but but value %s was provided instead. (code FUNC_ERROR)")((t1 - t0))
    return None
main = main
main()
