import random
from functools import reduce
start = """from __future__ import division, print_function

import math
import random

import perf
import six
import time
from six.moves import xrange


DEFAULT_THICKNESS = 0.25
DEFAULT_WIDTH = 2#256
DEFAULT_HEIGHT = 2#256
DEFAULT_ITERATIONS = 1#5#5000
DEFAULT_RNG_SEED = 1234
"""

gvector = """class GVector(object):
"""
  
gvector_init_headers = ["def __init__(self, x=0, y=0, z=0):"]
gvector_init_body = """
        self.x = x
        self.y = y
        self.z = z
"""

gvector_mag_headers = ["def Mag(self):", "def Mag(self:GVector)->float:"]
gvector_mag_body = """
        return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)
"""

gvector_dist_headers = ["def dist(self, other):"
                        , "def dist(self, other:GVector)->float:"
                        , "def dist(self:GVector, other)->float:"
                        , "def dist(self:GVector, other:GVector)->float:"]
gvector_dist_body = """
        return math.sqrt((self.x - other.x) ** 2 +
                         (self.y - other.y) ** 2 +
                         (self.z - other.z) ** 2)
"""

gvector_add_headers =  ["def __add__(self, other):"
                           , "def __add__(self:GVector, other)->GVector:"
                           , "def __add__(self, other:GVector)->GVector:"
                           , "def __add__(self:GVector, other:GVector)->GVector:"]
gvector_add_body = """
        if not isinstance(other, GVector):
            raise ValueError("Can't add GVector to " + str(type(other)))
        v = GVector(self.x + other.x, self.y + other.y, self.z + other.z)
        return v
"""

gvector_sub_headers =   ["def __sub__(self, other):"
                           , "def __sub__(self:GVector, other)->GVector:"
                           , "def __sub__(self, other:GVector)->GVector:"
                           , "def __sub__(self:GVector, other:GVector)->GVector:"]
gvector_sub_body = """
        return self + other * -1
"""

gvector_mul_headers =  ["def __mul__(self, other):"
                          , "def __mul__(self:GVector, other)->GVector:"
                          , "def __mul__(self, other:float)->GVector:"
                          , "def __mul__(self:GVector, other:float)->GVector:"]
gvector_mul_body = """
        v = GVector(self.x * other, self.y * other, self.z * other)
        return v
  __rmul__ = __mul__
"""

gvector_linear_combination_headers = ["def linear_combination(self, other, l1, l2=None):"
                                        ,"def linear_combination(self, other:GVector, l1, l2=None)->GVector:"
                                        ,"def linear_combination(self:GVector, other, l1, l2=None)->GVector:"
                                        ,"def linear_combination(self:GVector, other:GVector, l1, l2=None)->GVector:"]
gvector_linear_combination_body = """
        if l2 is None:
            l2 = 1 - l1
        v = GVector(self.x * l1 + other.x * l2,
                    self.y * l1 + other.y * l2,
                    self.z * l1 + other.z * l2)
        return v
"""

gvector_str_headers =  ["def __str__(self):", "def __str__(self:GVector):"]
gvector_str_body = """
        return "<%f, %f, %f>" % (self.x, self.y, self.z)
"""

gvector_repr_headers = ["def __repr__(self):", "def __str__(self:GVector):"]
gvector_repr_body = """
        return "GVector(%f, %f, %f)" % (self.x, self.y, self.z)
"""


getknots_headers = ["def GetKnots(points, degree):"
                    , "def GetKnots(points:List(Dyn), degree)->List(int):"
                    , "def GetKnots(points, degree:int)->List(int):"
                    , "def GetKnots(points:List(Dyn), degree:int)->List(int):"]
getknots_body = """
    knots = [0] * degree + range(1, len(points) - degree)
    knots += [len(points) - degree] * degree
    return knots
"""


spline = """class Spline(object):
"""

spline_init_headers = ["def __init__(self, points, degree=3, knots=None):"]
spline_init_body = """
        if knots is None:
            self.knots = GetKnots(points, degree)
        else:
            if len(points) > len(knots) - degree + 1:
                raise ValueError("too many control points")
            elif len(points) < len(knots) - degree + 1:
                raise ValueError("not enough control points")
            last = knots[0]
            for cur in knots[1:]:
                if cur < last:
                    raise ValueError("knots not strictly increasing")
                last = cur
            self.knots = knots
        self.points = points
        self.degree = degree
"""


spline_get_domain_headers = ["def GetDomain(self):", "def GetDomain(self:Spline)->(int, int):"]
spline_get_domain_body = """
        return (self.knots[self.degree - 1],
                self.knots[len(self.knots) - self.degree])
"""

spline_call_headers = ["def __call__(self, u):"
                       , "def __call__(self:Spline, u):"
                       , "def __call__(self, u:float):"
                       , "def __call__(self:Spline, u:float):"]
spline_call_body = """
        dom = self.GetDomain()
        if u < dom[0] or u > dom[1]:
            raise ValueError("Function value not in domain")
        if u == dom[0]:
            return self.points[0]
        if u == dom[1]:
            return self.points[-1]
        I = self.GetIndex(u)
        d = [self.points[I - self.degree + 1 + ii]
             for ii in range(self.degree + 1)]
        U = self.knots
        for ik in range(1, self.degree + 1):
            for ii in range(I - self.degree + ik + 1, I + 2):
                ua = U[ii + self.degree - ik]
                ub = U[ii - 1]
                co1 = (ua - u) / (ua - ub)
                co2 = (u - ub) / (ua - ub)
                index = ii - I + self.degree - ik - 1
                d[index] = d[index].linear_combination(d[index + 1], co1, co2)
        return d[0]
"""

spline_get_index_headers = ["def GetIndex(self, u):"
                            , "def GetIndex(self:Spline, u)->int:"
                            , "def GetIndex(self, u:float)->int:"
                            , "def GetIndex(self:Spline, u:float)->int:"]
spline_get_index_body = """
        dom = self.GetDomain()
        for ii in range(self.degree - 1, len(self.knots) - self.degree):
            if u >= self.knots[ii] and u < self.knots[ii + 1]:
                I = ii
                break
        else:
            I = dom[1] - 1
        return I
"""

spline_len_headers = ["def __len__(self):"
                         , "def __len__(self:Spline)->int:"]
spline_len_body = """
        return len(self.points)
"""

spline_repr_headers = ["def __repr__(self):", "def __repr__(self:Spline):"]
spline_repr_body = """
        return "Spline(%r, %r, %r)" % (self.points, self.degree, self.knots)
"""


write_ppm_headers = ["def write_ppm(im, filename):"]
write_ppm_body = """
    magic = 'P6\\n'
    maxval = 255
    w = len(im)
    h = len(im[0])

    #if six.PY3:
        #fp = open(filename, "w", encoding="latin1", newline='')
    #else:
        #fp = open(filename, "wb")
    #with fp:
    #fp.write(magic)
    #fp.write('%i %i\\n%i\\n' % (w, h, maxval))
    for j in range(h):
        for i in range(w):
            val = im[i][j]
            c = val * 255
            #fp.write('%c%c%c' % (c, c, c))
"""


chaos_game = """class Chaosgame(object):
"""


chaos_init_headers = ["def __init__(self, splines, thickness=0.1):"]
chaos_init_body = """
        self.splines = splines
        self.thickness = thickness
        self.minx = min([p.x for spl in splines for p in spl.points])
        self.miny = min([p.y for spl in splines for p in spl.points])
        self.maxx = max([p.x for spl in splines for p in spl.points])
        self.maxy = max([p.y for spl in splines for p in spl.points])
        self.height = self.maxy - self.miny
        self.width = self.maxx - self.minx
        self.num_trafos = []
        maxlength = thickness * self.width / self.height
        for spls in splines:
            length = 0
            curr = spls(0)
            for i in range(1, 1000):
                last = curr
                t = 1 / 999 * i
                curr = spls(t)
                length += curr.dist(last)
            self.num_trafos.append(max(1, int(length / maxlength * 1.5)))
        self.num_total = sum(self.num_trafos)
"""

chaos_get_random_trafo_headers = ["def get_random_trafo(self):"
                                  , "def get_random_trafo(self:Chaosgame)->(int,int):"]
chaos_get_random_trafo_body = """
        r = random.randrange(int(self.num_total) + 1)
        l = 0
        for i in range(len(self.num_trafos)):
            if r >= l and r < l + self.num_trafos[i]:
                return i, random.randrange(self.num_trafos[i])
            l += self.num_trafos[i]
        return len(self.num_trafos) - 1, random.randrange(self.num_trafos[-1])
"""

chaos_transform_point_headers = ["def transform_point(self, point, trafo=None):"
                                 , "def transform_point(self:Chaosgame, point, trafo=None)->GVector:"
                                 , "def transform_point(self, point:GVector, trafo=None)->GVector:"
                                 , "def transform_point(self:Chaosgame, point:GVector, trafo=None)->GVector:"]
chaos_transform_point_body = """
        x = (point.x - self.minx) / self.width
        y = (point.y - self.miny) / self.height
        if trafo is None:
            trafo = self.get_random_trafo()
        start, end = self.splines[trafo[0]].GetDomain()
        length = end - start
        seg_length = length / self.num_trafos[trafo[0]]
        t = start + seg_length * trafo[1] + seg_length * x
        basepoint = self.splines[trafo[0]](t)
        if t + 1 / 50000 > end:
            neighbour = self.splines[trafo[0]](t - 1 / 50000)
            derivative = neighbour - basepoint
        else:
            neighbour = self.splines[trafo[0]](t + 1 / 50000)
            derivative = basepoint - neighbour
        if derivative.Mag() != 0:
            basepoint.x += derivative.y / derivative.Mag() * (y - 0.5) * \
                self.thickness
            basepoint.y += -derivative.x / derivative.Mag() * (y - 0.5) * \
                self.thickness
        else:
            print("r", end='')
        self.truncate(basepoint)
        return basepoint
"""

chaos_truncate_headers = ["def truncate(self, point):"
                          , "def truncate(self, point:GVector):"
                          , "def truncate(self:Chaosgame, point):"
                          , "def truncate(self:Chaosgame, point:GVector):"]
chaos_truncate_body = """
        if point.x >= self.maxx:
            point.x = self.maxx
        if point.y >= self.maxy:
            point.y = self.maxy
        if point.x < self.minx:
            point.x = self.minx
        if point.y < self.miny:
            point.y = self.miny
"""

chaos_create_image_chaos_headers = ["def create_image_chaos(self, w, h, iterations, filename, rng_seed):"
                                    , "def create_image_chaos(self:Chaosgame, w, h, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self,  w:int, h, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self,  w, h:int, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self,  w, h, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w:int, h, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w, h:int, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w, h, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self,  w:int, h:int, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self,  w:int, h, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self,  w, h:int, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w:int, h:int, iterations, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w:int, h, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w, h:int, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self,  w:int, h:int, iterations:int, filename, rng_seed):"
                            , "def create_image_chaos(self:Chaosgame,  w:int, h:int, iterations:int, filename, rng_seed):"]
chaos_create_image_chaos_body = """
        # Always use the same sequence of random numbers
        # to get reproductible benchmark
        random.seed(rng_seed)

        im = [[1] * h for i in range(w)]
        point = GVector((self.maxx + self.minx) / 2,
                        (self.maxy + self.miny) / 2, 0)
        for _ in xrange(iterations):
            point = self.transform_point(point)
            x = (point.x - self.minx) / self.width * w
            y = (point.y - self.miny) / self.height * h
            x = int(x)
            y = int(y)
            if x == w:
                x -= 1
            if y == h:
                y -= 1
            im[x][h - y - 1] = 0

        #if filename:
        #    write_ppm(im, filename)
"""


rest = """
def main(runner, args):
    splines = [
        Spline([
            GVector(2.001670, 4.011320, 0.000000),
            GVector(2.335040, 3.312830, 0.000000),
            GVector(2.366800, 3.233460, 0.000000),
            GVector(2.366800, 3.233460, 0.000000)],
            3, [0, 0, 0, 1, 1, 1])
    ]

    runner.metadata['chaos_thickness'] = args.thickness
    runner.metadata['chaos_width'] = args.width
    runner.metadata['chaos_height'] = args.height
    runner.metadata['chaos_iterations'] = args.iterations
    runner.metadata['chaos_rng_seed'] = args.rng_seed

    chaos_var = Chaosgame(splines, args.thickness)
    chaos_var.create_image_chaos(args.width, args.height, args.iterations, args.filename, args.rng_seed)
    #runner.bench_func('chaos', chaos.create_image_chaos,
    #                  args.width, args.height, args.iterations,
    #                  args.filename, args.rng_seed)


def add_cmdline_args(cmd, args):
    cmd.append("--width=%s" % args.width)
    cmd.append("--height=%s" % args.height)
    cmd.append("--thickness=%s" % args.thickness)
    cmd.append("--rng-seed=%s" % args.rng_seed)
    if args.filename:
        cmd.extend(("--filename", args.filename))


if __name__ == "__main__":
    runner = perf.Runner(add_cmdline_args=add_cmdline_args)
    runner.metadata['description'] = "Create chaosgame-like fractals"
    cmd = runner.argparser
    cmd.add_argument("--thickness",
                     type=float, default=DEFAULT_THICKNESS,
                     help="Thickness (default: %s)" % DEFAULT_THICKNESS)
    cmd.add_argument("--width",
                     type=int, default=DEFAULT_WIDTH,
                     help="Image width (default: %s)" % DEFAULT_WIDTH)
    cmd.add_argument("--height",
                     type=int, default=DEFAULT_HEIGHT,
                     help="Image height (default: %s)" % DEFAULT_HEIGHT)
    cmd.add_argument("--iterations",
                     type=int, default=DEFAULT_ITERATIONS,
                     help="Number of iterations (default: %s)"
                          % DEFAULT_ITERATIONS)
    cmd.add_argument("--filename", metavar="FILENAME.PPM",
                     help="Output filename of the PPM picture")
    cmd.add_argument("--rng-seed",
                     type=int, default=DEFAULT_RNG_SEED,
                     help="Random number generator seed (default: %s)"
                          % DEFAULT_RNG_SEED)

    args = runner.parse_args()
    t0 = time.time()
    main(runner, args)
    t1 = time.time()
    print(t1-t0)
"""

def random_element(collection):
    return collection[random.randrange(0, len(collection))]
i = 0
while i < 100:
    gvector_init = "  " + random_element(    gvector_init_headers) +     gvector_init_body
    gvector_mag = "  " + random_element(gvector_mag_headers) + gvector_mag_body
    gvector_dist = "  " + random_element(gvector_dist_headers) + gvector_dist_body
    gvector_add = "  " + random_element(gvector_add_headers) + gvector_add_body
    gvector_sub = "  " + random_element(gvector_sub_headers) + gvector_sub_body
    gvector_mul = "  " + random_element(gvector_mul_headers) + gvector_mul_body
    gvector_linear_combination = "  " + random_element(gvector_linear_combination_headers) + gvector_linear_combination_body
    gvector_str = "  " + random_element(gvector_str_headers) + gvector_str_body
    gvector_repr = "  " + random_element(gvector_repr_headers) + gvector_repr_body
    getknots = random_element(getknots_headers) + getknots_body
    spline_init = "  " + random_element(spline_init_headers) + spline_init_body
    spline_get_domain = "  " + random_element(spline_get_domain_headers) + spline_get_domain_body
    spline_call = "  " + random_element(spline_call_headers) + spline_call_body
    spline_get_index = "  " + random_element(spline_get_index_headers) + spline_get_index_body
    spline_len = "  " + random_element(spline_len_headers) + spline_len_body
    spline_repr = "  " + random_element(spline_repr_headers) + spline_repr_body
    write_ppm = random_element(write_ppm_headers) + write_ppm_body
    chaos_init = "  " + random_element(chaos_init_headers) + chaos_init_body
    chaos_get_random_trafo = "  " + random_element(chaos_get_random_trafo_headers) + chaos_get_random_trafo_body
    chaos_transform_point = "  " + random_element(chaos_transform_point_headers) + chaos_transform_point_body
    chaos_truncate = "  " + random_element(chaos_truncate_headers) + chaos_truncate_body
    chaos_create_image_chaos = "  " + random_element(chaos_create_image_chaos_headers) + chaos_create_image_chaos_body
    program = reduce(lambda x,y: x+y, [
        start,
        gvector,
        gvector_init,
        gvector_mag,
        gvector_dist,
        gvector_add,
        gvector_sub,
        gvector_mul,
        gvector_linear_combination,
        gvector_str,
        gvector_repr,
        getknots,
        spline,
        spline_init,
        spline_get_domain,
        spline_call,
        spline_get_index,
        spline_len,
        spline_repr,
        write_ppm,
        chaos_game,
        chaos_init,
        chaos_get_random_trafo,
        chaos_transform_point,
        chaos_truncate,
        chaos_create_image_chaos,
        rest
    ], "")
    programName = "chaos" + str(i) + ".py"
    with open(programName, 'w+') as f:
        f.write(program)
    i += 1
