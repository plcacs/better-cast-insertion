from .vis import Visitor
import ast
from . import flags
from .rtypes import Choice, selL, selR
from functools import reduce

class SelVisitor(Visitor):
    examine_functions = False
    #we assume a false value for selector makes a left selection, while true makes a right selection
    def reduce_expr(self, ns, functionEnv, selector, *args):
        return [self.dispatch(n, functionEnv, selector, *args) for n in ns]

    def reduce_stmt(self, ns, functionEnv, selector, *args):
        return [self.dispatch(n, functionEnv, selector,  *args) for n in ns]

    def default_expr(self, n, functionEnv, selector, *args):
        return n
    def default_stmt(self, n, functionEnv, selector, *args):
        return [n]

    def renameCode(self, n, functionEnv, selector, *args):
        ast = self.preorder(n, functionEnv, selector, *args)
        return ast
    def default(self, n, functionEnv, selector, *args):
        if isinstance(n, ast.expr):
            return self.default_expr(n, functionEnv, selector, *args)
        elif isinstance(n, ast.stmt):
            return self.default_stmt(n, functionEnv, selector, *args)        
        else: return n

    def lift(self, n, functionEnv, selector):
        return self.combine_stmt_expr(self.empty_stmt(), n)

    def dispatch_statements(self, ns, functionEnv, selector, *args):
        if not hasattr(self, 'visitor'): # preorder may not have been called
            self.visitor = self
        return self.reduce_stmt(ns, functionEnv, selector, *args)

    def visitModule(self, n, functionEnv, selector, *args):
        return self.dispatch_statements(n.body, functionEnv, selector, *args)

    def visitlist(self, n, functionEnv, selector, *args):
        return self.reduce_stmt(n, functionEnv, selector, *args)

## STATEMENTS ##
    # Function stuff
    def visitFunctionDef(self, n, functionEnv, selector, *args):
        largs = self.dispatch(n.args, functionEnv, selector *args)
        decorator = self.reduce_stmt(n.decorator_list, functionEnv, selector, *args)
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        nom = n.functionEnv
        return [ast_trans.FunctionDef(functionEnv=nom, args=args,
                                      body=body, decorator_list=decorator,
                                      returns=(n.returns if hasattr(n, 'returns') else None),
                                       lineno=n.lineno)]

    def visitarguments(self, n, functionEnv, selector, *args):
        if flags.PY_VERSION == 3:                                    
            checked_args = nparams.lenmatch(n.args)
            args = [self.dispatch(val, env, misc, costEnv) for val, (k, ty) in zip(n.defaults, checked_args)]
            nargs = dict(args=args, vararg=n.vararg,
                         kwonlyargs=n.kwonlyargs, kwarg=n.kwarg,
                         defaults=defaults, kw_defaults=kw_defaults)
            if flags.PY3_VERSION < 4:
                nargs['kwargannotation'] = None
                nargs['varargannotation'] = n.varargannotation
            return ast.arguments(**nargs)
        else: return self.lift(self.reduce_expr(n.defaults, functionEnv, selector, *args)) #not handling other python versions for now

    def visitReturn(self, n, functionEnv, selector, *args):        
        if n.value:
            value = self.dispatch(n.value, functionEnv, selector, *args)
            return [ast.Return(value=value, lineno=n.lineno)]
        else: return [ast.Return(value=None, lineno=n.lineno)]

    # Assignment stuff
    def visitAssign(self, n, functionEnv, selector, *args):
        val = self.dispatch(n.value, functionEnv, selector, *args)
        targets = self.reduce_expr(targets)
        #targets = self.reduce_expr(n.targets, functionEnv, selector, *args)
        return [ast.Assign(targets=targets, value=val, lineno=n.lineno)]

    def visitAugAssign(self, n, functionEnv, selector, *args):
        target = self.dispatch(n.target, functionEnv, selector, *args)
        value = self.dispatch(n.value, functionEnv, selector, *args)
        optarget = utils.copy_assignee(n.target, ast.Load())
        assignment = ast.Assign(targets=[n.target], 
                                value=ast.BinOp(left=optarget,
                                                op=n.op,
                                                right=n.value,
                                                lineno=n.lineno),
                                lineno=n.lineno)
        return self.dispatch(assignment, env, misc, costEnv)        

    def visitDelete(self, n, functionEnv, selector, *args):
        targets = self.dispatch_statements(n.targets, functionEnv, selector, *args)
        return ([ast.Expr(targ, lineno=n.lineno) for targ in targets] + \
                    [ast.Delete(targets=n.targets, lineno=n.lineno)])

    # Control flow stuff
    def visitIf(self, n, functionEnv, selector, *args):
        test = self.dispatch(n.test, functionEnv, selector, *args)
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        orelse = self.dispatch_statements(n.orelse, functionEnv, selector, *args) if n.orelse else []
        return [ast.If(test=test, body=body, orelse=orelse, lineno=n.lineno)]

    def visitFor(self, n, functionEnv, selector, *args):
        target = self.dispatch(n.target, functionEnv, selector, *args)
        iter = self.dispatch(n.iter, functionEnv, selector, *args)
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        orelse = self.dispatch_statements(n.orelse, functionEnv, selector, *args) if n.orelse else []
        return [ast.For(target=target, iter=res,
                         body=targcheck+body, orelse=orelse, lineno=n.lineno)]
        
    def visitWhile(self, n, functionEnv, selector, *args):
        test = self.dispatch(n.test, functionEnv, selector, *args)
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        orelse = self.dispatch_statements(n.orelse, functionEnv, selector, *args) if n.orelse else []
        return [ast.While(test=test, body=body, orelse=orelse, lineno=n.lineno)]

    def visitWith(self, n, functionEnv, selector, *args):
        body = self.dispatch_statements(n.body, *args)
        if flags.PY_VERSION == 3 and flags.PY3_VERSION >= 3:
            items = self.reduce_expr(n.items, *args)
            return [ast.With(items=items, body=body, lineno=n.lineno)]
        else:
            context = self.dispatch(n.context_expr, *args)
            optional_vars = self.dispatch(n.optional_vars, functionEnv, selector, *args) if n.optional_vars else None
            items = self.combine_expr(context, optional_vars)
        return [ast.With(context_expr=context, optional_vars=optional_vars, 
                              body=body, lineno=n.lineno)]
    
    def visitwithitem(self, n, functionEnv, selector, *args):
        context = self.dispatch(n.context_expr, functionEnv, selector, *args),
        optional_vars = self.dispatch(n.optional_vars, functionEnv, selector, *args)
        return ast.withitem(context_expr=context_expr, optional_vars=optional_vars)

    # Class stuff
    def visitClassDef(self, n, functionEnv, selector, *args):
        bases = self.reduce_expr(n.bases, *args)
        if flags.PY_VERSION == 3:
            keywords = [self.dispatch(kwd.value, functionEnv, selector, *args) for kwd in n.keywords]
        else: keywords = []
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        return [ast_trans.ClassDef(functionEnv=functionEnv, bases=bases, keywords=keywords,
                                   starargs=(n.starargs if hasattr(n, 'starargs') else None),
                                   kwargs=(n.kwargs if hasattr(n, 'kwargs') else None), body=body,
                                    decorator_list=n.decorator_list, lineno=n.lineno)]

    # Exception stuff
    # Python 2.7, 3.2
    def visitTryExcept(self, n, functionEnv, selector, *args):
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        handlers = self.reduce_stmt(n.handlers, functionEnv, selector, *args)
        orelse = self.dispatch_statements(n.orelse, functionEnv, selector, *args) if n.orelse else []
        return [ast.TryExcept(body=body, handlers=handlers, orelse=orelse, lineno=n.lineno)]

    # Python 2.7, 3.2
    def visitTryFinally(self, n, functionEnv, selector, *args):
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        finalbody = self.dispatch_statements(n.finalbody, functionEnv, selector, *args)
        return [ast.TryFinally(body=body, finalbody=finalbody, lineno=n.lineno)]
    
    # Python 3.3
    def visitTry(self, n, functionEnv, selector, *args):
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        handlers = self.reduce_stmt(n.handlers, functionEnv, selector, *args)
        orelse = self.dispatch_statements(n.orelse, functionEnv, selector, *args) if n.orelse else []
        finalbody = self.dispatch_statements(n.finalbody, functionEnv, selector, *args)
        return [ast.Try(body=body, handlers=handlers, orelse=orelse, finalbody=finalbody, lineno=n.lineno)]

    def visitExceptHandler(self, n, functionEnv, selector, *args):
        ty = self.dispatch(n.type, functionEnv, selector, *args) if n.type else None
        body = self.dispatch_statements(n.body, functionEnv, selector, *args)
        return ast.ExceptHandler(type=ty, functionEnv=n.functionEnv, body=body, lineno=n.lineno)

    def visitRaise(self, n, functionEnv, selector, *args):
        if flags.PY_VERSION == 3:
            exc = self.dispatch(n.exc, functionEnv, selector, *args) if n.exc else None
            cause = self.dispatch(n.cause, functionEnv, selector, *args) if n.cause else None
            return [ast.Raise(exc=exc, cause=cause, lineno=n.lineno)]
        elif flags.PY_VERSION == 2:
            type = self.dispatch(n.type, functionEnv, selector, *args) if n.type else None
            inst = self.dispatch(n.inst, functionEnv, selector, *args) if n.inst else None
            tback = self.dispatch(n.tback, functionEnv, selector, *args) if n.tback else None
            return [ast.Raise(type=type, inst=inst, tback=tback, lineno=n.lineno)]

    def visitAssert(self, n, functionEnv, selector, *args):
        test = self.dispatch(n.test, functionEnv, selector, *args)
        msg = self.dispatch(n.msg, functionEnv, selector, *args) if n.msg else None
        return [ast.Assert(test=test, msg=msg, lineno=n.lineno)]

    # Miscellaneous
    def visitExpr(self, n, functionEnv, selector, *args):
        return [n]

    def visitPass(self, n, functionEnv, selector, *args):
        return [n]

    def visitBreak(self, n, functionEnv, selector, *args):
        return [n]

    def visitContinue(self, n, functionEnv, selector, *args):
        return [n]

    def visitPrint(self, n, functionEnv, selector, *args):
        dest = self.dispatch(n.dest, functionEnv, selector, *args) if n.dest else self.empty_expr()
        values = self.reduce_expr(n.values, functionEnv, selector, *args)
        return [ast.Print(dest=dest, values=values, nl=n.nl, lineno=n.lineno)]

    def visitExec(self, n, functionEnv, selector, *args):
        body = self.dispatch(n.body, functionEnv, selector, *args)
        globals = self.dispatch(n.globals, functionEnv, selector, *args) if n.globals else None
        locals = self.dispatch(n.locals, functionEnv, selector, *args) if n.locals else None
        return [ast.Exec(body=body, globals=globals, locals=locals, lineno=n.lineno)]

### EXPRESSIONS ###
    # Op stuff
    def visitBoolOp(self, n, functionEnv, selector, *args):
        values = self.reduce_expr(values, functionEnv, selector, *args)
        return ast.BoolOp(op=n.op, values=values, lineno=n.lineno)

    def visitBinOp(self, n, functionEnv, selector, *args):
        left = self.dispatch(n.left, functionEnv, selector, *args)
        right = self.dispatch(n.right, functionEnv, selector, *args)
        return ast.BinOp(left=left, op=n.op, right=right, lineno=n.lineno)

    def visitUnaryOp(self, n, functionEnv, selector, *args):
        operand = self.dispatch(n.operand, functionEnv, selector, *args)
        return ast.UnaryOp(op=n.op, operand=operand, lineno=n.lineno)

    def visitCompare(self, n, functionEnv, selector, *args):
        left = self.dispatch(n.left, functionEnv, selector, *args)
        comparators = self.reduce_expr(n.comparators, functionEnv, selector, *args)
        return ast.Compare(left=left, ops=n.ops, comparators=comparators, lineno=n.lineno)

    # Collections stuff    
    def visitList(self, n, functionEnv, selector, *args):
        elts = self.reduce_expr(n.elts, functionEnv, selector, *args)
        return ast.List(elts=elts, ctx=n.ctx, lineno=n.lineno)

    def visitTuple(self, n, functionEnv, selector, *args):
        elts = self.reduce_expr(n.elts, functionEnv, selector, *args)
        return ast.Tuple(elts=elts, ctx=n.ctx, lineno=n.lineno)

    def visitDict(self, n, functionEnv, selector, *args):
        keys = self.reduce_expr(n.keys, functionEnv, selector, *args),
        values = self.reduce_expr(n.values, functionEnv, selector, *args)
        return ast.Dict(keys=list(keys), values=list(values), lineno=n.lineno)

    def visitSet(self, n, functionEnv, selector, *args):
        elts = self.reduce_expr(n.elts, functionEnv, selector, *args)
        return ast.Set(elts=elts, lineno=n.lineno)

    def visitListComp(self, n, functionEnv, selector, *args):
        generators = self.reduce_expr(n.generators, functionEnv, selector, *args)
        elt = self.dispatch(n.elt, functionEnv, selector, *args)
        return ast.ListComp(elt=elt, generators=list(generators), lineno=n.lineno)

    def visitSetComp(self, n, functionEnv, selector, *args):
        generators = self.reduce_expr(n.generators,*args)
        elt = self.dispatch(n.elt, *args)
        return ast.SetComp(elt=elt, generators=list(generators), lineno=n.lineno)

    def visitDictComp(self, n, functionEnv, selector, *args):
        generators = self.reduce_expr(n.generators, functionEnv, selector, *args)
        key = self.dispatch(n.key, functionEnv, selector, *args)
        value = self.dispatch(n.value, functionEnv, selector, *args)
        return ast.DictComp(key=key, value=value, generators=list(generators), lineno=n.lineno)

    def visitGeneratorExp(self, n, functionEnv, selector, *args):
        generators = self.reduce_expr(n.generators,*args)
        elt = self.dispatch(n.elt, functionEnv, selector, *args)
        return ast.GeneratorExp(elt=elt, generators=list(generators), lineno=n.lineno)

    def visitcomprehension(self, n, functionEnv, selector, *args):
        iter = self.dispatch(n.iter, functionEnv, selector, *args)
        ifs = self.reduce_expr(n.ifs, functionEnv, selector, *args)
        target = self.dispatch(n.target, functionEnv, selector, *args)
        return ast.comprehension(target=target, iter=res, 
                                   ifs=ifs)

    # Control flow stuff
    def visitYield(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args) if n.value else None 
        return ast.Yield(value=value, lineno=n.lineno)

    def visitYieldFrom(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args)
        return ast.YieldFrom(value=value, lineno=n.lineno)

    def visitIfExp(self, n, functionEnv, selector, *args):
        test = self.dispatch(n.test, functionEnv, selector, *args)
        body = self.dispatch(n.body, functionEnv, selector, *args)
        orelse = self.dispatch(n.orelse, functionEnv, selector, *args)
        return ast.IfExp(test=test, body=body, orelse=orelse, lineno=n.lineno)

    # Function stuff
    def visitCall(self, n, functionEnv, selector, *args):
        func = self.dispatch(n.func, functionEnv, selector, *args)
        argdata = self.reduce_expr(n.args, functionEnv, selector, *args)
        return ast_trans.Call(func=func, args=argdata, keywords=n.keywords,
                              starargs=getattr(n, 'starargs', None),
                              kwargs=getattr(n, 'kwargs', None), lineno=n.lineno)

    def visitLambda(self, n, functionEnv, selector, *args):
        largs = self.dispatch(n.args, functionEnv, selector, *args)
        body = self.dispatch(n.body, functionEnv, selector, *args)
        return ast.Lambda(args=largs, body=body, lineno=n.lineno)

    # Variable stuff
    def visitAttribute(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args)
        return ast.Attribute(value=value, attr=n.attr, ctx=n.ctx, lineno=n.lineno)

    def visitSubscript(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args)
        slice = self.dispatch(n.slice, functionEnv, selector, *args)
        return ast.Subscript(value=value, slice=slice, ctx=n.ctx, lineno=n.lineno)

    def visitIndex(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args)
        return ast.Index(value=value)

    def visitSlice(self, n, functionEnv, selector, *args):
        lower = self.dispatch(n.lower, functionEnv, selector, *args) if n.lower else None
        upper = self.dispatch(n.upper, functionEnv, selector, *args) if n.upper else None
        step = self.dispatch(n.step, functionEnv, selector, *args) if n.step else None
        return ast.Slice(lower=lower, upper=upper, step=step)

    def visitExtSlice(self, n, functionEnv, selector, *args):
        dims = self.reduce_expr(n.dims, functionEnv, selector, *args)
        return ast.ExtSlice(dims=dims)

    def visitStarred(self, n, functionEnv, selector, *args):
        value = self.dispatch(n.value, functionEnv, selector, *args)
        return ast.Starred(value=value, ctx=n.ctx, lineno=n.lineno)

    def visitEllipsis(self, n, functionEnv, selector, *args):
        return n

    #might need to add code for constants
    def visitNum(self, n, functionEnv, selector, *args):
        return n

    def visitStr(self, n, functionEnv, selector, *args):
        return n

    def visitBytes(self, n, functionEnv, selector, *args):
        return n
        
    def visitFunctionEnv(self, n, functionEnv, selector, *args):
        if (n.id+"_vari" in functionEnv):
            cty = functionEnv[n.id+"_vari"]
            choice_name = cty.name
            possible_name = makeChoice(newName, n.id, n.id+"_fast")
            new_name = apply_decision(cName, selector)
            return ast.Name(id=new_name, ctx = n.ctx, lineno=n.lineno)
        else:
            return ast.Name(id=n.id, ctx = n.ctx, lineno=n.lineno)
