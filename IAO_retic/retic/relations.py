from .rtypes import *
from . import flags, logging
from .exc import UnknownTypeError, UnexpectedTypeError
import pdb
import math
import itertools
import copy
from functools import reduce

firstLetter = (ord('A') - 1)
tvar = 0
cvar = 0#(ord('a') - 1)
def fresh_choice_name():
    global cvar
    cvar += 1
    #cname = chr(cvar)
    return str(cvar)

def fresh_tvar():
    global tvar
    tvar += 1    
    return TypeVariable(tvar)

def fresh_opaque_var():
    global tvar
    tvar += 1
    return TypeVariable(tvar, True)

def searchForTop(pattern, path):
    if (pattern == 2):
            return True, path
    elif tyinstance(pattern, Choice):
        left, newPath = searchForTop(selL(pattern.name, pattern), path + [lambda x: selL(pattern.name, x)])
        if (left):
            return left, newPath
        else:
            return searchForTop(selR(pattern.name,pattern), path + [lambda x: selR(pattern.name, x)])
    else:
        return False, path

def hideDyns(pi,branch): #left branch = True, right branch = False
    if pi in [0,2]:
        return (pi,set(),set())
    if tyinstance(pi, Choice) and pi.kind == VariType.DYNAMIC:
        pi1 = selL(pi.name, pi)
        pi2 = selR(pi.name, pi)
        npl,npr = pi1,pi2

        actDynsl, maybeDynsl, actDynsr, maybeDynsr =  [set() for _ in  range(4)]

        if tyinstance(pi1, Choice) and pi1.kind != VariType.FLOW:
            npl,actDynsl,maybeDynsl = hideDyns(pi1, True)       
        
        if tyinstance(pi2, Choice) and pi2.kind != VariType.FLOW:
            npr,actDynsr,maybeDynsr = hideDyns(pi2, False)           

        npi = npl if branch else npr

        if ((npl == 2 and npr == 0) or (tyinstance(npl, Choice) and npl.kind == VariType.FLOW and npr == 0)):
            return (npi,set([(pi.kind, pi.name)]).union(actDynsl).union(actDynsr), maybeDynsl.union(maybeDynsr) )
        if (npl == 2 and tyinstance(npr, Choice) and npr.kind == VariType.FLOW):
            return (npi,actDynsl.union(actDynsr), set([(pi.kind, pi.name)]).union(maybeDynsl).union(maybeDynsr) )

        # D can be simplified to ok or err
        return (npi,actDynsl.union(actDynsr), maybeDynsl.union(maybeDynsr))
    
    if tyinstance(pi, Choice) and pi.kind == VariType.STATIC:
        pi1 = selL(pi.name, pi)
        pi2 = selR(pi.name, pi)
        npl,npr = pi1,pi2

        actDynsl, maybeDynsl, actDynsr, maybeDynsr =  [set() for _ in  range(4)]

        if pi1 not in [0,2] and tyinstance(pi1, Choice) and not pi1.kind == VariType.FLOW:
            npl,actDynsl,maybeDynsl = hideDyns(pi1, True)       
        
        if pi2 not in [0,2] and  tyinstance(pi2, Choice) and not pi2.kind == VariType.FLOW:
            npr,actDynsr,maybeDynsr = hideDyns(pi2, False)           

        npi = npl

        return (npi,actDynsl.union(actDynsr), maybeDynsl.union(maybeDynsr))

    if tyinstance(pi, Choice) and pi.kind == VariType.FLOW:
        pi1 = selL(pi.name, pi)
        pi2 = selR(pi.name, pi)
        npl,npr = pi1,pi2

        actDynsl, maybeDynsl, actDynsr, maybeDynsr =  [set() for _ in  range(4)]

        if pi1 not in [0, 2] and  tyinstance(pi1, Choice) and pi1.kind != VariType.FLOW:
            npl,actDynsl,maybeDynsl = hideDyns(pi1, True)       
        
        if pi2 not in [0, 2] and  tyinstance(pi2, Choice) and pi2.kind != VariType.FLOW:
            npr,actDynsr,maybeDynsr = hideDyns(pi2, False)           

        npi = makeChoice(pi.name, npl,npr, pi.kind)

        return (npi,actDynsl.union(actDynsr), maybeDynsl.union(maybeDynsr))


def ftv(ty):
    if (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            return ftv(ty.to)
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys += ftv(t)

            newTys = ftv(ty.to)
            return newTys
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys += ftv(t)
            
            newTys += ftv(ty.to)
            return newTys
        else:
            raise UnknownTypeError()    
    if (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls += (ftv(e))
        return newEls            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = ftv(ty.type)
        return (underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        return ftv(ty.keys) + ftv(ty.values)
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = list(itertools.chain.from_iterable([ftv(ty.members[n]) for n in ty.members]))
        #ty.members = mems #mutate ty
        return mems        
    elif (tyinstance(ty, Class)):
        #check methods and attributes        
        mems = list(itertools.chain.from_iterable([ftv(ty.members[n]) for n in ty.members]))
        #ty.members = mems #mutate ty
        instance_mems = list(itertools.chain.from_iterable([ftv(ty.instance_members[n]) for n in ty.instance_members]))
        return mems + instance_mems
    elif (tyinstance(ty, Choice)):
        lty = ftv(selL(ty.name, ty))
        rty = ftv(selR(ty.name, ty))
        return lty + rty
    elif (tyinstance(ty, TypeVariable)):
        return [ty]
    else:
        return []

def fc(ty):
    if (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            return fc(ty.to)
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys += fc(t)

            newTys = fc(ty.to)
            return newTys
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys += fc(t)
            
            newTys += fc(ty.to)
            return newTys
        else:
            raise UnknownTypeError()    
    if (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls += (fc(e))
        return newEls            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = fc(ty.type)
        return (underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        return fc(ty.keys) + fc(ty.values)
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = list(itertools.chain.from_iterable([fc(ty.members[n]) for n in ty.members]))
        #ty.members = mems #mutate ty
        return mems
    elif (tyinstance(ty, Class)):
        #check methods and attributes
        #if isinstance(ty.members, set):
            #pdb.set_trace()
        mems = list(itertools.chain.from_iterable([fc(ty.members[n]) for n in ty.members]))
        #ty.members = mems #mutate ty
        instance_mems = list(itertools.chain.from_iterable([fc(ty.instance_members[n]) for n in ty.instance_members]))
        return mems + instance_mems
    elif (tyinstance(ty, Choice)):
        lty = fc(selL(ty.name, ty))
        rty = fc(selR(ty.name, ty))
        if (ty.kind == VariType.FLOW):
            return [ty.name] + lty + rty
        return lty + rty
    elif (tyinstance(ty, TypeVariable)):
        return []
    else:
        return []

def replace_choice_name(ty, newName):
    if (tyinstance(ty, TypeScheme)):
        return replace_choice_name(ty.ty, newName)
    if (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            return Function(ty.froms, replace_choice_name(ty.to, newName))
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys.append(replace_choice_name(t, newName))
            froms = AnonymousParameters(newTys)
            to = replace_choice_name(ty.to, newName)
            return Function(froms, to)
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys.append((n,replace_choice_name(t, newName)))
            froms = NamedParameters(newTys)
            to = replace_choice_name(ty.to, newName)
            return Function(froms, to)            
        else:
            raise UnknownTypeError()    
    if (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls.append(replace_choice_name(e, newName))
        return Tuple(*newEls)            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = replace_choice_name(ty.type, newName)
        return List(underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        return Dict(replace_choice_name(ty.keys, newName), replace_choice_name(ty.values, newName))
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = {n: replace_choice_name(ty.members[n], newName) for n in ty.members}
        ty.members = mems #mutate ty        
        return ty
    elif (tyinstance(ty, Class)):
        #check methods and attributes
        mems = {n: replace_choice_name(ty.members[n], newName) for n in ty.members}
        instance_mems = {n: replace_choice_name(ty.instance_members[n], newName) for n in ty.instance_members}
        ty.members = mems #mutate ty
        ty.instance_members = instance_mems
        return ty
    elif (tyinstance(ty, Choice)):
        lty = replace_choice_name(selL(ty.name, ty), newName)
        rty = replace_choice_name(selR(ty.name, ty), newName)
        if (ty.kind == VariType.FLOW and ty.name in newName):
            return makeChoice(newName[ty.name], lty, rty, ty.kind)
        return makeChoice(ty.name, lty, rty, ty.kind)
    elif (tyinstance(ty, TypeVariable)):
        return ty
    else:
        return ty

def permute_choices(n):
    global cvar
    selectors = [str(i) for i in range(1,cvar+1)]
    return itertools.combinations(selectors, n)

def apply_decision(decision, pat):
    newPat = pat
    for s in decision:
        newPat = s(newPat)
    return newPat

def apply_decisions_cost(decision, cost, pat):
    c = cost
    p = pat
    for sel in decision:
        c = sel(c)
        p = sel(p)
    if (not p):
        # if the selection on the pattern returns False, return infinity for the cost
        return float("inf")
    while (tyinstance(c, Choice)):
        #otherwise get rid of all of the remaining choices by setting them as if the parameter was Dyn
        c = selL(c.name, c)
        
    return c

def find_lowest_cost_perms(cost, pat):
    #collect the n choose 2 decisions adding 2 new type annotations
    #global cvar
    min_cost = float("inf")
    min_cost_val = float("inf")
    min_desc = (None, None)
    min_d_func = (None, None)
    relevantDecisions = collect_choices(cost)
    decisions3 = itertools.combinations(relevantDecisions, 3)
    decisions2 = itertools.combinations(relevantDecisions, 2)
    decisions1 = map(lambda x: [x], relevantDecisions)
    #decisions3 = permute_choices(3)
    #decisions2 = permute_choices(2)
    #decisions1 = [[str(i)] for i in range(1,cvar+1)]
    decisions, dec = itertools.tee(itertools.chain(decisions3, decisions2, decisions1), 2)
    for d in decisions:
        dFunc = map(lambda y: (lambda x: selR(y, x)), d)
        curr_cost = apply_decisions(dFunc, cost, pat)
        c = curr_cost
        if (not isinstance(c, float)):
            for n in c.free_symbols:
                c = c.subs(n, 10000)        
        if (c <= min_cost_val):
            min_cost = curr_cost
            min_cost_val = c
            min_desc = d
            min_d_func = dFunc
    dwco = []
    for s in dec:
        dFunc = map(lambda y: (lambda x: selR(y, x)), s)
        curr_cost = apply_decisions(dFunc, cost, pat)
        c = curr_cost
        if (not isinstance(c, float)):
            for n in c.free_symbols:
                c = c.subs(n, 10000)
        if (s != min_desc):
            if ((c / min_cost_val) < 1.5):                
                dwco.append(s)
    return (min_cost, min_desc, min_d_func, dwco)

def collect_choices(cost):
    names = set()
    def collect_choices_help(c):
        if (tyinstance(c, Choice)):
            names.add(c.name)
            collect_choices_help(selL(c.name, c))
            collect_choices_help(selR(c.name, c))

    collect_choices_help(cost)
    return names

def ftvEnv(env):
    tys = []
    for t in env.values():
        ts = ftv(t)
        tys += ts
    return tys

def fcEnv(env):
    tys = []
    for t in env.values():
        ts = fc(t)
        tys += ts
    return tys

def generalize(ty, env):
    ct = set(fc(ty))
    ce = set(fcEnv(env))
    ft = set(ftv(ty))
    fe = set(ftvEnv(env))
    fvs = list(ft - fe)
    fcs = list(ct -ce)
    return TypeScheme(fcs, fvs, ty)

def instantiate(tys):
    choiceSub= {}
    for cName in tys.cvars:
        choiceSub[cName] = fresh_choice_name()
    newTy = replace_choice_name(tys.ty, choiceSub)
    newSub = {}
    for var in tys.tvars:
        var_fun = fresh_opaque_var if (var.opaque) else fresh_tvar
        newSub[var] = var_fun()
    return applySub(newTy, newSub)

def tvars_to_dyn(ty):
    new_type = Dyn
    if (tyinstance(ty, TypeScheme)):
        new_type = tvars_to_dyn(ty.ty)
    elif (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            new_type = Function(ty.froms, tvars_to_dyn(ty.to))
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys.append(tvars_to_dyn(t))
            froms = AnonymousParameters(newTys)
            to = tvars_to_dyn(ty.to)
            new_type = Function(froms, to)
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys.append((n,tvars_to_dyn(t)))
            froms = NamedParameters(newTys)
            to = tvars_to_dyn(ty.to)
            new_type = Function(froms, to)            
        else:
            raise UnknownTypeError()    
    elif (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls.append(tvars_to_dyn(e))
        new_type = Tuple(*newEls)            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = tvars_to_dyn(ty.type)
        new_type = List(underlying)
    elif (tyinstance(ty, Iterable)):
        #check underlying type
        underlying = tvars_to_dyn(ty.type)
        new_type = Iterable(underlying)
    elif (tyinstance(ty, Set)):
        #check underlying type
        underlying = tvars_to_dyn(ty.type)
        new_type = Set(underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        new_type = Dict(tvars_to_dyn(ty.keys), tvars_to_dyn(ty.values))
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = {n: tvars_to_dyn(ty.members[n]) for n in ty.members}
        ty.members = mems #mutate ty
        new_type = ty
    elif (tyinstance(ty, Class)):
        #check methods and attributes
        mems = {n: tvars_to_dyn(ty.members[n]) for n in ty.members}
        ty.members = mems #mutate ty
        ty.instance_members = {n: tvars_to_dyn(ty.instance_members[n]) for n in ty.instance_members}
        new_type = ty
    elif (tyinstance(ty, Choice)):
        if (ty.kind == VariType.FLOW):
            return Dyn
        lty = tvars_to_dyn(selL(ty.name, ty))
        rty = tvars_to_dyn(selR(ty.name, ty))
        new_type = makeChoice(ty.name, lty, rty, ty.kind)
    elif (tyinstance(ty, TypeVariable)):
        new_type = Dyn
    elif pinstance(ty, DynParameters):
        return ty #NOTE: DynParameters are not inferred
    elif pinstance(ty, NamedParameters):
        newTys =  []
        for n,t in ty.parameters:
            newTys.append((n,tvars_to_dyn(t)))
        froms = NamedParameters(newTys)
        return froms
    elif pinstance(ty, AnonymousParameters):
        newTys = []
        for t in ty.parameters:
            newTys.append(tvars_to_dyn(t))
        froms = AnonymousParameters(newTys)
        return froms
    elif ty in [Dyn, Void, String, Bytes]:
        new_type = ty
    elif prim(ty):
        new_type = ty
    else:
        #pdb.set_trace()
        new_type = ty
        return
    new_type.inferred = ty.inferred
    return new_type

def info_join(ty1, ty2):
    def memjoin(m1, m2):
        mems = {}
        for k in m1:
            if k in m2:
                mems[k] = ijoin(m1[k], m2[k])
            else: mems[k] = m1[k]
        for k in m2:
            if k not in m1:
                mems[k] = m2[k]
        return mems

    def ijoin(ty1, ty2):
        assert isinstance(ty1, PyType)
        assert isinstance(ty2, PyType)
        if not ty1.top_free() and ty2.top_free():
            return InfoTop
        elif tyinstance(ty1, Dyn):
            return ty2
        elif tyinstance(ty2, Dyn):
            return ty1
        elif ty1 == ty2:
            return ty1
        elif tyinstance(ty1, Function) and tyinstance(ty2, Function):
            return Function(info_paramjoin(ty1.froms, ty2.froms), ijoin(ty1.to, ty2.to))
        elif tyinstance(ty1, Object) and tyinstance(ty2, Object):
            name = ty1.name if ty1.name == ty2.name else ''
            ty1 = ty1.substitute(ty1.name, TypeVariable(name), False)
            ty2 = ty2.substitute(ty2.name, TypeVariable(name), False)
            mems = memjoin(ty1.members, ty2.members)  
            return Object(name, mems)
        elif tyinstance(ty1, Class) and tyinstance(ty2, Class):
            name = ty1.name if ty1.name == ty2.name else ''
            ty1 = ty1.substitute(ty1.name, TypeVariable(name), False)
            ty2 = ty2.substitute(ty2.name, TypeVariable(name), False)
            mems = memjoin(ty1.members, ty2.members)
            inst = memjoin(ty1.instance_members, ty2.instance_members)
            return Class(name, mems, inst)
        elif tyinstance(ty1, List) and tyinstance(ty2, List):
            return List(ijoin(ty1.type, ty2.type))
        elif tyinstance(ty1, Set) and tyinstance(ty2, Set):
            return Set(ijoin(ty1.type, ty2.type))
        elif tyinstance(ty1, Dict) and tyinstance(ty2, Dict):
            return Dict(ijoin(ty1.keys, ty2.keys), ijoin(ty1.values, ty2.values))
        elif tyinstance(ty1, Tuple) and tyinstance(ty2, Tuple):
            if len(ty1.elements) == len(ty2.elements):
                return Tuple(*[ijoin(e1, e2) for (e1, e2) in zip(ty1.elements, ty2.elements)])
            else: return InfoTop
        elif tyinstance(ty1, Structural) and tyinstance(ty2, Structural) and \
             (tyinstance(ty1, Object) or tyinstance(ty2, Object)):
            #pdb.set_trace()
            if tyinstance(ty1, Class):
                return ijoin(Object('', ty1.members), ty2)
            elif tyinstance(ty2, Class):
                return ijoin(ty1, Object('', ty2.members))
            return InfoTop #ijoin(ty1.structure(), ty2.structure())
        else: return InfoTop
    join = ijoin(ty1, ty2)
    #pdb.set_trace()
    if join.top_free():
        return join
    else: return InfoTop

def n_info_join(*types):
    if type(types[0]) == list and len(types) == 1:
        types = types[0]
    if len(types) == 0:
        return Dyn
    join = types[0]
    for ty in types[1:]:
        join = info_join(join, ty)
        if not join.top_free() or not ty.top_free():
            return InfoTop
    return join

def info_paramjoin(p1, p2):
    if pinstance(p1, DynParameters):
        return p2
    elif pinstance(p2, DynParameters):
        return p1
    elif pinstance(p1, NamedParameters):
        if len(p1.parameters) != len(p2.parameters):
            return InfoTop
        elif pinstance(p2, NamedParameters):
            if all(k1 == k2 for (k1, _), (k2, _) in zip(p1.parameters, p2.parameters)):
                return NamedParameters([(k1, info_join(t1, t2)) for (k1, t1), (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
            else: return InfoTop
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, (_, t2) in\
                                            zip(p2.parameters, p1.parameters)])
        else: raise UnknownTypeError()
    elif pinstance(p1, AnonymousParameters):
        if len(p1.parameters) != len(p2.parameters):
            return InfoTop
        elif pinstance(p2, NamedParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, t2 in\
                                            zip(p1.parameters, p2.parameters)])
        else: raise UnknownTypeError()
    else: raise UnknownTypeError()


def params_have_types(params):
    return pinstance(params, NamedParameters) or pinstance(params, AnonymousParameters)

def extract_param_types(params):
    if pinstance(params, NamedParameters):
        return [p[1] for p in params.parameters]
    elif pinstance(params, AnonymousParameters):
        return params.parameters
    elif pinstance(params, DynParameters):
        return params
    else:
        raise UnknownTypeError()

def and_fun(x, y): return x and y

def less_precise(t1, t2):
    if (t1 == Dyn):
        return True
    elif (t1 == t2):
        return True
    elif ((not tyinstance(t1, Choice)) and t2 == Dyn):
        return False    
    elif (tyinstance(t1, Choice)):
        return makeChoice(t1.name, less_precise(selL(t1.name, t1), t2), less_precise(selR(t1.name, t1), t2), t1.kind)
    elif (tyinstance(t2, Choice)):
        return makeChoice(t2.name, less_precise(t1, selL(t2.name, t2)), less_precise(t1, selR(t2.name, t2)), t2.kind)
    elif tyinstance(t1, Function) and tyinstance(t2, Function):
        if pinstance(t1.froms, DynParameters):
            return less_precise(t1.to, t2.to)
        elif pinstance(t2.froms, DynParameters):
            return False
        elif params_have_types(t1.froms) and params_have_types(t2.froms):
            # probably need to make the code below contravariant in the domain precision
            return reduce(patternMeet, [less_precise(*tys) for tys in zip(extract_param_types(t1.froms), extract_param_types(t2.froms))], True) and less_precise(t1.to, t2.to)
        else:
            return False
    elif tyinstance(t1, Tuple) and tyinstance(t2, Tuple):
        return reduce(and_fun, [less_precise(*tys) for tys in zip(t1.elements, t2.elements)], True)
    elif tyinstance(t1, Dict) and tyinstance(t2, Dict):
        return less_precise(t1.keys, t2.keys) and less_precise(t1.values, t2.values)
    elif tyinstance(t1, List) and tyinstance(t2, List):
        return less_precise(t1.type, t2.type)
    elif tyinstance(t1, Set) and tyinstance(t2, Set):
        return less_precise(t1.type, t2,type)
    elif tyinstance(t1, Iterable) and tyinstance(t2, Iterable):
        return less_precise(t1.type, t2.type)
    elif tyinstance(t1, Class) and tyinstance(t2, Class):
        return reduce(and_fun, [less_precise(*tys) for tys in zip(t1.members, t2.members)], True) and  reduce(and_fun, [less_precise(*tys) for tys in zip(t1.instance_members, t2.instance_members)], True) 
    elif tyinstance(t1, Object) and tyinstance(t2, Object):
        return reduce(and_fun, [less_precise(*tys) for tys in zip(t1.members, t2.members)], True)
    elif ((not tyinstance(t1, Choice)) and not tyinstance(t2, Choice)):
        #return prim_subtype(t2, t1) #need to invert the subtype relation here

        #for now just check if both types are numeric
        return prim(t1) and prim(t2)
    else: # not handling classes and objects for now
        pdb.set_trace()
        raise UnknownTypeError()

def less_precise_params(param_tys, arg_tys):
    if pinstance(param_tys, AnonymousParameters):
        return reduce(lambda x, y: x and y, [less_precise(param_ty, arg_ty) for param_ty, arg_ty in zip(param_tys.parameters, arg_tys)], True)
    elif pinstance(param_tys, NamedParameters):
        p_tys = list(map(lambda x: x[1], param_tys.parameters))
        return reduce(lambda x, y: x and y, [less_precise(param_ty, arg_ty) for param_ty, arg_ty in list(zip(p_tys, arg_tys))], True)
    elif pinstance(param_tys, DynParameters):
        return True
    elif tyinstance(param_tys, Choice):
        left = less_precise_params(selL(param_tys.name, param_tys), arg_tys)
        right = less_precise_params(selR(param_tys.name, param_tys), arg_tys)
        return makeChoice(param_tys.name, left, right)
    else:
        raise UnknownTypeError()

def subtype_params(param_tys, arg_tys):
    if pinstance(param_tys, AnonymousParameters):
        return reduce(lambda x, y: x and y, [less_precise(param_ty, arg_ty) for param_ty, arg_ty in zip(param_tys.parameters, arg_tys)], True)
    elif pinstance(param_tys, NamedParameters):
        p_tys = list(map(lambda x: x[1], param_tys.parameters))
        return reduce(lambda x, y: x and y, [less_precise(param_ty, arg_ty) for param_ty, arg_ty in list(zip(p_tys, arg_tys))], True)
    elif pinstance(param_tys, DynParameters):
        return True
    elif tyinstance(param_tys, Choice):
        left = subtype_params(selL(param_tys.name, param_tys), arg_tys)
        right = subtype_params(selR(param_tys.name, param_tys), arg_tys)
        return makeChoice(param_tys.name, left, right)
    else:
        raise UnknownTypeError()
        

def prim_subtype(t1, t2):
    prims = [Bool, Int, Float, Complex]
    #if isinstance(t1, Class) or isinstance(t2, Class) or isinstance(t1, Object) or isinstance(t2, Object):
    #    pdb.set_trace()
    t1tys = [tyinstance(t1, ty) for ty in prims]
    t2tys = [tyinstance(t2, ty) for ty in prims]
    if not(any(t1tys)) or not(any(t2tys)):
        return False
    return t1tys.index(True) <= t2tys.index(True)

def primjoin(tys, min=Int, max=Complex):
    try:
        ty = tys[0]
        for ity in tys[1:]:
            if prim_subtype(ty, ity):
                ty = ity
        if not prim_subtype(ty, max):
            return Dyn
        if prim_subtype(ty, min):
            return min
        else: return ty
    except UnexpectedTypeError:
        return Dyn
    except IndexError:
        return Dyn
def prim(ty):
    return any(tyinstance(ty, t) for t in [Bool, Int, Float, Complex])

def binop_type(l, op, r):
    #NOTE: I am replacing isntances of InfoTop being returned with Dyn
    if tyinstance(l, InferBottom) or tyinstance(r, InferBottom):
        return InferBottom
    if not flags.MORE_BINOP_CHECKING and (tyinstance(l, Dyn) or tyinstance(r, Dyn)):
        return Dyn    

    def intlike(ty):
        return any(tyinstance(ty, t) for t in [Bool, Int])
    def arith(op):
        return any(isinstance(op, o) for o in [ast.Add, ast.Mult, ast.Div, ast.FloorDiv, ast.Sub, ast.Pow, ast.Mod])
    def shifting(op):
        return any(isinstance(op, o) for o in [ast.LShift, ast.RShift])
    def logical(op):
        return any(isinstance(op, o) for o in [ast.BitOr, ast.BitAnd, ast.BitXor])
    def listlike(ty):
        return any(tyinstance(ty, t) for t in [List, String])

    if isinstance(op, ast.FloorDiv):
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [String, Complex, List, Tuple, Dict]):
            return InfoTop
    if isinstance(op, ast.Mod):
        if any(tyinstance(l, ty) for ty in [Complex, List, Tuple, Dict]):
            return InfoTop
    if shifting(op) or logical(op):
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Float, Complex, String, List, Tuple, Dict]):
            return InfoTop
    if arith(op):
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Dict]):
            return InfoTop
        if not isinstance(op, ast.Add) and not isinstance(op, ast.Mult) and \
                any(tyinstance(nd, ty) for nd in [l, r] for ty in [String, List, Tuple]):
            return InfoTop
    if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Object, Dyn]):
        return Dyn
    
    if tyinstance(l, Bool):
        if arith(op) or shifting(op) or isinstance(op, ast.Mod):
            if isinstance(op, ast.Div) and prim_subtype(r, Float):
                return Float
            if tyinstance(r, Bool):
                return Int
            elif prim(r):
                return r
            elif listlike(r) and isinstance(op, ast.Mult):
                return r
            elif tyinstance(r, Tuple) and isinstance(op, ast.Mult):
                return Dyn
            else: return InfoTop
        elif logical(op):
            return r
        else:
            return InfoTop
    elif tyinstance(l, Int):
        if isinstance(op, ast.Div) and prim_subtype(r, Float):
            return Float
        elif listlike(r) and isinstance(op, ast.Mult):
            return r
        elif isinstance(r, Tuple) and isinstance(op, ast.Mult):
            return Dyn
        elif prim_subtype(r, l):
            return l
        elif prim_subtype(l, r):
            return r
        else:
            return InfoTop
    elif prim(l):
        if prim_subtype(r, l):
            return l
        elif prim_subtype(l, r):
            return r
        else:
            return InfoTop
    elif listlike(l):
        if intlike(r) and isinstance(op, ast.Mult):
            return l
        elif tyinstance(l, String) and isinstance(op, ast.Mod):
            return l
        elif any(tyinstance(l, ty) and tyinstance(r, ty) for ty in [List, String]) and isinstance(op, ast.Add):
            return tyjoin([l, r])
        else:
            return InfoTop
    elif tyinstance(l, Tuple):
        if intlike(r) and isinstance(op, ast.Mult):
            return Dyn
        elif tyinstance(r, Tuple) and isinstance(op, ast.Add):
            return Tuple(*(l.elements + r.elements))
        else:
            return InfoTop
    else:
        return Dyn

def infer_binop_type(l, op, r):
    if tyinstance(l, InferBottom) or tyinstance(r, InferBottom):
        return {}, 0, InferBottom
    if not flags.MORE_BINOP_CHECKING and (tyinstance(l, Dyn) or tyinstance(r, Dyn)):
        return {}, 2, Dyn
    #pdb.set_trace()


    def intlike(ty):
        return any(tyinstance(ty, t) for t in [Bool, Int])
    def arith(op):
        return any(isinstance(op, o) for o in [ast.Add, ast.Mult, ast.Div, ast.FloorDiv, ast.Sub, ast.Pow, ast.Mod])
    def shifting(op):
        return any(isinstance(op, o) for o in [ast.LShift, ast.RShift])
    def logical(op):
        return any(isinstance(op, o) for o in [ast.BitOr, ast.BitAnd, ast.BitXor])
    def listlike(ty):
        return any(tyinstance(ty, t) for t in [List, String])


    #if isinstance(op, ast.Mod):
    #    if any(tyinstance(l, ty) for ty in [Complex, List, Tuple, Dict]):
    #        return {}, 2, InfoTop
    if listlike(l):
        if intlike(r) and isinstance(op, ast.Mult):
            return {}, 2, l
        elif tyinstance(l, String) and isinstance(op, ast.Mod):
            return {}, 2, l
        elif any(tyinstance(l, ty) and tyinstance(r, ty) for ty in [List, String]) and isinstance(op, ast.Add):
            return {}, 2, tyjoin([l, r])
        else:
            return {}, 2, Dyn
    if logical(op) or shifting(op):
        lsub, lpat, lty = unify(l, Int)
        rsub, rpat, rty = unify(r, Int)
        return (compose(lsub, rsub), patternMeet(lpat, rpat), Int)
    if arith(op):
        (lsub, lpat, lty) =  unify(l, Float)
        (rsub, rpat, rty) =  unify(r, Float)
        return (compose(lsub, rsub), patternMeet(lpat, rpat), Float)
    else:
        return {}, 0, Dyn

def subcompat(ty1, ty2, env=None, ctx=None):
    if env == None:
        env = {}

    if not ty1.top_free() or not ty2.top_free():
        return False

    return subtype(env, ctx, merge(ty1, ty2), ty2)

def normalize(ty):
    if ty == int:
        return Int
    elif ty == bool:
        return Bool
    elif ty == float:
        return Float
    elif ty == type(None):
        return Void
    elif ty == complex:
        return Complex
    elif ty == str:
        return String
    elif ty == None:
        return Dyn
    elif isinstance(ty, tuple):
        return Tuple(*[normalize(t) for t in ty])
    elif isinstance(ty, dict):
        nty = {}
        for k in ty:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty[k])
        return Object('', nty)
    elif ty is Object:
        return normalize(Object('', {}))
    elif ty is Class:
        return normalize(Class('', {}, {}))
    elif tyinstance(ty, Object):
        nty = {}
        for k in ty.members:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty.members[k])
        return Object(ty.name, nty)
    elif tyinstance(ty, Class):
        nty = {}
        for k in ty.members:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty.members[k])
        ity = {}
        for k in ty.instance_members:
            if type(k) != str:
                raise UnknownTypeError()
            ity[k] = normalize(ty.instance_members[k])
        return Class(ty.name, nty, ity)
    elif tyinstance(ty, Tuple):
        return Tuple(*[normalize(t) for t in ty.elements])
    elif tyinstance(ty, Function):
        return Function(normalize_params(ty.froms), normalize(ty.to))
    elif tyinstance(ty, Dict):
        return Dict(normalize(ty.keys), normalize(ty.values))
    elif tyinstance(ty, List):
        return List(normalize(ty.type))
    elif tyinstance(ty, Set):
        return Set(normalize(ty.type))
    elif tyinstance(ty, Iterable):
        return Iterable(normalize(ty.type))
    elif isinstance(ty, PyType):
        return ty
    else: raise UnknownTypeError(ty)

def normalize_params(params):
    if pinstance(params, AnonymousParameters):
        return AnonymousParameters([normalize(p) for p in params.parameters])
    elif pinstance(params, NamedParameters):
        return NamedParameters([(k, normalize(p)) for k,p in params.parameters])
    elif pinstance(params, DynParameters):
        return params
    else: raise UnknownTypeError()

def widen(*types):
    join = tyjoin(types)

def tyjoin(*types):
    if isinstance(types[0], list) and len(types) == 1:
        types = types[0]
    if len(types) == 0:
        return Dyn
    if all(tyinstance(x, InferBottom) for x in types):
        return InferBottom
    types = [ty for ty in types if not tyinstance(ty, InferBottom)]
    if len(types) == 0:
        return Dyn
    join = types[0]
    if tyinstance(join, Dyn):
        return Dyn
    new_types = types[1:]
    #if any(prim(ty) for ty in types[1:]) and (any(tyinstance(ty, Object) or tyinstance(ty, Class) for ty in types[1:])):
    #    return Dyn
    for ty in new_types:
        if not flags.FLAT_PRIMITIVES:
            pjoin = primjoin([ty,join])
            if not tyinstance(pjoin, Dyn):
                join = pjoin

        if not tyinstance(ty, join.__class__) or \
                tyinstance(ty, Dyn):
            return Dyn
        elif tyinstance(ty, TypeVariable):
            if ty.name == join.name:
                continue
            else: return Dyn
        elif tyinstance(ty, List):
            join = List(tyjoin([ty.type, join.type]))
        elif tyinstance(ty, Tuple):
            if len(ty.elements) == len(join.elements):
                join = Tuple(*[tyjoin(list(p)) for p in zip(ty.elements, join.elements)])
            else: return Dyn
        elif tyinstance(ty, Dict):
            join = Dict(tyjoin([ty.keys, join.keys]), tyjoin([ty.values, join.values]))
        elif tyinstance(ty, Function):
            join = Function(paramjoin(ty.froms, join.froms), 
                            tyjoin([ty.to, join.to]))
        elif tyinstance(ty, Choice):
            join = makeChoice(ty.name, tyjoin(selL(ty.name, ty), join), tyjoin(selR(ty.name, ty), join), ty.kind) 
        elif tyinstance(ty, Object) or tyinstance(ty, Class):
            name = ty.name if ty.name == join.name else ''
            members = {}
            for x in ty.members:
                if x in join.members:
                    members[x] = tyjoin([ty.members[x], join.members[x]])
            if tyinstance(ty, Class) and tyinstance(join, Class):
                imems = {}
                for x in ty.instance_members:
                    if x in join.instance_members:
                        imems[x] = tyjoin([ty.instance_members[x], join.instance_members[x]])
                join = Class(name, members, imems)
            else: join = ty.__class__(name,members)
        if join == Dyn: return Dyn    
    return join

def paramjoin(p1, p2):
    if pinstance(p1, DynParameters):
        return p1
    elif pinstance(p2, DynParameters):
        return p2
    elif pinstance(p1, NamedParameters):
        if len(p1.parameters) != len(p2.parameters):
            return DynParameters
        elif pinstance(p2, NamedParameters):
            if all(k1 == k2 for (k1, _), (k2, _) in zip(p1.parameters, p2.parameters)):
                return NamedParameters([(k1, tyjoin(t1, t2)) for (k1, t1), (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
            else: 
                return AnonymousParameters([tyjoin(t1, t2) for (_, t1), (_, t2) in\
                                                zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, (_, t2) in\
                                            zip(p2.parameters, p1.parameters)])
        else: raise UnknownTypeError()
    elif pinstance(p1, AnonymousParameters):
        if len(p1.parameters) != len(p2.parameters):
            return DynParameters
        elif pinstance(p2, NamedParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, (_, t2) in\
                                             zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, t2 in\
                                             zip(p1.parameters, p2.parameters)])
        else: raise UnknownTypeError()
    else: raise UnknownTypeError()

def shallow(ty):
    return ty.__class__
    
def param_subtype(env, ctx, p1, p2):    
    if p1 == p2:
        return True
    elif pinstance(p1, NamedParameters):
        if pinstance(p2, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(((k1 == k2 or not flags.PARAMETER_NAME_CHECKING) and subtype(env, ctx, f2, f1)) for\
                        (k1,f1), (k2,f2) in zip(p1.parameters, p2.parameters)) # Covariance handled here
        elif pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(subtype(env, ctx, f2, f1) for (_, f1), f2 in\
                        zip(p1.parameters, p2.parameters))
        else: return False
    elif pinstance(p1, AnonymousParameters):
        if pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(subtype(env, ctx, f2, f1) for f1, f2 in zip(p1.parameters,
                                                                p2.parameters))
        elif pinstance(p1, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and len(p1.parameters) == 0
        else: return False
    else: return False
        
def param_equal(env, ctx, p1, p2):
    if p1 == p2:
        return True
    elif pinstance(p1, NamedParameters):
        if pinstance(p2, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(((k1 == k2 or not flags.PARAMETER_NAME_CHECKING) and equal(env, ctx, f2, f1)) for\
                        (k1,f1), (k2,f2) in zip(p1.parameters, p2.parameters)) # Covariance handled here
        elif pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(equal(env, ctx, f2, f1) for (_, f1), f2 in\
                        zip(p1.parameters, p2.parameters))
        else: return False
    elif pinstance(p1, AnonymousParameters):
        if pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(equal(env, ctx, f2, f1) for f1, f2 in zip(p1.parameters,
                                                                p2.parameters))
        elif pinstance(p1, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and len(p1.parameters) == 0
        else: return False
    else: return False
        
def subtype(env, ctx, ty1, ty2):
    if not flags.FLAT_PRIMITIVES and prim_subtype(ty1, ty2):
        return True
    elif ty1 == ty2:
        return True
    elif tyinstance(ty2, InfoTop):
        return True
    elif tyinstance(ty1, InferBottom) or tyinstance(ty2, InferBottom):
        return True
    elif tyinstance(ty2, Bytes):
        return tyinstance(ty1, Bytes)
    elif tyinstance(ty2, Iterable):
        if tyinstance(ty1, List):
            return equal(env, ctx, ty1.type, ty2.type)
        elif tyinstance(ty1, Tuple):
            return equal(env, ctx, tyjoin(ty1.elements), ty2.type)
        elif tyinstance(ty1, Set):
            return equal(env, ctx, ty1.type, ty2.type)
        elif tyinstance(ty1, Iterable):
            return equal(env, ctx, ty1.type, ty2.type)
        elif tyinstance(ty1, Dict):
            return equal(env, ctx, ty1.keys, ty2.type)
        else:
            return False
    elif tyinstance(ty2, List):
        if tyinstance(ty1, List):
            return equal(env, ctx, ty1.type, ty2.type)
        else: return False
    elif tyinstance(ty2, Tuple):
        if tyinstance(ty1, Tuple):
            return len(ty1.elements) == len(ty2.elements) and \
                all(equal(env, ctx, e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements))
        else: return False
    elif tyinstance(ty2, Function):
        if tyinstance(ty1, Function):
            return param_subtype(env, ctx, ty1.froms, ty2.froms) and subtype(env, ctx, ty1.to, ty2.to) # Covariance DOES NOT happen here, it's in param_subtype
        elif tyinstance(ty1, Class):
            if '__new__' in ty1.members:
                fty = ty1.member_type('__new__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            elif '__init__' in ty1.members:
                fty = ty1.member_type('__init__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            else: fty = Function(DynParameters, ty1.instance())
            return subtype(env, ctx, fty, ty2)
        elif tyinstance(ty1, Object):
            if '__call__' in ty1.members:
                return subtype(env, ctx, ty1.member_type('__call__'), ty2)
            else: return False
        else: return False
    elif tyinstance(ty2, Object):
        if tyinstance(ty1, Object):
            for m in ty2.members:
                if m not in ty1.members or \
                   not equal(env, ctx, 
                             # We don't want to do member_type because
                             # if theres a subclass relation btwn t1
                             # and t2 and the superclass is an alias
                             # in one of them, will do differing
                             # amounts of subsitution
                             ty1.members[m].substitute(ty1.name, TypeVariable(ty2.name), False),
                             ty2.members[m]):
                    logging.debug('Object not a subtype due to member %s: %s =/= %s' %\
                                  (m, ty1.member_type(m, None),
                                   ty2.member_type(m)), flags.SUBTY)
                    return False
            return True
        elif tyinstance(ty1, Self):
            if ctx:
                return subtype(env, ctx, ctx.instance(), ty2)
            else:
                return True
        else: return False
    elif tyinstance(ty2, Class):
        if tyinstance(ty1, Class):
            return all((m in ty1.members and \
                        equal(env, ctx, ty1.member_type(m), ty2.member_type(m))) \
                       for m in ty2.members) and \
                           all((m in ty1.instance_members and \
                                equal(env, ctx, ty1.instance_member_type(m), 
                                      ty2.instance_member_type(m)) for m in ty2.instance_members))
        else: return False
    elif tyinstance(ty2, Self):
        if ctx:
            return subtype(env, ctx, ty1, ctx.instance())
        else:
            return tyinstance(ty1, Object)
    elif tyinstance(ty2, TypeVariable):
        typ = env[ty2] if ty2 in env else Dyn
        return subtype(env, ctx, ty1, typ)
    elif tyinstance(ty1, TypeVariable):
        typ = env[ty1] if ty1 in env else Dyn
        return subtype(env, ctx, typ, ty2)
    elif tyinstance(ty1, Base):
        return tyinstance(ty2, shallow(ty1))
    elif tyinstance(ty1, Choice):
        left = selL(ty1.name, ty1)
        right = selR(ty1.name, ty1)
        return makeChoice(ty1.name, subtype(env, ctx, left, ty2), subtype(env, ctx, right, ty2))
    elif tyinstance(ty2, Choice):
        left = selL(ty2.name, ty2)
        right = selR(ty2.name, ty2)
        return makeChoice(ty2.name, subtype(env, ctx, ty1, left), subtype(env, ctx, ty1, right))
    else: return False

# Type equality modulo type variable unrolling -- needed for equirecursive types
def equal(env, ctx, ty1, ty2):
    if not flags.FLAT_PRIMITIVES and prim_subtype(ty1, ty2):
        return True
    elif ty1 == ty2:
        return True
    elif isinstance(ty2, TypeVariable) and tyinstance(ty1, TypeVariable):
        return ty2.name == ty1.name
    elif tyinstance(ty2, InfoTop):
        return True
    elif tyinstance(ty1, InferBottom) or tyinstance(ty2, InferBottom):
        return True
    elif tyinstance(ty2, Bytes):
        return tyinstance(ty1, Bytes)
    elif tyinstance(ty2, List):
        if tyinstance(ty1, List):
            return equal(env, ctx, ty1.type, ty2.type)
        else: return False
    elif tyinstance(ty2, Tuple):
        if tyinstance(ty1, Tuple):
            return len(ty1.elements) == len(ty2.elements) and \
                all(equal(env, ctx, e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements))
        else: return False
    elif tyinstance(ty2, Function):
        if tyinstance(ty1, Function):
            return param_equal(env, ctx, ty1.froms, ty2.froms) and equal(env, ctx, ty1.to, ty2.to)
        elif tyinstance(ty1, Class):
            if '__new__' in ty1.members:
                fty = ty1.member_type('__new__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            elif '__init__' in ty1.members:
                fty = ty1.member_type('__init__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            else: fty = Function(DynParameters, ty1.instance())
            return equal(env, ctx, fty, ty2)
        elif tyinstance(ty1, Object):
            if '__call__' in ty1.members:
                return equal(env, ctx, ty1.member_type('__call__'), ty2)
            else: return False
        else: return False
    elif tyinstance(ty2, Object):
        if tyinstance(ty1, Object):
            for m in ty2.members:
                if m not in ty1.members or \
                   not equal(env, ctx, 
                             # We don't want to do member_type because
                             # if theres a subclass relation btwn t1
                             # and t2 and the superclass is an alias
                             # in one of them, will do differing
                             # amounts of subsitution
                             ty1.members[m].substitute(ty1.name, TypeVariable(ty2.name), False),
                             ty2.members[m]):
                    logging.debug('Object not equal due to member %s: %s =/= %s' %\
                                  (m, ty1.member_type(m, None),
                                   ty2.member_type(m)), flags.SUBTY)
                    return False
            return all(m in ty2.members for m in ty1.members)
        elif tyinstance(ty1, Self):
            if ctx:
                return equal(env, ctx, ctx.instance(), ty2)
            else:
                return True
        else: return False
    elif tyinstance(ty2, Class):
        if tyinstance(ty1, Class):
            return all((m in ty1.members and \
                        equal(env, ctx, ty1.member_type(m), ty2.member_type(m))) \
                       for m in ty2.members) and \
                           all((m in ty1.instance_members and \
                                equal(env, ctx, ty1.instance_member_type(m), 
                                      ty2.instance_member_type(m)) for m in ty2.instance_members)) \
                           and all(m in ty2.members for m in ty1.members) and \
                           all(m in ty2.instance_members for m in ty1.members)
        else: return False
    elif tyinstance(ty2, Self):
        if ctx:
            return equal(env, ctx, ty1, ctx.instance())
        else:
            return tyinstance(ty1, Object)
    elif tyinstance(ty2, TypeVariable):
        return equal(env, ctx, ty1, env[ty2])
    elif tyinstance(ty1, TypeVariable):
        return equal(env, ctx, env[ty1], ty2)
    elif tyinstance(ty1, Base):
        return tyinstance(ty2, shallow(ty1))
    else:
        return False
    

def merge(ty1, ty2):
    if tyinstance(ty1, Dyn):
        return ty2
    elif tyinstance(ty2, Dyn):
        return Dyn
    elif tyinstance(ty1, Choice):
        return makeChoice(ty1.name, merge(selL(ty1.name, ty1), selL(ty1.name, ty2)), merge(selR(ty1.name, ty1), selR(ty1.name, ty2)), ty1.kind)
    elif tyinstance(ty2, Choice):
        return makeChoice(ty2.name, merge(selL(ty2.name, ty1), selL(ty2.name, ty2)), merge(selR(ty2.name, ty1), selR(ty2.name, ty2)), ty2.kind)
    elif tyinstance(ty1, List):
        if tyinstance(ty2, List):
            return List(merge(ty1.type, ty2.type))
        elif tyinstance(ty2, Tuple):
            return Tuple(*[merge(ty1.type, ty2m) for ty2m in ty2.elements])
        else: return ty1
    elif tyinstance(ty1, Dict):
        if tyinstance(ty2, Dict):
            return Dict(merge(ty1.keys, ty2.keys), merge(ty1.values, ty2.values))
        else: return ty1
    elif tyinstance(ty1, Tuple):
        if tyinstance(ty2, Tuple) and len(ty1.elements) == len(ty2.elements):
            return Tuple(*[merge(e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements)])
        else: return ty1
    elif tyinstance(ty1, Object):
        if tyinstance(ty2, Object):
            nty = {}
            for n in ty1.members:
                if n in ty2.members:
                    nty[n] = merge(ty1.members[n],ty2.members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.members:
                    if n not in nty:
                        nty[n] = ty2.members[n]
            return Object(ty1.name, nty)
        elif tyinstance(ty2, Function):
            if '__call__' in ty1.members:
                cty = merge(ty1.members['__call__'],ty2)
            else: cty = ty2
            return Object(ty1.name, {'__call__': ty2})
        else: return ty1
    elif tyinstance(ty1, Class):
        if tyinstance(ty2, Class):
            nty = {}
            for n in ty1.members:
                if n in ty2.members:
                    nty[n] = merge(ty1.members[n],ty2.members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.members:
                    if n not in nty:
                        nty[n] = ty2.members[n]
            ity = {}
            for n in ty1.instance_members:
                if n in ty2.instance_members:
                    nty[n] = merge(ty1.instance_members[n],ty2.instance_members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.instance_members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.instance_members:
                    if n not in nty:
                        nty[n] = ty2.instance_members[n]
            return Class(ty1.name, nty, ity)
        else: return ty1
    elif tyinstance(ty1, Function):
        if tyinstance(ty2, Function):
            return Function(merge_params(ty1.froms, ty2.froms), merge(ty1.to, ty2.to))
        else: return ty1
    else: return ty1

def merge_params(p1, p2):
    if pinstance(p1, DynParameters):
        return p2
    elif pinstance(p2, DynParameters):
        return DynParameters
    else:
        args = p2.lenmatch(p1.parameters)
        if args == None:
            return p1
        elif pinstance(p1, AnonymousParameters):
            return AnonymousParameters([merge(t1, t2) for t1, t2 in args])
        elif pinstance(p1, NamedParameters):
            return NamedParameters([(k, merge(t1, t2)) for (k,t1), t2 in zip(p1.parameters, map(lambda x: x[1], args))])
        else: raise UnknownTypeError()

def occurs(t1, t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return occ
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or occurs(t1, t)
            occ = occ or occurs(t1, t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or occurs(t1, t)
            occ = occ or occurs(t1, t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or occurs(t1, e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return occurs(t1, underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return occurs(t1, t2.keys) or occurs(t1, t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or occurs(t1, n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or occurs(t1, n)
        return occ
    else:
        return t1 == t2

def hasDyn(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return True
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasDyn(t)
            occ = occ or hasDyn(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasDyn(t)
            occ = occ or hasDyn(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasDyn(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasDyn(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasDyn(t2.keys) or hasDyn(t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or hasDyn(n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or hasDyn(n)
        return occ
    elif (tyinstance(t2, Choice)):
        lty = selL(t2.name, t2)
        rty = selR(t2.name, t2)
        return hasDyn(lty) or hasDyn(rty)
    else:
        return t2 == Dyn

def hasClassOrObject(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return True
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasClassOrObject(t)
            occ = occ or hasClassOrObject(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasClassOrObject(t)
            occ = occ or hasClassOrObject(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasClassOrObject(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasClassOrObject(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasClassOrObject(t2.keys) or hasClassOrObject(t2.values)
    elif (tyinstance(t2, Object)):
        return True
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        return True
    elif (tyinstance(t2, Choice)):
        lty = selL(t2.name, t2)
        rty = selR(t2.name, t2)
        return hasClassOrObject(lty) or hasClassOrObject(rty)
    else:
        return t2 == Dyn

def hasChoice(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return hasChoice(t2.to)
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasChoice(t)
            occ = occ or hasChoice(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasChoice(t)
            occ = occ or hasChoice(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasChoice(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasChoice(underlying)
    elif (tyinstance(t2, Iterable)):
        #check underlying type
        underlying = t2.type
        return hasChoice(underlying)
    elif (tyinstance(t2, Set)):
        #check underlying type
        underlying = t2.type
        return hasChoice(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasChoice(t2.keys) or hasChoice(t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or hasChoice(n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or hasChoice(n)
        return occ
    elif (tyinstance(t2, Choice)):
        return True
    else:
        return False

    
def hasTvar(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return occ
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasTvar(t)
            occ = occ or hasTvar(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasTvar(t)
            occ = occ or hasTvar(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasTvar(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasTvar(underlying)
    elif (tyinstance(t2, Iterable)):
        #check underlying type
        underlying = t2.type
        return hasTvar(underlying)
    elif (tyinstance(t2, Set)):
        #check underlying type
        underlying = t2.type
        return hasTvar(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasTvar(t2.keys) or hasTvar(t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or hasTvar(n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or hasTvar(n)
        return occ
    elif (tyinstance(t2, Choice)):
        lty = selL(t2.name, t2)
        rty = selR(t2.name, t2)
        return hasTvar(lty) or hasTvar(rty)
    else:
        return tyinstance(t2, TypeVariable)

def patternMeet(p1, p2):
    if (tyinstance(p1, Choice)):
        left = patternMeet(selL(p1.name, p1), selL(p1.name, p2))
        right = patternMeet(selR(p1.name, p1), selR(p1.name, p2))
        return makeChoice(p1.name, left, right, p1.kind)
    elif (tyinstance(p2, Choice)):
        left = patternMeet(selL(p2.name, p1), selL(p2.name, p2))
        right = patternMeet(selR(p2.name, p1), selR(p2.name, p2))
        return makeChoice(p2.name, left, right, p2.kind)
    else:
        return  min(p1, p2)

def domSub(sub):
    return sub.keys()

def mergeSubs(chc, sub1, sub2, kind):
    combinedSub = list(sub1.keys()) + list(sub2.keys())
    #combDom = domSub(combinedSub)
    newSubs = {}
    for tv in combinedSub:
        if tv in sub1 and tv in sub2:
            newSubs[tv] = makeChoice(chc, sub1[tv], sub2[tv], kind)
        elif tv in sub1 and tv not in sub2:
            newSubs[tv] = makeChoice(chc, sub1[tv], fresh_tvar(), kind)
        else:
            newSubs[tv] = makeChoice(chc, fresh_tvar(), sub2[tv], kind)
    return newSubs

def replaceFreeSymbols(cost):
    if tyinstance(cost, Choice):
        #NOTE removing selL selR makeChoice...
        #return Choice(cost.name, replaceFreeSymbols(cost.left), replaceFreeSymbols(selR(cost.name, cost)))
        return Choice(cost.name, replaceFreeSymbols(selL(cost.name,cost)), replaceFreeSymbols(selR(cost.name, cost)), cost.kind)
    else:
        retCost = cost
        for c in cost.free_symbols:
            retCost = retCost.subs(c, 10000)
        return retCost

def findMinimumDepth(cost, pattern, maxCount):
    def findMin(cost, pattern, currentCount):
        if (pattern == False or currentCount > maxCount):
            return float("inf"), ""
        elif tyinstance(cost, Choice):
            if tyinstance(pattern, Choice):
                lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern), currentCount + 1)
                rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name,pattern), currentCount + 1)
            else:
                lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern), currentCount + 1)
                rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name, pattern), currentCount + 1)
            if (lcost < rcost):
                return lcost,  cost.name + ".1," + lpath
            else:
                return rcost,  cost.name + ".2," + rpath
        else:
            retCost = cost
            for c in cost.free_symbols:
                retCost = retCost.subs(c, 10000)
            return retCost, ""
        #return cost, ""
    return findMin(cost, pattern, 0)

def findMinimum(cost, pattern):
    if (pattern == False):
        return float("inf"), ""
    elif tyinstance(cost, Choice):
        if tyinstance(pattern, Choice):
            lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern))
            rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name,pattern))
        else:
            lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern))
            rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name, pattern))
        if (lcost < rcost):
            return lcost,  cost.name + ".1," + lpath
        else:
            return rcost,  cost.name + ".2," + rpath
    else:
        retCost = cost
        for c in cost.free_symbols:
            retCost = retCost.subs(c, 10000)
        return retCost, ""
        #return cost, "
        
def codSub(sub):
    return sub.values()

def applySubOld(ty, sub):
    if tyinstance(ty, TypeVariable) and ty in sub:
        return sub[ty]
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            newFroms = NamedParameters(list(map(lambda el: (el[0], applySub(el[1], sub)), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
        elif tyinstance(ty.froms, AnonymousParameters):
            newFroms = AnonymousParameters(list(map(lambda el: applySub(el, sub), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
        else:
            newFroms = ty.froms
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
    elif tyinstance(ty, Choice):
        return reduceChoices(makeChoice(ty.name, applySub(selL(ty.name,ty), sub), applySub(selR(ty.name,ty), sub), ty.kind))
    elif (tyinstance(ty, List)):
        return reduceChoices(List(applySub(ty.type, sub)))
    elif (tyinstance(ty, Dict)):
        return reduceChoices(Dict(applySub(ty.keys, sub), applySub(ty.values, sub)))
    elif (tyinstance(ty, Tuple)):
        return reduceChoices(Tuple(*list(map(lambda el: applySub(el, sub), ty.elements))))
    else:
        return ty

def applySub(ty, sub):
    if (sub == {}):
        return ty
    if tyinstance(ty, TypeVariable) and ty in sub:
        return sub[ty]
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            newFroms = NamedParameters(list(map(lambda el: (el[0], applySub(el[1], sub)), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return (Function(newFroms, newTo))
        elif tyinstance(ty.froms, AnonymousParameters):
            newFroms = AnonymousParameters(list(map(lambda el: applySub(el, sub), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return (Function(newFroms, newTo))
        else:
            newFroms = ty.froms
            newTo = applySub(ty.to, sub)
            return (Function(newFroms, newTo))
    elif tyinstance(ty, Choice):
        return (makeChoice(ty.name, applySub(selL(ty.name,ty), sub), applySub(selR(ty.name,ty), sub), ty.kind))
    elif (tyinstance(ty, List)):
        return (List(applySub(ty.type, sub)))
    elif (tyinstance(ty, Set)):
        return (Set(applySub(ty.type, sub)))
    elif (tyinstance(ty, Iterable)):
        return (Iterable(applySub(ty.type, sub)))
    elif (tyinstance(ty, Dict)):
        return (Dict(applySub(ty.keys, sub), applySub(ty.values, sub)))
    elif (tyinstance(ty, Tuple)):
        return (Tuple(*list(map(lambda el: applySub(el, sub), ty.elements))))
    else:
        return ty

def compose(sub1, sub2):
    newSub = sub1.copy()
    for i,v in sub2.items():
        if i in sub1 and i in sub2 and i.local and sub1[i] != sub2[i]:
            #sub2[i] = Dyn
            newSub[i] = Dyn
        else:
            sub2[i] = applySub(v, newSub)        
        if i not in newSub:
            newSub[i] = sub2[i]
    return newSub

def isPlain(ty):
    return tyinstance(ty, Int) or tyinstance(ty, Float) or tyinstance(ty, Bytes) or tyinstance(ty, String) or tyinstance(ty, Bool)

def allLeft(ty):
    if (tyinstance(ty, Choice)):
        left = allLeft(selL(ty.name, ty))
        return left
    elif (tyinstance(ty, List)):
         return List(allLeft(ty.type))
    elif tyinstance(ty, Tuple):
        return Tuple(*[allLeft(t) for t in ty.elements])
    elif tyinstance(ty, Dict):
        return Dict(allLeft(ty.keys), allLeft(ty.values))
    elif tyinstance(ty, Object):
        newTy = ty.copy()
        newTy.values = [allLeft(v) for v in newTy.members]
        return newTy
    elif tyinstance(ty, Set):
        return Set(allLeft(ty.type))
    elif tyinstance(ty, Iterable):
        return Iterable(allLeft(ty.type))
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            names = [n[0] for n in ty.froms.parameters]
            types = [t[1] for t in ty.froms.parameters]
            selTypes = map(allLeft, types)
            newPList = list(zip(names, selTypes))
            return Function(NamedParameters(newPList), allLeft(ty.to))
        elif tyinstance(ty.froms, AnonymousParameters):
            selTypes = map(allLeft, ty.froms.parameters)
            return Function(AnonymousParameters(list(selTypes)), allLeft(ty.to))
        else:
            return Function(ty.froms, allLeft(ty.to))
    else:
        return ty #not handling class for now
    
def validElims(pattern):
    descSet = []
    def inner(pat, currentDesc):
        if (pat == 2):
            descSet.append(currentDesc)
            return
        if (tyinstance(pat, Choice)):
            currentDesc.append((lambda x: selL(pat.name, x)))
            inner(selL(pat.name, pat), currentDesc)
            currentDesc.append((lambda x: selR(pat.name, x)))
            inner(selR(pat.name, pat), currentDesc)
        else:
            return
    inner(pattern, [])
    return descSet


def expand(desc, choiceList):
    newDescList = desc[:]
    chcList = list(choiceList)
    def inner(i):
        if (i < 0):
            return 
        else:
            newDescList.append(lambda x: (selR(chcList[i], x)))
            return inner(i-1)
    inner(len(chcList)-1)
    return newDescList

def boundChoices(ty):    
    if (tyinstance(ty, Choice)):
        leftChoices = boundChoices(selL(ty.name, ty))
        rightChoices = boundChoices(selR(ty.name, ty))
        return set([ty.name]).union(leftChoices).union(rightChoices)
    elif (tyinstance(ty, List)):
         return boundChoices(ty.type)
    elif tyinstance(ty, Tuple):
        return reduce(lambda x, y: x.union(y), [boundChoices(t) for t in ty.elements], set())
    elif tyinstance(ty, Dict):
        return boundChoices(ty.keys).union(boundChoices(ty.values))
    elif tyinstance(ty, Object):
        return reduce(lambda x, y: x.union(y), [boundChoices(v) for v in ty.members], set())
    elif tyinstance(ty, Set):
        return boundChoices(ty.type)
    elif tyinstance(ty, Iterable):
        return boundChoices(ty.type)
    elif tyinstance(ty, Function):
        return_choices = boundChoices(ty.to)
        if tyinstance(ty.froms, NamedParameters):
            types = [boundChoices(t[1]) for t in ty.froms.parameters]
            return reduce(lambda x, y: x.union(y), types, return_choices)
        elif tyinstance(ty.froms, AnonymousParameters):
            selTypes = reduce(lambda x, y: x.union(y), [boundChoices(v) for v in ty.froms.parameters], return_choices)
            return selTypes
        elif tyinstance(ty.froms, DynParameters):
            return return_choices
        else:
            return reduce(lambda x, y: x.union(y), map(boundChoices, ty.froms), return_choices)
    else:
        return set() #not handling class for now
    
    
def reduceChoices(ty):
    if (tyinstance(ty, Choice)):
        left = reduceChoices(selL(ty.name, ty))
        right = reduceChoices(selR(ty.name, ty))
        return makeChoice(ty.name, left, right, ty.kind)
    elif (tyinstance(ty, List)):
         return List(reduceChoices(ty.type))
    elif tyinstance(ty, Tuple):
        return Tuple(*[reduceChoices(t) for t in ty.elements])
    elif tyinstance(ty, Dict):
        return Dict(reduceChoices(ty.keys), reduceChoices(ty.values))
    elif tyinstance(ty, Object):
        newTy = ty.copy()
        newTy.values = [reduceChoices(v) for v in newTy.values]
        return newTy            
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            names = [n[0] for n in ty.froms.parameters]
            types = [t[1] for t in ty.froms.parameters]
            selTypes = map(reduceChoices, types)
            newPList = list(zip(names, selTypes))
            return Function(NamedParameters(newPList), reduceChoices(ty.to))
        elif tyinstance(ty.froms, AnonymousParameters):
            selTypes = map(reduceChoices, ty.froms.parameters)
            return Function(AnonymousParameters(list(selTypes)), reduceChoices(ty.to))
        else:
            return Function(ty.froms, reduceChoices(ty.to))
    else:
        return ty #not handling class for now

def liftToFunc(ty):
    if (tyinstance(ty, Function)):
        #if tyinstance(ty.froms, DynParameters):
        #    dom = DynParameters
        #elif tyinstance(ty.froms, AnonymousParameters):
        #    dom = ty.froms.parameters
        #else:
        #    dom = [x[1] for x in ty.froms.parameters]
        return ty.froms, ty.to, 2, {}
    elif (tyinstance(ty, Choice)):
        ltydom, ltycod, lpat, subl = liftToFunc(selL(ty.name,ty))
        rtydom, rtycod, rpat, subr = liftToFunc(selR(ty.name, ty))                
        return makeChoice(ty.name, ltydom, rtydom, ty.kind), makeChoice(ty.name, ltycod, rtycod, ty.kind), makeChoice(ty.name, lpat, rpat, ty.kind), mergeSubs(ty.name, subl, subr, ty.kind)
    elif (tyinstance(ty, Dyn)):
        return DynParameters, Dyn, 2, {}
    elif (tyinstance(ty, TypeVariable)):
        var_fun = fresh_opaque_var if (ty.opaque) else fresh_tvar
        sub, pat, ty = unify(ty, Function(AnonymousParameters([var_fun()]), var_fun()))
        dom = ty.froms
        cod = ty.to
        return dom, cod, pat, sub
    else:
        return AnonymousParameters([Dyn]), Dyn, 0, {}

def isSlicable(ty):
    if tyinstance(ty, Choice):
        left = isSlicable(selL(ty.name, ty))
        right = isSlicable(selR(ty.name, ty))
        return makeChoice(ty.name, left, right, ty.kind)
    else:
        return any(tyinstance(ty, kind) for kind in [List, Tuple, String, Bytes, Dyn, TypeVariable])

def get_ast_vars(node):
    if isinstance(node, ast.List):
        return [get_ast_vars(x) for x in node.elts]
    elif isinstance(node, ast.Tuple):
        return [get_ast_vars(x) for x in node.elts]
    elif isinstance(node, ast.Param):
        return [x]

def set_inferred(ty):
    if tyinstance(ty, Choice):
        left_type = selL(ty.name, ty)
        right_type = selR(ty.name, ty)
        return makeChoice(ty.name, set_inferred(left_type), set_inferred(right_type), ty.kind)
    else:
        ty.inferred = True
        return ty
    
def unify(t1, t2):
    if (t1 == Dyn):
        return {}, 2, t2
    elif (t2 == Dyn):
        return {}, 2, t1
    elif (tyinstance(t1, TypeVariable)):
        #sub t1 for t2 with occurs check
        if (t1 != t2 and occurs(t1, t2)):
            return {}, 0, t1
        if t1.local:
            sub = {t1: t2}
            return sub, 2, t2
        elif (hasDyn(t2)):
            if (tyinstance(t2, Function)):
                if (tyinstance(t2.froms, DynParameters)):
                    var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                    fVars = Function(AnonymousParameters([var_fun()]), var_fun())
                    (newSub, pat, ty) = unify(t1, fVars)
                    (secSub, secPat, resTy) =  unify(fVars, t2)
                    return (compose(secSub, newSub), patternMeet(secPat, pat), resTy)
                elif (tyinstance(t2.froms, Choice)):
                    lParams = selL(t2.froms.name, t2.froms)
                    rParams = selR(t2.froms.name, t2.froms)
                    lFunc = Function(lParams, t2.to)
                    rFunc = Function(rParams, t2.to)
                    return unify(makeChoice(t2.froms.name, t1, t1,t2.froms.kind), makeChoice(t2.froms.name, lFunc, rFunc, t2.froms.kind))
                else:
                    var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                    domList = [var_fun() for _ in t2.froms.parameters]
                    fVars = Function(AnonymousParameters(domList), var_fun())
                    (newSub, pat, ty) = unify(t1, fVars)
                    (secSub, secPat, resTy) =  unify(fVars, t2)
                    sub = compose(secSub, newSub)
                    return (sub, patternMeet(secPat, pat), applySub(sub, resTy))                        
            elif (tyinstance(t2, Choice)):
                #pdb.set_trace()
                name = t2.name
                kind = t2.kind
                newTy = Choice(name, t1, t1, kind)
                return unify(newTy, t2)
            elif (tyinstance(t2, Object)):
                newSub = sub
                pat = 2
                newObj = t2.copy()
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                for field in t2.members:
                    newObj.members[field] = var_fun()
                (sub, pat, ty) = unify(newObj, t2)
                inferred_type = set_inferred(ty)
                sub[t1] = inferred_type
                return (sub, pat, ty)
            elif (tyinstance(t2, List)):
                underlying = t2.type
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                fv = var_fun()
                (sub1, pat1, ty1) = unify(t1, List(fv))
                (newSub, newPat, ty2) = unify (fv, underlying)
                return (compose(newSub, sub1), patternMeet(pat1, newPat), List(ty2))
            elif (tyinstance(t2, Iterable)):
                underlying = t2.type
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                fv = var_fun()
                (sub1, pat1, ty1) = unify(t1, Iterable(fv))
                (newSub, newPat, ty2) = unify (fv, underlying)
                return (compose(newSub, sub1), patternMeet(pat1, newPat), Iterable(ty2))
            elif (tyinstance(t2, Set)):
                underlying = t2.type
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                fv = var_fun()
                (sub1, pat1, ty1) = unify(t1, Set(fv))
                (newSub, newPat, ty2) = unify (fv, underlying)
                return (compose(newSub, sub1), patternMeet(pat1, newPat), Set(ty2))
            elif (tyinstance(t2, Tuple)):
                tvarList = []
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                for ty in t2.elements:
                    tvarList.append(var_fun())
                tempType = Tuple(*tvarList)
                firstSub, firstPat, ty1 = unify(t1, tempType)
                newSub, newPat, ty2 =  unify(tempType, t2)
                return (compose(newSub, firstSub), patternMeet(newPat, firstPat), ty2)
            elif (tyinstance(t2, Dict)):
                var_fun = fresh_opaque_var if (t1.opaque) else fresh_tvar
                key = var_fun()
                val = var_fun()
                sub1, pat1, ty1 = unify(t1, Dict(key, val))
                sub2, pat2, ty2 = unify(Dict(key, val), t2)
                return (compose(sub2, sub1), patternMeet(pat2, pat1), ty2)
            elif (tyinstance(t2, Class)):
                #class_obj = Object(t2.name, t2.members)
                return {t1 : t2}, 2, t2               
            else:
                return {}, 0, t1               
            #elif (tyinstance(t2, list)):
            #    newSub = sub
            #    freshVars = 
            #    for ty in t2:
            #        newSub = unify(fresh_tvar() , ty, newSub)
            #    return newSub
        else:
            pat = 2
            #pdb.set_trace()
            if (t1.opaque or (tyinstance(t2, TypeVariable) and t2.opaque)):
                pat = 1 #using 1 for maybe pattern. Should probably switch to enum for Patterns
            sub = {}
            copied_type = copy.deepcopy(t2)
            inferred_type = set_inferred(copied_type)
            sub[t1] = inferred_type            
            return (sub, pat, t2)
    elif (tyinstance(t2, TypeVariable)):
        return unify(t2, t1)
    elif (tyinstance(t1, Function) and tyinstance(t2, Function)):
        if (tyinstance(t1.froms, DynParameters)): 
            pat1 = 2
            sub, pat2, codty = unify(t1.to, t2.to)
            return sub, pat2, Function(t2.froms, codty)
        elif tyinstance(t2.froms, DynParameters):
            pat1 = 2
            sub, pat2, codty = unify(t1.to, t2.to)
            return sub, pat2, Function(t1.froms, codty)
        elif (tyinstance(t1.froms, NamedParameters)):
            params = t1.froms.parameters
            paramNames = [x[0] for x in params]
            types1 = [x[1] for x in params]
            if (tyinstance(t2.froms, NamedParameters)):
                otherParams = t2.froms.parameters
                names2 = [x[0] for x in otherParams]
                types2 = [x[1] for x in otherParams]
                zippedTypes = zip(types1, types2)
                pat = 2
                joinedDom = []
                sub = {}
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                zippedParams = list(zip(paramNames, joinedDom))
                codSub, codPat, codTy = unify(t1.to, t2.to)                                
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
            elif (tyinstance(t2.froms, Choice)):
                    lParams = selL(t2.froms.name, t2.froms)
                    rParams = selR(t2.froms.name, t2.froms)
                    lFunc = Function(lParams, t2.to)
                    rFunc = Function(rParams, t2.to)
                    return unify(makeChoice(t2.froms.name, t1, t1, t2.froms.kind), makeChoice(t2.froms.name, lFunc, rFunc, t2.froms.kind))
            else: #Anonymous parameters
                paramTypes = t2.froms.parameters
                zippedTypes = zip(types1, paramTypes)
                joinedDom = []
                sub = {}
                pat = 2
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)
                zippedParams = list(zip(paramNames, joinedDom))
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
                #t2 Anonymous params
        elif (tyinstance(t1.froms, Choice)):
            lParams = selL(t1.froms.name, t1.froms)
            rParams = selR(t1.froms.name, t1.froms)
            lFunc = Function(lParams, t1.to)
            rFunc = Function(rParams, t1.to)
            return unify(makeChoice(t1.froms.name, lFunc, rFunc, t1.froms.kind), makeChoice(t1.froms.name, t2, t2, t1.froms.kind))    
        else:
            types1 = t1.froms.parameters
            if (tyinstance(t2.froms, NamedParameters)):
                otherParams = t2.froms.parameters
                namedParams = [x[0] for x in otherParams]
                types2 = [x[1] for x in otherParams]
                zippedTypes = zip(types1, types2)
                pat = 2
                joinedDom = []
                sub = {}
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)
                zippedParams = list(zip(namedParams, joinedDom))
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
            else:
                paramTypes = t2.froms.parameters
                zippedTypes = zip(types1, paramTypes)
                joinedDom = []
                sub = {}
                pat = 2
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)                                
                return compose(codSub, sub), patternMeet(codPat, pat), Function(AnonymousParameters(joinedDom), codTy)
            #t1 Anonymous params
    elif (tyinstance(t1, Tuple) and tyinstance(t2, Tuple)):
        zippedTypes = zip(t1.elements, t2.elements)
        pat = 2
        tupList = []
        sub = {}
        for ty1, ty2 in zippedTypes:
            sub_new, pat2, ty = unify(ty1, ty2)
            sub = compose(sub, sub_new)
            pat = patternMeet(pat, pat2)
            tupList.append(ty)
        return sub, pat, Tuple(*tupList)
    elif (tyinstance(t1, List) and tyinstance(t2, List)):
        sub, pat, ty = unify (t1.type, t2.type)
        return sub, pat, List(ty)    
    elif (tyinstance(t1, Set) and tyinstance(t2, Set)):
        sub, pat, ty = unify (t1.type, t2.type)
        return sub, pat, Set(ty)
    elif (tyinstance(t1, Iterable) and tyinstance(t2, Iterable)):
        sub, pat, ty = unify (t1.type, t2.type)
        return sub, pat, Iterable(ty)
    elif (tyinstance(t1, Iterable) and any(tyinstance(t2, collection_ty) for collection_ty in [List, Set, Dict])):
        if tyinstance(t2, Dict):
            return unify(t1.type, t2.keys)
        else:
            return unify(t1.type, t2.type)
    elif (tyinstance(t2, Iterable) and any(tyinstance(t1, collection_ty) for collection_ty in [List, Set, Dict])):
        if tyinstance(t1, Dict):
            return unify(t2.type, t1.keys)
        else:
            return unify(t1.type, t2.type)                          
    elif (tyinstance(t1, Dict) and tyinstance(t2, Dict)):
        sub1, pat1, ty1 = unify (t1.keys, t2.keys)
        sub2, pat2, ty2 = unify (t1.values, t2.values)
        sub = compose(sub2, sub1)
        pat = patternMeet(pat2, pat1)
        return sub, pat, Dict(ty1,ty2)
    elif (tyinstance(t1, Choice)):
        if (tyinstance(t2, Choice) and t2.name == t1.name):
            (sub1, pat1, tl) = unify(selL(t1.name, t1), selL(t2.name, t2))
            (sub2, pat2, tr) = unify(selR(t1.name, t1), selR(t1.name, t2))
            pat = makeChoice(t1.name, pat1, pat2, t1.kind)
            sub = mergeSubs(t1.name, sub1, sub2, t1.kind)
            return sub, pat, makeChoice(t1.name, tl, tr, t1.kind)
        else:
            left = selL(t1.name, t2)
            right = selR(t1.name, t2)
            #pdb.set_trace()
            lifted = Choice(t1.name, left, right, t1.kind)
            return unify(t1, lifted)
    elif (tyinstance(t2, Choice)):
        return unify(t2, t1)
    elif (prim_subtype(t1, t2) or prim_subtype(t2, t1)):
        return {}, 2, tyjoin(t1, t2)
    elif  isPlain(t1) and isPlain(t2):
        if t1 == t2:
            return {}, 2, t1
        else:
            return {}, 0, Dyn
    else:
        return {}, 0, Dyn
            
            
        
                    



