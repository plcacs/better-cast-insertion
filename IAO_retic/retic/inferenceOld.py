from . import flags, utils, rtypes
from .relations import *
from .visitors import GatheringVisitor
from .typing import Var, StarImport
import sympy
import pdb

def updateEnv(env, sub):
    for f in env:
        t = env[f]
        newTy = applySub(t, sub)
        env[f] = newTy

class InferVisitor(GatheringVisitor):
    examine_functions = False
    def combine_expr(self, s1, s2):
        return s1 + s2
    combine_stmt = combine_expr
    combine_stmt_expr = combine_expr
    empty_stmt = list
    empty_expr = list        
        
    def infer(self, typechecker, locals, initial_locals, ns, env, misc, costEnv):
        lenv = {}
        env = env.copy()
        new_assignments = []
        while True:
            verbosity = flags.WARNINGS
            flags.WARNINGS = -1
            assignments = self.preorder(ns, env, misc, 
                                        typechecker, costEnv)
            flags.WARNINGS = verbosity
            #pdb.set_trace()
            while assignments:
                k, v = assignments[0]
                del assignments[0]
                if isinstance(k, ast.Name):
                    new_assignments.append((k,v))
                elif isinstance(k, tuple):
                    k,v = k
                    if isinstance(k, ast.Tuple) or isinstance(k, ast.List):
                        if tyinstance(v, Tuple):
                            assignments += (list(zip(k.elts, v.elements)))
                        elif tyinstance(v, Iterable) or tyinstance(v, List):
                            assignments += ([(e, v.type) for e in k.elts])
                        elif tyinstance(v, Dict):
                            assignments += (list(zip(k.elts, v.keys)))
                        elif tyinstance(v, Choice):                            
                            newTy = Tuple(*([fresh_tvar()] * len(k.elts)))
                            s, p, t = unify(newTy, v)
                            updateEnv(env, s)
                            assignments += (list(zip(k.elts, t.elements)))
                        else: assignments += ([(e, Dyn) for e in k.elts])
                elif isinstance(k, ast.Tuple) or isinstance(k, ast.List):
                    if tyinstance(v, Tuple):
                        assignments += (list(zip(k.elts, v.elements)))
                    elif tyinstance(v, Iterable) or tyinstance(v, List):
                        assignments += ([(e, v.type) for e in k.elts])
                    elif tyinstance(v, Dict):
                        assignments += (list(zip(k.elts, v.keys)))
                    elif tyinstance(v, Choice):
                            fList = []
                            nassigns = []
                            for var in k.elts:
                                fv = fresh_tvar()
                                fList.append(fv)
                                nassigns.append((var, fv))
                            newTy = Tuple(*fList)
                            s, p, t = unify(newTy, v)
                            updateEnv(env, s)
                            nassigns = list(map(lambda x: (x[0], applySub(x[1], s)), nassigns))                            
                            assignments += nassigns                            
                            #assignments += (list(zip(k.elts, t.elements)))
                    else: assignments += ([(e, Dyn) for e in k.elts])
            nlenv = {}
            for local in [local for local in locals if local not in initial_locals]:
                #if isinstance(local, TypeVariable) or isinstance(local, StarImport):
                #    continue
                ltys = [y for x,y in new_assignments if x.id == local.var]
                #NOTE: perhaps if I add choices sooner, this will work
                ty = tyjoin(ltys).lift()            
                nlenv[local] = ty
            #pdb.set_trace()
            # if nlenv == lenv:
            #     env.update({Var(k.var): initial_locals[k] for k in initial_locals})
            #     break
            # else:
            env.update(nlenv)
            lenv = nlenv
            return {k:env[k] if not tyinstance(env[k], InferBottom) else Dyn for k in env}
    
    def visitAssign(self, n, env, misc, typechecker, costEnv):
        _, costObj, vty, _, _ = typechecker.preorder(n.value, env, misc, costEnv)
        assigns = []
        for target in n.targets:
            #pdb.set_trace()
            (ntarget,_), cost, tty, b, c = typechecker.preorder(target, env, misc, costEnv)
            costObj += cost
            if not (flags.SEMANTICS == 'MONO' and isinstance(target, ast.Attribute) and \
                        not tyinstance(tty, Dyn)):
                assigns.append((ntarget,vty))
        return assigns
    def visitAugAssign(self, n, env, misc, typechecker, costEnv):
        optarget = utils.copy_assignee(n.target, ast.Load())

        assignment = ast.Assign(targets=[n.target], 
                                value=ast.BinOp(left=optarget,
                                                op=n.op,
                                                right=n.value,
                                                lineno=n.lineno),
                                lineno=n.lineno)
        return self.dispatch(assignment, env, misc, typechecker, costEnv)
    def visitFor(self, n, env, misc, typechecker, costEnv):
        target, c, tty, _, _ = typechecker.preorder(n.target, env, misc, costEnv)
        _, cost, ity, _, _ = typechecker.preorder(n.iter, env, misc, costEnv)
        costObj = c + cost
        body = self.dispatch_statements(n.body, env, misc, typechecker, costEnv)
        orelse = self.dispatch_statements(n.orelse, env, misc, typechecker, costEnv)
        return [(target, utils.iter_type(ity))] + body + orelse
    def visitFunctionDef(self, n, env, misc, typechecker, costEnv):
        costObj = sympy.Integer(0)
        return [(ast.Name(id=n.name, ctx=ast.Store()), env[Var(n.name)])]
    def visitClassDef(self, n, env, misc, typechecker, costEnv):
        costObj = sympy.Integer(0)
        return [(ast.Name(id=n.name, ctx=ast.Store()), env[Var(n.name)])]
    def visitImport(self, n, env, *args):
        costObj = sympy.Integer(0)
        return [(ast.Name(id=t.asname if t.asname is not None else t.name, ctx=ast.Store()), env[Var(t.asname if t.asname is not None else t.name)]) for t in n.names]
    def visitImportFrom(self, n, env, *args):
        costObj = sympy.Integer(0)
        if '*' in [t.name for t in n.names]:
            impenv = env[StarImport(n.module)]
            return [(ast.Name(id=t.var, ctx=ast.Store()), impenv[t]) for t in impenv if isinstance(t, Var)]
        return [(ast.Name(id=t.asname if t.asname is not None else t.name, ctx=ast.Store()), env[Var(t.asname if t.asname is not None else t.name)]) for t in n.names]
