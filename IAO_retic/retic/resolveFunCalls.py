from __future__ import print_function
import ast
from .vis import Visitor
from .gatherers import FallOffVisitor, WILL_RETURN
from .inference import InferVisitor
from .typing import *
from .relations import *
from .exc import StaticTypeError, UnimplementedException
from .errors import errmsg, static_val
from .astor.misc import get_binop
from . import typing, utils, flags, rtypes, reflection, annotation_removal, logging, runtime, ast_trans, selectionVisitors
from functools import reduce
#from sympy import *
import sympy 
import ast
import collections
import pdb

firstLetter = (ord('A') - 1)
patEnv = {}
functionEnv = collections.OrderedDict()
localEnv = {}
original_env = {}
class_name = None
        
def applyEnv(env, sub):
    if (sub == {}):
        return env
    newEnv = env.copy()
    for f in newEnv:
        t = newEnv[f]
        newTy = applySub(t, sub)
        newEnv[f] = newTy
    return newEnv

def updateEnv(env, sub):
    if (sub == {}):
        return env
    for f in env:
        t = env[f]
        newTy = applySub(t, sub)
        env[f] = newTy
    return env

def inc_char():
    global firstLetter 
    firstLetter += 1
    return chr(firstLetter)


def fixup(n, lineno=None, col_offset=None):
    if isinstance(n, list) or isinstance(n, tuple):
        return [fixup(e, lineno if lineno else e.lineno) for e in n]
    else:
        if lineno != None:
            n.lineno = lineno
        if col_offset != None:
            n.col_offset = None
        return ast.fix_missing_locations(n)

##Cast insertion functions##
#Normal casts
def cast(env, ctx, val, src, trg, msg, cast_function='retic_cast', misc=None):
    if flags.SEMANTICS == 'MGDTRANS':
        from . import mgd_typecheck
        return mgd_typecheck.cast(env, ctx, val, src, trg, msg, misc=misc)

    if flags.SEMI_DRY:
        return val
    if flags.SQUELCH_MESSAGES:
        msg = ''
    assert hasattr(val, 'lineno'), ast.dump(val)
    lineno = str(val.lineno)

    if hasTvar(src):
        src = relations.tvars_to_dyn(src)
    if hasTvar(trg):
        trg = relations.tvars_to_dyn(trg)
    merged = merge(src, trg)
    #NOTE: Removing top_free and subcompat checks
    #pdb.set_trace()
    #if not trg.top_free() or not subcompat(src, trg, env, ctx):
    #    return error(msg % static_val(src), lineno)
    if src == merged:
        return val
    if trg == Dyn and relations.prim(src): #This is a hotfix to prevent adding unnecessary calls to "cast" I will add this to the canonical implementation
        return val
    if not flags.OPTIMIZED_INSERTION:
        msg = '\n' + msg
        logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
        return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                              args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                              keywords=[], starargs=None, kwargs=None), val.lineno)
    else:
        msg = '\n' + msg
        if flags.SEMANTICS == 'MONO':
            logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
            return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                  args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                  keywords=[], starargs=None, kwargs=None), val.lineno)
        elif flags.SEMANTICS == 'TRANS':
            if not tyinstance(trg, Dyn):
                args = [val]
                cast_function = 'check_type_'
                if tyinstance(trg, Int):
                    cast_function += 'int'
                elif tyinstance(trg, Float):
                    cast_function += 'float'
                elif tyinstance(trg, String):
                    cast_function += 'string'
                elif tyinstance(trg, List):
                    cast_function += 'list'
                elif tyinstance(trg, Complex):
                    cast_function += 'complex'
                elif tyinstance(trg, Tuple):
                    cast_function += 'tuple'
                    args += [ast.Num(n=len(trg.elements))]
                elif tyinstance(trg, Dict):
                    cast_function += 'dict'
                elif tyinstance(trg, Bool):
                    cast_function += 'bool'
                elif tyinstance(trg, Set):
                    cast_function += 'set'
                elif tyinstance(trg, Function):
                    cast_function += 'function'
                elif tyinstance(trg, Void):
                    cast_function += 'void'
                elif tyinstance(trg, Class):
                    cast_function += 'class'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                elif tyinstance(trg, Object):                    
                    if len(trg.members) == 0:
                        return (val) #NOTE: temporary work around
                    cast_function += 'object'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                else:
                    logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
                    #NOTE: need to return a cost for transient return 0 in meantime
                    return (fixup(ast.Call(func=ast.Name(id='retic_cast', ctx=ast.Load()),
                                          args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                           keywords=[], starargs=None, kwargs=None), val.lineno))
                #NOTE: need to return a cost for transient return 0 in meantime
                return (fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                       args=args, keywords=[], starargs=None, kwargs=None)))
            else: return (val, sympy.Integer(0)) #NOTE: need to return a cost for transient return 0 in meantime
        elif flags.SEMANTICS == 'MGDTRANS':
            raise Exception('Should not be invoking this version of cast()')
        elif flags.SEMANTICS == 'GUARDED':            
            #NOTE: import guarded to call retic_cast directly
            from . import guarded
            #print("Val = %s" % val)
            #print("Src = %s" % src)
            #print("Trg = %s" % merged)
            #print("Cost = %s" % cost)
            logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
            #NOTE inital cost returned by cast in guarded
            #NOTE: Removing calls that actually insert casts!
            #return val, costObj
            return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                  args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                   keywords=[], starargs=None, kwargs=None), val.lineno)
        elif flags.SEMANTICS == 'NOOP':
            return val
        else: raise UnimplementedException('Efficient insertion unimplemented for this semantics')

# Casting with unknown source type, as in cast-as-assertion 
# function return values at call site
def check(val, trg, msg, check_function='retic_check', lineno=None, ulval=None):
    msg = '\n' + msg
    if flags.SEMI_DRY:
        return val
    if flags.SQUELCH_MESSAGES:
        msg = ''
    assert hasattr(val, 'lineno')
    lineno = str(val.lineno)

    if not flags.OPTIMIZED_INSERTION:
        logging.warn('Inserting check at line %s: %s' % (lineno, trg), 2)
        return fixup(ast.Call(func=ast.Name(id=check_function, ctx=ast.Load()),
                              args=[val, trg.to_ast(), ast.Str(s=msg)],
                              keywords=[], starargs=None, kwargs=None), val.lineno)
    else:
        if flags.SEMANTICS == 'TRANS':
            if not tyinstance(trg, Dyn):
                args = [val]
                cast_function = 'check_type_'
                if tyinstance(trg, Int):
                    cast_function += 'int'
                elif tyinstance(trg, Float):
                    cast_function += 'float'
                elif tyinstance(trg, String):
                    cast_function += 'string'
                elif tyinstance(trg, List):
                    cast_function += 'list'
                elif tyinstance(trg, Complex):
                    cast_function += 'complex'
                elif tyinstance(trg, Tuple):
                    cast_function += 'tuple'
                    args += [ast.Num(n=len(trg.elements))]
                elif tyinstance(trg, Dict):
                    cast_function += 'dict'
                elif tyinstance(trg, Bool):
                    cast_function += 'bool'
                elif tyinstance(trg, Void):
                    cast_function += 'void'
                elif tyinstance(trg, Set):
                    cast_function += 'set'
                elif tyinstance(trg, Function):
                    cast_function += 'function'
                elif tyinstance(trg, Class):
                    cast_function += 'class'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                elif tyinstance(trg, Object):
                    if len(trg.members) == 0:
                        return val
                    cast_function += 'object'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                else:
                    logging.warn('Inserting check at line %s: %s' % (lineno, trg), 2)
                    return fixup(ast.Call(func=ast.Name(id=check_function, ctx=ast.Load()),
                                          args=[val, trg.to_ast(), ast.Str(s=msg)],
                                          keywords=[], starargs=None, kwargs=None), val.lineno)

                return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                      args=args, keywords=[], starargs=None, kwargs=None))
                # return fixup(ast.IfExp(test=ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                #                                      args=args, keywords=[], starargs=None, kwargs=None),
                #                        body=val,
                #                        orelse=ast.Call(func=ast.Name(id='retic_error', ctx=ast.Load()),
                #                                        args=[ast.Str(s=msg)], keywords=[], starargs=None,
                #                                        kwargs=None)), val.lineno)
            else: return val
        elif flags.SEMANTICS == 'MGDTRANS':
            raise Exception('Should not be invoking this version of cast()')
            
        else: return val

# Check, but within an expression statement
def check_stmtlist(val, trg, msg, check_function='retic_check', lineno=None):
    if flags.SEMI_DRY:
        return []
    assert hasattr(val, 'lineno'), ast.dump(val)
    chkval = check(val, trg, msg, check_function, val.lineno)
    if not flags.OPTIMIZED_INSERTION:
        return [ast.Expr(value=chkval, lineno=val.lineno)]
    else:
        if flags.SEMANTICS not in ['TRANS', 'MGDTRANS'] or chkval == val or tyinstance(trg, Dyn):
            return []
        else: return [ast.Expr(value=chkval, lineno=val.lineno)]

# Insert a call to an error function if we've turned off static errors
def error(msg, lineno, error_function='retic_error'):
    if flags.STATIC_ERRORS or flags.SEMI_DRY:
        raise StaticTypeError(msg)
    else:
        logging.warn('Static error detected at line %d' % lineno, 0)
        return fixup(ast.Call(func=ast.Name(id=error_function, ctx=ast.Load()),
                              args=[ast.Str(s=msg+' (statically detected)')], keywords=[], starargs=None,
                              kwargs=None), lineno)

# Error, but within an expression statement
def error_stmt(msg, lineno, error_function='retic_error'):
    if flags.STATIC_ERRORS or flags.SEMI_DRY:
        raise StaticTypeError(msg)
    else:
        return [ast.Expr(value=error(msg, lineno, error_function), lineno=lineno)]

class MakeFastPass(Visitor):
    falloffvisitor = FallOffVisitor()

    def dispatch_debug(self, tree, *args):
        ret = super().dispatch(tree, *args)
        print('results of %s:' % tree.__class__.__name__)
        if isinstance(ret, tuple):
            if isinstance(ret[0], ast.AST):
                print(ast.dump(ret[0]))
            if isinstance(ret[1], PyType):
                print(ret[1])
        if isinstance(ret, ast.AST):
            print(ast.dump(ret))
        return ret

    if flags.DEBUG_VISITOR:
        dispatch = dispatch_debug

    def resolve(self, n, env, misc, functionEnv):
        #oldEnv = env
        #env = env.copy()
        
        n = fixup(n)
        #env.update(typing.initial_environment())
        #addTypesToEnv(env)
        #pdb.set_trace()
        tn = self.preorder(n, env, misc, functionEnv)
        tn0 = tn[0]
        ty = tn[1]
        pat = tn[2]
        #tn = [x[0] for x in tn]
        tn0 = fixup(tn0)
        #pdb.set_trace()
        #updateEnv(oldEnv, sub)
        if flags.DRY_RUN:
            return n
        
        return tn0, ty, pat

    def visitlist(self, n, env, misc, functionEnv):
        global localEnv
        body = [] 
        types = []
        pats = []
        pat = 2
        ty = rtypes.Dyn
        newEnv = env.copy()
        for s in n:
            stmts, ty, pat1 = self.dispatch(s, newEnv, misc, functionEnv)            
            body += stmts
            if not(isinstance(s, ast.FunctionDef)):
                pat = patternMeet(pat, pat1)
        return body, ty, pat
        
    def visitModule(self, n, env, misc, functionEnv):
        body, ty, pat = self.dispatch(n.body, env, misc, functionEnv)
        return ast.Module(body=body), ty, pat

    def default(self, n, *args):        
        if isinstance(n, ast.expr):
            return ((n, Dyn), Dyn, 2)
        elif isinstance(n, ast.stmt):
            return ([n], Dyn, 2)
        else: (n, Dyn, 2)

## STATEMENTS ##
    # Import stuff
    def visitImport(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    def visitImportFrom(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    # Function stuff
    def visitFunctionDef(self, n, env, misc, functionEnv): #TODO: check defaults, handle varargs and kwargs
        global localEnv
        global original_env
        global class_name
        
        (functionEnv, local_envs) = functionEnv

        
        name = n.name if n.name not in rtypes.TYPES else n.name + '_'
        if class_name != None:
            lookup_name = class_name + "_" + name #prepend class name to methods in the env
        else:
            lookup_name = name

        try:
            nty = instantiate(functionEnv[Var(lookup_name)])
            #nTy = instantiate(functionEnv[Var(lookup_name + "_vari")])
        except KeyError as e :
            assert False, ('%s at %s:%d' % (e ,misc.filename, n.lineno))
        #NOTE: gives the function type's domain and codomain Dyn unless it's annotated        
        froms = nty.froms if hasattr(nty, 'froms') else DynParameters
        #new_params = []
        #if (isinstance(froms, NamedParameters)):
        #    paramList = froms.parameters
        #    for i, j in paramList:
        #        if j == Dyn:
        #            new_params.append((i, rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())))
        #        else:
        #            new_params.append((i, j))
        #froms = NamedParameters(new_params)

        to = nty.to if (hasattr(nty, 'to') and nty.to != Dyn) else Dyn#rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())
        fty = Function(froms, to)
        if not misc.methodscope and not nty.self_free():
            error(errmsg('UNSCOPED_SELF', misc.filename, n), lineno=n.lineno)

        

        #NOTE: VERY IMPORTANT, I've commented out the higher order casts added to each function below. The new version casts from nty to nty
        #res, cost = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), Dyn, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)
        #res = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), Dyn, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)
        #commenting out the cast from Dyn to nty for now
        res = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), nty, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)
        #res_fast = cast(env, misc.cls, ast.Name(id=name+"_fast", ctx=ast.Load(), lineno=n.lineno), nty, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)
        
        
        assign = ast.Assign(targets=[ast.Name(id=name, ctx=ast.Store(), lineno=n.lineno)], 
                            value=res,
                            lineno=n.lineno)
        #assign_fast = ast.Assign(targets=[ast.Name(id=name+"_fast", ctx=ast.Store(), lineno=n.lineno)], 
        #                    value=res_fast,
        #                    lineno=n.lineno)
        ((args, argnames, specials), typ, pat) = self.dispatch(n.args, env, froms, misc, n.lineno, (functionEnv, local_envs))

        
        decorator_list = n.decorator_list#[self.dispatch(dec, env, misc)[0] for dec in n.decorator_list if not is_annotation(dec)]
        # Decorators have a restricted syntax that doesn't allow function calls
        env = (misc.extenv if misc.cls else env).copy()
        
        if misc.cls:
            receiver = None if (not misc.methodscope or len(argnames) == 0) else\
                ast.Name(id=argnames[0], ctx=ast.Load())
        else: 
            receiver = None


        argtys = froms.lenmatch([Var(x) for x in argnames])        
        assert(argtys != None)
        initial_locals = dict(argtys + specials)
        localEnv = initial_locals
        logging.debug('Function %s typechecker starting in %s' % (n.name, misc.filename), flags.PROC)

        #get the slowPass and fastPass versions at
        lty = instantiate(functionEnv[Var(lookup_name)])
        slowLocals = dict(localEnv)
        #fastLocals = dict(localEnv)
            
        slowArgTys = lty.froms.lenmatch([Var(x) for x in argnames])        
        for paramName, paramType in tvars_to_dyn(lty).froms.parameters:
            slowLocals[Var(paramName)] = paramType
            localEnv[Var(paramName)] = paramType
        #do I need to change misc.extenv?

        #get the local variables that are not parameters, for the fucntion
        non_parameter_locals = local_envs[Var(lookup_name)]
        #localEnv.update(non_parameter_locals)

        local_env_slow = local_envs[Var(lookup_name)]
        original_env = local_env_slow
        #pdb.set_trace()
        #for key in local_envs[Var(name)]:
        #    local_env_dyns[key] = env[key] #NOTE: used to be Dyn. I am going to try to fall back on Reticulated's local variable inference
        
        slow_ret_misc,  slow_bty, slow_bpat = misc.static.resolve(n.body, env, env, slowLocals, \
                                                                  typing.Misc(ret=to, cls=misc.cls, receiver=receiver, extenv=misc.extenv, gensymmer=misc.gensymmer, typenames=misc.typenames, extend=misc), (functionEnv, local_env_slow)) 
        

        slowBody, slowEnv = slow_ret_misc

        force_checks = tyinstance(froms, DynParameters)

        slowArgChecks = sum((check_stmtlist(ast.Name(id=arg.var, ctx=ast.Load(), lineno=n.lineno), ty, 
                                        errmsg('ARG_CHECK', misc.filename, n, arg.var, ty), \
                                            lineno=n.lineno) for (arg, ty) in slowArgTys), [])
        

        logging.debug('Returns checker starting in %s' % misc.filename, flags.PROC)
        fo = self.falloffvisitor.dispatch_statements(slowBody) #NOTE: probably need to find costs here
        logging.debug('Returns checker finished in %s' % misc.filename, flags.PROC)
        #NOTE: taking off this check, just make sure to unify type against void        
        #patEnv[Var(name)] = pat

        #for var_name, var_ty in local_env_slow.items():            
        #    print("The type for the function {0}'s variable, {1}, is: {2}".format(name, var_name, var_ty))
        #print("The pattern for the function {0} is: {1}".format(name, pat))
        
        localEnv = {}
        logging.debug('Function %s typechecker finished in %s' % (n.name, misc.filename), flags.PROC)

        return ([ast_trans.FunctionDef(name=name, args=args,
                                      body=slowArgChecks+slowBody, decorator_list=decorator_list,
                                      returns=(n.returns if hasattr(n, 'returns') else None),
                                       lineno=n.lineno), assign], nty, pat)
        
        
        
        #pdb.set_trace()
        #print("")

        #functionEnv[name] = env[Var(name)]

        #pdb.set_trace()        


        
            
            
            
        

    def visitarguments(self, n, env, nparams, misc, lineno, functionEnv):
        def argextract(arg):
            if flags.PY_VERSION == 3 and flags.PY3_VERSION >= 4:
                return arg.arg
            else: return arg
        specials = []
        if n.vararg:
            specials.append(Var(argextract(n.vararg), n))
        if n.kwarg:
            specials.append(Var(argextract(n.kwarg), n))
        if flags.PY_VERSION == 3 and n.kwonlyargs:
            specials += [Var(arg.arg, n) for arg in n.kwonlyargs]
        
        checked_args = nparams.lenmatch(n.args)
        if checked_args == None:
            pdb.set_trace()
        #pdb.set_trace()
        assert checked_args != None, '%s <> %s, %s, %d' % (nparams, ast.dump(n), misc.filename, lineno)
        checked_args = checked_args[-len(n.defaults):]

        defaults = []
        pat = 2
        tys = []
        for val, (k, ty) in zip(n.defaults, checked_args):
            ((val, vty), ptys, ppat) = self.dispatch(val, env, misc, functionEnv)
            res= cast(env, misc.cls, val, vty, ty, errmsg('DEFAULT_MISMATCH', misc.filename, lineno, k, ty), misc=misc)
            pat = patternMeet(pat, ppat)
            tys.append(ptys)
            defaults.append(res)
        #NOTE: typecheck each arg
        args, argns = tuple(zip(*[(self.visitarg(arg, env, misc, functionEnv))[0] for arg in n.args])) if\
            len(n.args) > 0 else ([], [])

        args = list(args)
        argns = list(argns)

        assert len(defaults) == len(n.defaults)

        if flags.PY_VERSION == 3:
            kw_defaults = [(fixup(self.dispatch(d, env, misc, functionEnv)[0], lineno) if d else None) for d in n.kw_defaults]

            nargs = dict(args=args, vararg=n.vararg,
                         kwonlyargs=n.kwonlyargs, kwarg=n.kwarg,
                         defaults=defaults, kw_defaults=kw_defaults)

            if flags.PY3_VERSION < 4:
                nargs['kwargannotation'] = None
                nargs['varargannotation'] = n.varargannotation
        elif flags.PY_VERSION == 2:
            nargs = dict(args=args, vararg=n.vararg, kwarg=None, defaults=defaults)
        return ((ast.arguments(**nargs), argns, [(k, Dyn) for k in specials]), tys, pat)

    def visitarg(self, n, env, misc, functionEnv):
        #NOTE: Function to handle if annotation is present?
        def annotation(n):
            if misc.cls:
                if isinstance(n, ast.Name) and n.id == misc.cls.name:
                    if misc.receiver:
                        return (ast.Attribute(value=misc.receiver, attr='__class__', ctx=ast.Load()))
                    else: return None
                elif isinstance(n, ast.Attribute):
                    return (ast.Attribute(value=annotation(n.value), attr=n.attr, ctx=n.ctx))
            return n
        if flags.PY_VERSION == 3:
            return ((ast.arg(arg=n.arg, annotation=annotation(n.annotation)), n.arg), Dyn, 2)
        else: return ((n, n.id), Dyn, 2) 
            
    def visitReturn(self, n, env, misc, functionEnv):
        global original_env
        (functionEnv, local_env) = functionEnv
        if n.value:
            ((value, ty), exp_ty, exp_pat) = self.dispatch(n.value, env, misc, (functionEnv, local_env))
            #NOTE: Adding a check below to avoid casts to less precise type, when the type inferred
            if isinstance(n.value, ast.Name) and Var(n.value.id) in local_env:
                if ty.inferred and less_precise(misc.ret, ty) and local_env != original_env:
                    original_type = original_env[Var(n.value.id)] if Var(n.value.id) in original_env else ty
                    value = cast(env, misc.cls, value, original_type, misc.ret, errmsg('RETURN_ERROR', misc.filename, n, misc.ret), misc=misc)
                else:
                    value = cast(env, misc.cls, value, ty, misc.ret, errmsg('RETURN_ERROR', misc.filename, n, misc.ret), misc=misc)
            else:
                value = cast(env, misc.cls, value, ty, misc.ret, errmsg('RETURN_ERROR', misc.filename, n, misc.ret), misc=misc)
        else:            
            value = None
            exp_ty = Void
            exp_pat = 2
            if not subcompat(Void, misc.ret):
                return (error_stmt(errmsg('RETURN_NONEXISTANT', misc.filename, n, misc.ret), lineno=n.lineno), Dyn, 0)
        return ([ast.Return(value=value, lineno=n.lineno)], exp_ty, exp_pat)

    # Assignment stuff
    def visitAssign(self, n, env, misc, functionEnv):
        ((val, vty), val_ty, val_pat) = self.dispatch(n.value, env, misc, functionEnv)
        ttys = []
        tyList = []
        targets = []
        attrs = []
        pat = val_pat
        for target in n.targets:
            ((ntarget, tty), cty, cpat) = self.dispatch(target, env, misc, functionEnv)
            #pdb.set_trace()
            tyList.append(tty)
            #tty = makeChoice(fresh_choice_name(), Dyn, fresh_tvar())
            pat = patternMeet(pat, cpat)
            #check if n.value is in functionEnv, make new temp functionEnv for assigns
            #Mutate functionEnv for now, but this is probably incorrect and may cause problems
            if (not isinstance(target, ast.Tuple) and isinstance(n.value, ast.Name) and hasattr(n, "id") and Var(n.value.id) in functionEnv):
                pass
                #functionEnv[Var(target.id)] = functionEnv[Var(n.value.id)]
            elif (not isinstance(target, ast.Tuple) and isinstance(n.value, ast.Attribute) and hasattr(n, "attr") and Var(n.value.attr) in functionEnv ):
                pass
                #functionEnv[Var(target.id)] = functionEnv[Var(n.value.attr)]
            if flags.SEMANTICS == 'MONO' and isinstance(target, ast.Attribute) and \
                    not tyinstance(tty, Dyn):
                attrs.append((ntarget, tty))
            else:
                ttys.append(tty)                
                targets.append(ntarget)
        stmts = []
        if targets:
            meet = n_info_join(ttys)
            if len(targets) == 1:
                err = errmsg('SINGLE_ASSIGN_ERROR', misc.filename, n, meet)
            else:
                err = errmsg('MULTI_ASSIGN_ERROR', misc.filename, n, ttys)

            #NOTE: removing casts on assignments for now
            if isinstance(n.targets[0], ast.Tuple):
                #pdb.set_trace()
                val = cast(env, misc.cls, val, vty, meet, err, misc=misc)            
            stmts.append(ast.Assign(targets=targets, value=val, lineno=n.lineno))
        for target, tty in attrs:
            lval = cast(env, misc.cls, val, vty, tty, errmsg('SINGLE_ASSIGN_ERROR', misc.filename, n, tty), misc=misc)
            stmts.append(ast.Expr(ast.Call(func=ast.Name(id='retic_setattr_'+\
                                                             ('static' if \
                                                                  tty.static() else 'dynamic'), 
                                                         ctx=ast.Load()),
                                           args=[target.value, ast.Str(s=target.attr), lval, tty.to_ast()],
                                           keywords=[], starargs=None, kwargs=None),
                                  lineno=n.lineno))


        finalPat = pat
        return (stmts, val_ty, finalPat)

    def visitAugAssign(self, n, env, misc, functionEnv):
        optarget = utils.copy_assignee(n.target, ast.Load())
        assignment = ast.Assign(targets=[n.target], 
                                value=ast.BinOp(left=optarget,
                                                op=n.op,
                                                right=n.value,
                                                lineno=n.lineno),
                                lineno=n.lineno)        
        return self.dispatch(assignment, env, misc, functionEnv)

    def visitDelete(self, n, env, misc, functionEnv):
        targets = []
        pat = 2
        for t in n.targets:
            ((value, ty), val_ty, val_pat) = self.dispatch(t, env, misc, functionEnv)
            targets.append(utils.copy_assignee(value, ast.Load()))
            pat = patternMeet(pat, val_pat)
        return ([ast.Expr(targ, lineno=n.lineno) for targ in targets] + \
                    [ast.Delete(targets=n.targets, lineno=n.lineno)], Dyn, 2)

    # Control flow stuff
    def visitIf(self, n, env, misc, functionEnv):
        ((test, tty), cond_ty, cond_pat) = self.dispatch(n.test, env, misc, functionEnv)
        newEnv1 = env        
        (body, body_ty, body_pat) = self.dispatch(n.body, newEnv1, misc, functionEnv)        
        (orelse, else_ty, else_pat) = self.dispatch(n.orelse, env, misc, functionEnv) if n.orelse else ([], Dyn, 2)       
        cF = Dyn if (body_ty != else_ty and n.orelse) else body_ty
        pat3 = patternMeet(patternMeet(body_pat, else_pat), cond_pat)
        finalPat = pat3
        
        #if statement will now return a choice with the type of its branches

        return ([ast.If(test=test, body=body, orelse=orelse, lineno=n.lineno)], cF, finalPat) 

    def visitFor(self, n, env, misc, functionEnv):
        ((target, tty),  targ_ty, targ_pat) = self.dispatch(n.target, env, misc, functionEnv)
        ((iter, ity),  iter_ty, iter_pat) = self.dispatch(n.iter, env, misc, functionEnv)
        pat1 = patternMeet(targ_pat, iter_pat)
        pat, ty = 2, Dyn
        
        pat2 = patternMeet(pat, pat1)
        newEnv1 = env
        (body, body_ty, body_pat) = self.dispatch(n.body, newEnv1, misc, functionEnv)
        (orelse, else_ty, else_pat) = self.dispatch(n.orelse, newEnv1, misc, functionEnv) if n.orelse else ([], Dyn, 2)
        branchPat = body_pat
        cF = body_ty
        if n.orelse:
            branchPat = patternMeet(body_pat, else_pat) #makeChoice(flowName, body_pat, else_pat, VariType.FLOW)
            cF = Dyn if body_ty != else_ty else body_ty
        finalPat = patternMeet(pat2, branchPat)
        #NOTE: not sure if I need to get a cost from below
        targcheck = check_stmtlist(utils.copy_assignee(target, ast.Load()),
                                   tty, errmsg('ITER_CHECK', misc.filename, n, tty), lineno=n.lineno)

        #Do not add the cost below!
        res = cast(env, misc.cls, iter, ity, iter_ty,
                            errmsg('ITER_ERROR', misc.filename, n, iter_ty), misc=misc)
        #NOTE: Very important to add objects here to keep track of iteration costs and not just constant cost
        return ([ast.For(target=target, iter=res,
                         body=targcheck+body, orelse=orelse, lineno=n.lineno)], cF, finalPat)
        
    def visitWhile(self, n, env, misc, functionEnv):
        ((test, tty), test_ty, test_pat) = self.dispatch(n.test, env, misc, functionEnv)
        newEnv = env
        (body, body_ty, body_pat) = self.dispatch(n.body, newEnv, misc, functionEnv)
        (orelse, else_ty, else_pat) = self.dispatch(n.orelse, newEnv, misc, functionEnv) if n.orelse else ([], Dyn, 2)
        branchPat = body_pat
        cF = body_ty
        if n.orelse:
            branchPat = patternMeet(branchPat, else_pat)
            cF = Dyn if body_ty != else_ty else body_ty
            
        finalPat = patternMeet(test_pat, branchPat)        
        
        #updateEnv(env, finalSub)        
        
        return ([ast.While(test=test, body=body, orelse=orelse, lineno=n.lineno)], cF, finalPat)

    def visitWith(self, n, env, misc, functionEnv):
        (body, body_ty, body_pat) = self.dispatch(n.body, env, misc, functionEnv)
        if flags.PY_VERSION == 3 and flags.PY3_VERSION >= 3:
            items = [self.dispatch(item, env, misc, functionEnv) for item in n.items]
            items = [x[0] for x in items]            
            return ([ast.With(items=items, body=body, lineno=n.lineno)], body_ty, body_pat)
        else:
            ((context_expr, _),  context_ty, context_pat) = self.dispatch(n.context_expr, env, misc, functionEnv)
            ((optional_vars, _),  op_ty, op_pat) = self.dispatch(n.optional_vars, env, misc, functionEnv) if\
                               n.optional_vars else ((None, Dyn), Dyn, 2)
            pat1 = patternMeet(context_pat, body_pat)
            finalPat = patternMeet(op_pat, pat1)
            return ([ast.With(context_expr=context_expr, optional_vars=optional_vars, 
                              body=body, lineno=n.lineno)], body_ty, finalPat)
    
    def visitwithitem(self, n, env, misc, functionEnv):
        ((context_expr, _), context_ty, context_pat) = self.dispatch(n.context_expr, env, misc, functionEnv)
        ((optional_vars, _),  op_ty, op_pat) = self.dispatch(n.optional_vars, env, misc, functionEnv) if\
                           n.optional_vars else ((None, Dyn), Dyn, 2)
        pat = patternMeet(context_pat, op_pat)
        return (ast.withitem(context_expr=context_expr, optional_vars=optional_vars), context_ty, pat)
        

    # Class stuff
    def visitClassDef(self, n, env, misc, functionEnv): #Keywords, kwargs, etc
        global class_name
        old_class_name = class_name
        bases = [ast.Call(func=ast.Name(id='retic_actual', ctx=ast.Load()), args=[base], 
                          kwargs=None, starargs=None, keywords=[]) for\
                base in [self.dispatch(base, env, misc, functionEnv)[0][0] for base in n.bases]] #NOTE: need to add costs in class
        keywords = [] # Will be ignored if py2
        if flags.PY_VERSION == 3:
            metaclass_handled = flags.SEMANTICS != 'MONO'
            for keyword in n.keywords:
                ((kval, _), kty, kpat) = self.dispatch(keyword.value, env, misc, functionEnv)
                if flags.SEMANTICS == 'MONO' and keyword.arg == 'metaclass':
                    metaclass_handled = True
                keywords.append(ast.keyword(arg=keyword.arg, value=kval))
            if not metaclass_handled:
                logging.warn('Adding Monotonic metaclass to classdef at line %s: <%s>' % (n.lineno,
                                                                                  n.name), 1)
                keywords.append(ast.keyword(arg='metaclass', 
                                            value=ast.Name(id=runtime.Monotonic.__name__,
                                                           ctx=ast.Load())))
        nty = env[Var(n.name)]
        oenv = misc.extenv if misc.cls else env.copy()
        env = env.copy()
        
        initial_locals = {Var(n.name, n): nty}

        stype = ast.Assign(targets=[ast.Name(id='retic_class_type', ctx=ast.Store(), 
                                             lineno=n.lineno)],
                           value=nty.to_ast(), lineno=n.lineno)

        logging.debug('Class %s typechecker starting in %s' % (n.name, misc.filename), flags.PROC)
        class_name = n.name
        ((rest, _), ty, pat) = misc.static.resolve(n.body, env, env, initial_locals, 
                                        typing.Misc(ret=Void, cls=nty, gensymmer=misc.gensymmer, typenames=misc.typenames,
                                                    methodscope=True, extenv=oenv, extend=misc), functionEnv)
        if flags.SEMANTICS not in ['MGDTRANS', 'TRANS']:
            body = [stype] + rest
        else:
            body = rest
        logging.debug('Class %s typechecker finished in %s' % (n.name, misc.filename), flags.PROC)

        name = n.name if n.name not in rtypes.TYPES else n.name + '_'
        res = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), Dyn, nty, 
                                       errmsg('BAD_CLASS_INJECTION', misc.filename, n, nty), misc=misc)
        assign = ast.Assign(targets=[ast.Name(id=name, ctx=ast.Store(), lineno=n.lineno)], 
                            value=res, lineno=n.lineno)
        class_name = old_class_name
        return ([ast_trans.ClassDef(name=name, bases=bases, keywords=keywords,
                                   starargs=(n.starargs if hasattr(n, 'starargs') else None),
                                   kwargs=(n.kwargs if hasattr(n, 'kwargs') else None), body=body,
                                    decorator_list=n.decorator_list, lineno=n.lineno), assign], nty, 2)

    # Exception stuff
    # Python 2.7, 3.2
    def visitTryExcept(self, n, env, misc, functionEnv):
        (body, body_ty, body_pat) = self.dispatch(n.body, env, misc, functionEnv)
        handlers = []
        pat = body_pat
        for handler in n.handlers:
            (handler, hty, hpat) = self.dispatch(handler, env, misc, functionEnv)
            handlers.append(handler)
            pat = patternMeet(pat, hpat)
        (orelse, else_ty, else_pat) = self.dispatch(n.orelse, env, misc, functionEnv) if n.orelse else ([], Dyn, 2)
        finalPat = patternMeet(pat, else_pat)
        return ([ast.TryExcept(body=body, handlers=handlers, orelse=orelse, lineno=n.lineno)], body_ty, finalPat)

    # Python 2.7, 3.2
    def visitTryFinally(self, n, env, misc, functionEnv):
        (body, body_ty, body_pat) = self.dispatch(n.body, env, misc, functionEnv)
        (finalbody, final_ty, final_pat) = self.dispatch(n.finalbody, env, misc, functionEnv)
        pat = patternMeet(final_pat, body_pat)
        return ([ast.TryFinally(body=body, finalbody=finalbody, lineno=n.lineno)], body_ty, pat)
    
    # Python 3.3
    def visitTry(self, n, env, misc, functionEnv):
        (body, body_ty, body_pat) = self.dispatch(n.body, env, misc, functionEnv)
        handlers = []
        pat = body_pat
        for handler in n.handlers:
            (handler, hty, hpat) = self.dispatch(handler, env, misc, functionEnv)
            handlers.append(handler)
            pat = patternMeet(pat, hpat)
        (orelse, ety, epat) = self.dispatch(n.orelse, env, misc, functionEnv) if n.orelse else ([], Dyn, 2)
        pat = patternMeet(pat, epat)
        (finalbody, fty, fpat) = self.dispatch(n.finalbody, env, misc, functionEnv)
        pat = patternMeet(pat, fpat)
        return ([ast.Try(body=body, handlers=handlers, orelse=orelse, finalbody=finalbody, lineno=n.lineno)], body_ty, pat)

    def visitExceptHandler(self, n, env, misc, functionEnv):
        ((type, tyty), ety, epat) = self.dispatch(n.type, env, misc, functionEnv) if n.type else ((None, Dyn), Dyn, 2)
        (body, bty, bpat) = self.dispatch(n.body, env, misc, functionEnv)
        pat = patternMeet(bpat, epat)
        if flags.PY_VERSION == 2 and n.name and type:
            ((name, nty), ntype, npat) = self.dispatch(n.name, env, misc, functionEnv)
            type = cast(env, misc.cls, type, tyty, nty, errmsg('EXCEPTION_ERROR', misc.filename, n, n.name, nty, n.name), misc=misc)
            pat = patternMeet(pat, npat)
        else: 
            name = n.name
        return (ast.ExceptHandler(type=type, name=name, body=body, lineno=n.lineno), ety, pat)

    def visitRaise(self, n, env, misc, functionEnv):
        if flags.PY_VERSION == 3:
            ((exc, _), ety, epat) = self.dispatch(n.exc, env, misc, functionEnv) if n.exc else ((None, Dyn), Dyn, 2)
            ((cause, _), cty, cpat) = self.dispatch(n.cause, env, misc, functionEnv) if n.cause else ((None, Dyn), Dyn, 2)
            pat = patternMeet(epat, cpat)
            return ([ast.Raise(exc=exc, cause=cause, lineno=n.lineno)], ety, pat)
        elif flags.PY_VERSION == 2:
            ((type, _), tty, tpat) = self.dispatch(n.type, env, misc, functionEnv) if n.type else ((None, Dyn), Dyn, 2)
            ((inst, _), ity, ipat) = self.dispatch(n.inst, env, misc, functionEnv) if n.inst else ((None, Dyn), Dyn, 2)
            ((tback, _), bty, bpat) = self.dispatch(n.tback, env, misc, functionEnv) if n.tback else ((None, Dyn), Dyn, 2)
            pat1 = patternMeet(tpat, ipat)
            pat = patternMeet(pat1, bpat)
            return ([ast.Raise(type=type, inst=inst, tback=tback, lineno=n.lineno)], tty, pat)

    def visitAssert(self, n, env, misc, functionEnv):
        ((test, _), tty, tpat) = self.dispatch(n.test, env, misc, functionEnv)
        ((msg, _), mty, mpat) = self.dispatch(n.msg, env, misc, functionEnv) if n.msg else ((None, Dyn), Dyn, 2)
        pat = patternMeet(tpat, mpat)
        return ([ast.Assert(test=test, msg=msg, lineno=n.lineno)], tty, pat)

    # Declaration stuff
    def visitGlobal(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    def visitNonlocal(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    # Miscellaneous
    def visitExpr(self, n, env, misc, functionEnv):
        ((value, ty), ety, epat) = self.dispatch(n.value, env, misc, functionEnv)
        return ([ast.Expr(value=value, lineno=n.lineno)], ety, epat)

    def visitPass(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    def visitBreak(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    def visitContinue(self, n, env, misc, functionEnv):
        return ([n], Dyn, 2)

    def visitPrint(self, n, env, misc, functionEnv):
        ((dest, _), dty, dpat) = self.dispatch(n.dest, env, misc, functionEnv) if n.dest else ((None, Void), Void, 2)
        (values) = [self.dispatch(val, env, misc, functionEnv) for val in n.values]
        values = [x[0] for x in values]
        val_tys = [x[1] for x in values]
        val_pats = [x[2] for x in values]
        pat = reduce(patternMeet, val_pats, 2)
        return ([ast.Print(dest=dest, values=values, nl=n.nl, lineno=n.lineno)], Void, pat)

    def visitExec(self, n, env, misc, functionEnv):
        ((body, _), bty, bpat) = self.dispatch(n.body, env, misc, functionEnv)
        ((globals, _), gty, gpat) = self.dispatch(n.globals, env, misc, functionEnv) if n.globals else ((None, Void),  Dyn, 2)
        ((locals, _), lty, lpat) = self.dispatch(n.locals, env, misc, functionEnv) if n.locals else ((None, Void), Dyn, 2)
        pat1 = patternMeet(bpat, gpat)
        pat = patternMeet(lpat, pat1)
        return ([ast.Exec(body=body, globals=globals, locals=locals, lineno=n.lineno)], bty, pat)

### EXPRESSIONS ###
    # Op stuff
    def visitBoolOp(self, n, env, misc, functionEnv):
        values = []
        tys = []
        for value in n.values: 
            ((value, ty), vty, vpat) = self.dispatch(value, env, misc, functionEnv)
            values.append(value)
            tys.append(ty)
        ty = tyjoin(tys)
        return ((ast.BoolOp(op=n.op, values=values, lineno=n.lineno), ty), vty, vpat)

    def visitBinOp(self, n, env, misc, functionEnv):
        ((left, lty), l_ty, lpat) = self.dispatch(n.left, env, misc, functionEnv)
        newEnv = env
        ((right, rty), r_ty, rpat) = self.dispatch(n.right, newEnv, misc, functionEnv)
        pat = patternMeet(lpat, rpat)
        ty = binop_type(l_ty, n.op, r_ty)
        #updateEnv(env, sub)
        #if not ty.top_free():
            #return ((error(errmsg('BINOP_INCOMPAT', misc.filename, n, lty, rty, get_binop(n.op)), lineno=n.lineno), Dyn), costObj, ty, pat, sub)
        node = ast.BinOp(left=left, op=n.op, right=right, lineno=n.lineno)
        return ((node, ty), ty, pat)

    def visitUnaryOp(self, n, env, misc, functionEnv):
        ((operand, ty), rty, rpat) = self.dispatch(n.operand, env, misc,functionEnv)
        node = ast.UnaryOp(op=n.op, operand=operand, lineno=n.lineno)
        if isinstance(n.op, ast.Invert):
            ty = primjoin([ty], Int, Int)
        elif any([isinstance(n.op, op) for op in [ast.UAdd, ast.USub]]):
            ty = primjoin([ty])
        elif isinstance(n.op, ast.Not):
            ty = Bool
        return ((node, ty), ty, rpat)

    def visitCompare(self, n, env, misc, functionEnv):        
        ((left, _), lty, lpat) = self.dispatch(n.left, env, misc, functionEnv)
        if (isinstance(n.ops[0], ast.Eq) or isinstance(n.ops[0], ast.NotEq)):
            comparators = [(comp, cty, cpat) for ((comp, _), cty, cpat) in [self.dispatch(ocomp, env, misc, functionEnv) for ocomp in n.comparators]]        
            comp_pats = [x[2] for x in comparators]
            
        
            pat = reduce(patternMeet, comp_pats, 2)
            pat = patternMeet(pat, lpat)
            comparators = [x[0] for x in comparators]
            return ((ast.Compare(left=left, ops=n.ops, comparators=comparators, lineno=n.lineno), Bool), Bool, pat)
        else:
            (left_pat, left_ty) = (2, Float) if lty in [Dyn, Int, Float] else (2, Dyn)
            #lsub, lpat, lty = unify(lty, Float)
            comparators = [(comp, cty, cpat) for ((comp, _), cty, cpat) in [self.dispatch(ocomp, env, misc, functionEnv) for ocomp in n.comparators]]        
            comp_pats = [x[2] for x in comparators]
        
            pat = reduce(patternMeet, comp_pats, 2)
            rty = (comparators[0])[2]
        
            (right_pat, right_ty) = (2, Float) if rty in [Dyn, Int, Float] else (2, Dyn)
            pat = patternMeet(pat, lpat)
            pat = patternMeet(pat, left_pat)
            pat = patternMeet(pat, right_pat)        
            comparators = [x[0] for x in comparators]
            return ((ast.Compare(left=left, ops=n.ops, comparators=comparators, lineno=n.lineno), Bool), Bool, pat)

    # Collections stuff    
    def visitList(self, n, env, misc, functionEnv):
        eltdata = [self.dispatch(x, env, misc, functionEnv) for x in n.elts]
        elttys = [ty for ((elt, ty), ety, epat) in eltdata] 
        elts = [elt for ((elt, ty), _, _) in eltdata]
        elpats = [epat for ((elt, ty), ety, epat) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        if isinstance(n.ctx, ast.Store):
            ty = Tuple(*elttys)
        else:
            inty = tyjoin(elttys)
            ty = List(inty) if flags.TYPED_LITERALS else Dyn
        return ((ast.List(elts=elts, ctx=n.ctx, lineno=n.lineno), ty), ty, pat)

    def visitTuple(self, n, env, misc, functionEnv):
        eltdata = [self.dispatch(x, env, misc, functionEnv) for x in n.elts]
        tys = [ty for ((elt, ty), ety, epat) in eltdata]
        elts = [elt for ((elt, ty), ety, epat) in eltdata]
        elpats = [epat for ((elt, ty), ety, epat) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        if isinstance(n.ctx, ast.Store):
            ty = Tuple(*tys)
        else:
            ty = Tuple(*tys) if flags.TYPED_LITERALS else Dyn
        return ((ast.Tuple(elts=elts, ctx=n.ctx, lineno=n.lineno), ty), ty, pat)

    def visitDict(self, n, env, misc, functionEnv):
        keys = [self.dispatch(key, env, misc, functionEnv) for key in n.keys]
        vals = [self.dispatch(val, env, misc, functionEnv) for val in n.values]
        keydata = [x[0] for x in keys]
        valdata = [x[0] for x in vals]
        keypats = [x[2] for x in keys]
        valpats = [x[2] for x in vals]
        pat1 = reduce(patternMeet, keypats, 2)
        pat2 = reduce(patternMeet, valpats, 2)
        pat = patternMeet(pat1, pat2)
        keys, ktys = list(zip(*keydata)) if keydata else ([], [])
        values, vtys = list(zip(*valdata)) if valdata else ([], [])
        return ((ast.Dict(keys=list(keys), values=list(values), lineno=n.lineno),\
                    Dict(tyjoin(list(ktys)), tyjoin(list(vtys)))), Dict(tyjoin(list(ktys)), tyjoin(list(vtys))), pat)

    def visitSet(self, n, env, misc, functionEnv):
        eltdata = [self.dispatch(x, env, misc, functionEnv) for x in n.elts]
        elttys = [ty for ((elt, ty),  _, _) in eltdata]
        elpats = [pat for ((elt, ty),  _, pat,) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        ty = tyjoin(elttys)
        elts = [elt for ((elt, ty),  _, _) in eltdata]
        return ((ast.Set(elts=elts, lineno=n.lineno), Set(ty) if flags.TYPED_LITERALS else opTy), opTy, pat)

    def visitListComp(self, n, env, misc, functionEnv):
        disp = [self.dispatch(generator, env, misc, n.lineno, functionEnv) for generator in n.generators]
        disp_pats = [x[2] for x in disp]
        pat = reduce(patternMeet, disp_pats, 2)
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety), elt_ty, elt_pat) = self.dispatch(n.elt, lenv, misc, functionEnv)
        pat = patternMeet(pat, elt_pat)
        return ((check(ast.ListComp(elt=elt, generators=list(generators), lineno=n.lineno), List(ety), errmsg('COMP_CHECK', misc.filename, n, List(ety))),\
                 (List(ety) if flags.TYPED_LITERALS else Dyn)), List(elt_ty), pat)

    def visitSetComp(self, n, env, misc, functionEnv):
        disp = [self.dispatch(generator, env, misc, n.lineno, functionEnv) for generator in n.generators]
        disp_pats = [x[2] for x in disp]
        pat = reduce(patternMeet, disp_pats, 2)
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety),  elt_ty, elt_pat) = self.dispatch(n.elt, lenv, misc, functionEnv)
        pat = patternMeet(pat, elt_pat)
        return ((check(ast.SetComp(elt=elt, generators=list(generators), lineno=n.lineno), Set(ety), errmsg('COMP_CHECK', misc.filename, n, Set(ety))), \
                 (Set(ety) if flags.TYPED_LITERALS else Dyn)), Set(elt_ty), pat)
    
    def visitDictComp(self, n, env, misc, functionEnv):
        disp = [self.dispatch(generator, env, misc, n.lineno, functionEnv) for generator in n.generators]
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv,[])))
        ((key, kty), key_ty, key_pat) = self.dispatch(n.key, lenv, misc, functionEnv)
        ((value, vty), val_ty, val_pat) = self.dispatch(n.value, lenv, misc, functionEnv)
        pat = patternMeet(val_pat, key_pat)
        return ((check(ast.DictComp(key=key, value=value, generators=list(generators), lineno=n.lineno), Dict(kty, vty), errmsg('COMP_CHECK', misc.filename, n, Dict(kty, vty))), \
                 (Dict(kty, vty) if flags.TYPED_LITERALS else Dyn)), Dict(key_ty, val_ty), pat)

    def visitGeneratorExp(self, n, env, misc, functionEnv):
        disp = [self.dispatch(generator, env, misc, n.lineno, functionEnv) for generator in n.generators]
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety), elty, pat) = self.dispatch(n.elt, lenv, misc, functionEnv)
        return ((check(ast.GeneratorExp(elt=elt, generators=list(generators), lineno=n.lineno), Dyn, errmsg('COMP_CHECK', misc.filename, n, Dyn)), Dyn), Dyn, pat)

    def visitcomprehension(self, n, env, misc, lineno, functionEnv):
        ((iter, ity), iter_ty, ipat) = self.dispatch(n.iter, env, misc, functionEnv)
        ifs = [if_ for ((if_, _), _, _) in [self.dispatch(if2, env, misc, functionEnv) for if2 in n.ifs]]
        ((target, tty), tty, tpat) = self.dispatch(n.target, env, misc, functionEnv)
        pat = patternMeet(ipat, tpat)
        ety = Dyn

        if tyinstance(ity, List):
            ety = ity.type
        elif tyinstance(ity, Tuple):
            ety = tyjoin(*ity.elements)
        elif tyinstance(ity, Dict):
            ety = ity.keys

        assignments = [(target, ety)]
        new_assignments = []
        while assignments:
            k, v = assignments[0]
            del assignments[0]
            if isinstance(k, ast.Name):
                new_assignments.append((Var(k.id),v))
            elif isinstance(k, ast.Tuple) or isinstance(k, ast.List):
                if tyinstance(v, Tuple):
                    assignments += (list(zip(k.elts, v.elements)))
                elif tyinstance(v, Iterable) or tyinstance(v, List):
                    assignments += ([(e, v.type) for e in k.elts])
                elif tyinstance(v, Dict):
                    assignments += (list(zip(k.elts, v.keys)))
                else: assignments += ([(e, Dyn) for e in k.elts])
        #NOTE: Below, the Iterable(tty) was commented out and replaced by Dyn. I'm adding it back
        iter_target = Iterable(tty) #Dyn #Iterable(tty)
        res = cast(env, misc.cls, iter, ity, iter_target, errmsg('ITER_ERROR', misc.filename, lineno, iter_target), misc=misc)
        return ((ast.comprehension(target=target, iter=res, 
                                   ifs=ifs), new_assignments), tty, pat)

    # Control flow stuff
    def visitYield(self, n, env, misc, functionEnv):
        ((value, _), vty, vpat) = self.dispatch(n.value, env, misc, functionEnv) if n.value else ((None, Void), Void, 2)
        return ((ast.Yield(value=value, lineno=n.lineno), Dyn), Dyn, vpat)

    def visitYieldFrom(self, n, env, misc, functionEnv):
        ((value, _), vty, vpat) = self.dispatch(n.value, env, misc, functionEnv)
        return ((ast.YieldFrom(value=value, lineno=n.lineno), Dyn), vty, vpat)

    def visitIfExp(self, n, env, misc, functionEnv):
        ((test, _), tty, tpat) = self.dispatch(n.test, env, misc, functionEnv)
        ((body, bty), bty, bpat) = self.dispatch(n.body, env, misc, functionEnv)
        ((orelse, ety), ety, epat)  = self.dispatch(n.orelse, env, misc, functionEnv)
        cF = Dyn if bty != ety else bty
        cPat = patternMeet(bpat, epat)
        pat = patternMeet(cPat, tpat)
        #used to return cF as a variation
        return ((ast.IfExp(test=test, body=body, orelse=orelse, lineno=n.lineno), cF), cF, pat)

    # Function stuff
    def visitCall(self, n, env, misc, functionEnv):
        global localEnv
        (functionEnv, local_env) = functionEnv
        selV = selectionVisitors.SelVisitor()
        
        if reflection.is_reflective(n):
            return (reflection.reflect(n, env, misc, self), Dyn)

        # Python3.5 gets rid of .kwargs and .starargs, instead has a Starred value in the args 
        # and a keyword arg with no key (i.e. kwarg in n.keywords; kwarg = keyword(arg=None, value=[w/e]))
        def has_starargs(n):
            if flags.PY3_VERSION >= 5:
                return (any(isinstance(e, ast.Starred) for e in n.args))
            else: return (n.starargs is not None)
        def has_kwargs(n):
            if flags.PY3_VERSION >= 5:
                return (any(e.arg is None for e in n.keywords))
            else: return (n.kwargs is not None)

        project_needed = [False] # Python2 doesn't have nonlocal
        class BadCall(Exception):
            def __init__(self, msg):
                self.msg = msg
        #NOTE: probably need to change result of cast_args to return all costs
        def cast_args(argdata, fun, funty):
            vs, ss = zip(*argdata) if argdata else ([], [])
            vs = list(vs)
            ss = list(ss)
            if tyinstance(funty, Dyn):
                if n.keywords or has_kwargs(n) or has_starargs(n):
                    targparams = DynParameters
                else: targparams = AnonymousParameters(ss)
                res = cast(env, misc.cls, fun, Dyn, Function(targparams, Dyn),
                                errmsg('FUNC_ERROR', misc.filename, n, Function(targparams, Dyn)), misc=misc)
                return ((vs, res, Dyn), Dyn, 2)
            elif tyinstance(funty, Function):
                argcasts = funty.froms.lenmatch(argdata)
                # Prototype implementation for type variables

                if argcasts != None:
                    substs = []
                    casts = []
                    for (v, s), t in argcasts:
                        if isinstance(t, TypeVariable):
                            substs.append((t.name, s))
                            casts.append(v)
                        else:
                            loopres = cast(env, misc.cls, v, s, t, errmsg('ARG_ERROR', misc.filename, n, t), misc=misc)
                            casts.append(loopres)
                    to = funty.to
                    for var,rep in substs:
                        # Still need to merge in case of multiple approaches
                        to = to.substitute(var, rep, False)
                    return ((casts, fun, to), to, 2)

                    # return ([cast(env, misc.cls, v, s, t, errmsg('ARG_ERROR', misc.filename, n, t)) for \
                    #             (v, s), t in argcasts],
                    #         fun, funty.to)
                else:
                    #return (([], fun, funty.to), 0, funty.to, 2, {})
                    raise BadCall(errmsg('BAD_ARG_COUNT', misc.filename, n, funty.froms.len(), len(argdata)))
            elif tyinstance(funty, Class):
                project_needed[0] = True
                if '__init__' in funty.members:
                    inst = funty.instance()
                    funty = funty.member_type('__init__')
                    if tyinstance(funty, Function):
                        funty = funty.bind()
                        funty.to = inst
                else:
                    funty = Function(DynParameters, funty.instance())
                return cast_args(argdata, fun, funty)
            elif tyinstance(funty, Object):
                if '__call__' in funty.members:
                    funty = funty.member_type('__call__')
                    return cast_args(argdata, fun, funty)
                else:
                    mfunty = Function(DynParameters, Dyn)
                    res = cast(env, misc.cls, fun, funty, Record({'__call__': mfunty}), 
                                                   errmsg('OBJCALL_ERROR', misc.filename, n), misc=misc)
                    ret = cast_args(argdata, res,
                                     mfunty)
                    return ret, Dyn, 2
            elif tyinstance(funty, Choice):
                retL, tyL, patL = cast_args(argdata, fun, selL(funty.name, funty))
                retR, tyR, patR = cast_args(argdata, fun, selR(funty.name, funty))
                ty = Choice(funty.name, tyL, tyR, funty.kind)
                pat = Choice(funty.name, patL, patR, funty.kind)
                return retL, ty, pat
            else: raise BadCall(errmsg('BAD_CALL', misc.filename, n, funty))
            #end of cast_args definition

        #def functionEnvHelper(arg):
        #    if isinstance(arg, ast.Attribute):
        #        if (Var(arg.attr) in functionEnv):
        #            return functionEnv[Var(arg.attr)]
        #    if isinstance(arg, ast.Name):
        #        if (Var(arg.id) in functionEnv):
        #            return functionEnv[Var(arg.id)]
        #    return 0
        
        #NOTE: self.dispatch calls will eventually return costs     
        ((func, ty), fty, fpat) = self.dispatch(n.func, env, misc, (functionEnv, local_env))        
            
        
        
        if tyinstance(fty, InferBottom):
            return ((n, Dyn), Dyn, 0)

        args_new = [self.dispatch(x, env, misc, (functionEnv, local_env)) for x in n.args]
        argdata = [x[0] for x in args_new]
        #arg_tys = [x[1] for x in argdata] #types of arguments?
        arg_tys = [x[1] for x in args_new] #types of arguments?
        arg_pats = [x[2] for x in args_new]
        arg_pat = reduce(patternMeet, arg_pats, 2)        

    
        
        #see if a variable is being used in the function spot
        if getattr(func, "id", None) != None:
            #uncomment lines below to debug a specific function call
            dom, cod, pat, sub = liftToFunc(fty)
            oty = ty #the original type, not lifted
            ty = Function(dom, cod)
            fpat = patternMeet(fpat, pat)
            patt = patternMeet(arg_pat, fpat)
            unifierTy = Function(AnonymousParameters(arg_tys), fresh_tvar())
            pat = patternMeet(patt, pat)        
            #if (func.id == "bench_FFT"):
            #    pdb.set_trace()
            #if (sub != {}):
            #    updateEnv(env, sub)
            argdata = list(map(lambda el: (el[0], el[1]), argdata))
            retty = ty.to

            #set these variables to obtain normal cast insertion behavior if variations aren't present
            fun_ty = tvars_to_dyn(instantiate(functionEnv[Var(func.id)])) if (Var(func.id) in functionEnv) else fty
            newVar = ast.Name(id=func.id, ctx=ast.Load(), lineno=n.lineno)
            new_ty = fun_ty
            new_arg_expressions = argdata            
            val_tup, nty, npat = cast_args(new_arg_expressions, newVar, new_ty)
        else:
            #we have a method call or some other expression returning a function
            #pdb.set_trace()
            pat = patternMeet(arg_pat, fpat)
            if hasattr(ty, 'froms') and pinstance(ty.froms, NamedParameters) and len(ty.froms.parameters) > 0 and ty.froms.parameters[0][0] == 'self':
                    ty.froms.parameters = list(ty.froms.parameters[1:])
            argdata = list(map(lambda el: (el[0], el[1]), argdata))            
            val_tup, retty, npat = cast_args(argdata, func, ty) #used to have oty as last argument
            #use original opaque type for method call
            #opaqueName = fresh_choice_name()
            #retty = oty
        
        
        
        #args = argdata
        try:            
            (args, func, _) = val_tup
        except BadCall as e:
            if flags.REJECT_WEIRD_CALLS or not (n.keywords or has_kwargs(n) or has_starargs(n)):
                return ((error(e.msg, lineno=n.lineno), Dyn), oty, pat)
            else:
                logging.warn('Function calls with keywords, starargs, and kwargs are not typechecked. Using them may induce a type error in file %s (line %d)' % (misc.filename, n.lineno), 0)
                args = n.args
                retty = Dyn
            
        call = ast_trans.Call(func=func, args=args, keywords=n.keywords,
                              starargs=getattr(n, 'starargs', None),
                              kwargs=getattr(n, 'kwargs', None), lineno=n.lineno)
        #NOTE: erase the line below when check has been instrumented with casts and change assignment to use cost2
        if project_needed[0]:
            call = cast(env, misc.cls, call, Dyn, retty, errmsg('BAD_OBJECT_INJECTION', misc.filename, n, retty, ty), misc=misc)
        else: call = check(call, retty, errmsg('RETURN_CHECK', misc.filename, n, retty))
        return ((call, retty), retty, pat)

    def visitLambda(self, n, env, misc, functionEnv):
        ((args, argnames, specials), aty, apat) = self.dispatch(n.args, env, DynParameters, misc, n.lineno, functionEnv)
        #params = [rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())] * len(argnames)
        params = [Dyn] * len(argnames)
        env = env.copy()
        env.update(dict(list(zip(argnames, params))))
        env.update(dict(specials))
        ((body, rty), bty, bpat) = self.dispatch(n.body, env, misc, functionEnv)
        if n.args.vararg:
            ffrom = DynParameters
        elif n.args.kwarg:
            ffrom = DynParameters
        elif flags.PY_VERSION == 3 and n.args.kwonlyargs:
            ffrom = DynParameters
        elif n.args.defaults:
            ffrom = DynParameters
        else: ffrom = NamedParameters(list(zip(argnames, params)))
        
        ty = Function(ffrom, rty) if flags.TYPED_LAMBDAS else Dyn
        return ((ast.Lambda(args=args, body=body, lineno=n.lineno), ty), ty, bpat)

    # Variable stuff
    def visitName(self, n, env, misc, functionEnv):
        global localEnv
        #NOTE: The functionEnv parameter also contains the local variable environments for functions
        #pdb.set_trace()
        (functionEnv, local_env) = functionEnv
        if isinstance(n.ctx, ast.Param): # Compatibility with 2.7
            return (n.id, Dyn, 2)
        id = n.id if n.id not in rtypes.TYPES else n.id + '_'

        astExp = ast.Name(id=id, ctx=n.ctx, lineno=n.lineno)
        try:
            #ty = env[Var(n.id)]
            ty = tvars_to_dyn(localEnv[Var(n.id)]) if Var(n.id) in localEnv  else tvars_to_dyn(functionEnv[Var(n.id)])  if Var(n.id) in functionEnv else tvars_to_dyn(local_env[Var(n.id)]) if (Var(n.id) in local_env and not isinstance(local_env[Var(n.id)], dict)) else tvars_to_dyn(env[Var(n.id)])
            if Var(n.id) in functionEnv:
                ty = instantiate(functionEnv[Var(n.id)])
            if isinstance(ty, TypeScheme):
                ty = instantiate(ty)
            
                #astExp = makeChoice(vty.name, ast.Name(id=id+"_slow", ctx=n.ctx, lineno=n.lineno), ast.Name(id=id+"_fast", ctx=n.ctx, lineno=n.lineno))
                #astExp = ast.Name(id=id+"_fast", ctx = n.ctx, lineno=n.lineno)
            
            if isinstance(n.ctx, ast.Del) and not tyinstance(ty, Dyn) and flags.REJECT_TYPED_DELETES:
                return ((error(errmsg('TYPED_VAR_DELETE', misc.filename, n, n.id, ty)), Dyn), Dyn, 0)
        except KeyError:
            #pdb.set_trace()
            ty = Dyn

        #pdb.set_trace()
        pat = patEnv[Var(n.id)] if Var(n.id) in patEnv else 2 #used to be True in else but seemed wrong
        

        return ((astExp, ty), ty, pat)

    def visitNameConstant(self, n, env, misc, functionEnv):
        if flags.TYPED_LITERALS:
            if n.value is True or n.value is False:
                return ((n, Bool), Bool, 2)
            elif n.value is None:
                return ((n, Void), Void, 2)
        return ((n, Dyn), Dyn, 2)

    def visitAttribute(self, n, env, misc, functionEnv):
        #pdb.set_trace()
        global original_env
        ((value, vty), val_ty, vpat) = self.dispatch(n.value, env, misc, functionEnv)
        original_type = vty
        if tyinstance(vty, InferBottom):
            vpat = 0
        
        if isinstance(vty, Structural) and isinstance(n.ctx, ast.Load):
            vty = vty.structure()
        assert vty is not None, n.value
        #if (isinstance(vty, TypeVariable) and vty.opaque):
            #return ((n, Dyn), costObj, Dyn, 2, vsub)
        #    vpat = patternMeet(vpat,  2)
        
        ty = Dyn        
        #NOTE: in case the inferred type causes a cast when a method is called
        #if n.value.id == "ps":
        #    pdb.set_trace()
        if original_type.inferred and isinstance(n.value, ast.Name):            
            old_ty = original_env[Var(n.value.id)] if Var(n.value.id) in original_env else vty
            if isinstance(old_ty, Structural) and isinstance(n.ctx, ast.Load):
                old_ty = old_ty.structure()
            #old_member_ty = old_ty.member_type(n.attr)            
        if tyinstance(vty, Self):
            try:
                ty = misc.cls.instance().member_type(n.attr)
            except KeyError:
                vpat = 0
            #    if flags.CHECK_ACCESS and not flags.CLOSED_CLASSES and not isinstance(n.ctx, ast.Store):
            #        value, cost = cast(env, misc.cls, value, misc.cls.instance(), Object(misc.cls.name, {n.attr: Dyn}), errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
            #        total_cost += (cost)
            #    ty = Dyn
            #if isinstance(value, ast.Name) and value.id == misc.receiver.id:
            #    if flags.SEMANTICS == 'MONO' and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del) and \
            #            not tyinstance(ty, Dyn):
            #        ans = ast.Call(func=ast.Name(id='retic_getattr_'+('static' if ty.static() else 'dynamic'), 
            #                                     ctx=ast.Load(), lineno=n.lineno),
            #                       args=[value, ast.Str(s=n.attr), ty.to_ast()],
            #                       keywords=[], starargs=None, kwargs=None, lineno=n.lineno)
            #        costObj = total_cost
            #        return ((ans, ty), costObj, ty, True, {})
            #    else:
            #           costObj = total_cost
            #           return ((ast.Attribute(value=value, attr=n.attr, lineno=n.lineno, ctx=n.ctx), ty), costObj, ty, True, {})
            #if isinstance(n.ctx, ast.Store):
            #    costObj = total_cost
            #    return ((ast.Attribute(value=value, attr=n.attr, lineno=n.lineno, ctx=n.ctx), ty), costObj, Dyn, True, {})
            #return ((ast.Call(func=ast.Name(id='retic_bindmethod', ctx=ast.Load()),
            #                args=[ast.Attribute(value=misc.receiver, attr='__class__', ctx=ast.Load()),
            #                      value, ast.Str(s=n.attr)], keywords=[], starargs=None, kwargs=None, 
            #                lineno=n.lineno), \
            #                      ty), total_cost)
        elif tyinstance(vty, Object) or tyinstance(vty, Class):
            try:                
                ty = vty.member_type(n.attr)
                if original_type.inferred and tyinstance(old_ty, Dyn):
                    if flags.CHECK_ACCESS and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del):
                        ty = Dyn
                        value = cast(env, misc.cls, value, vty, Record({n.attr: Dyn}), 
                             errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
                        #        total_cost += (cost3)
                    else:                
                        value = cast(env, misc.cls, value, vty, Record({}), 
                             errmsg('NON_OBJECT_' + ('WRITE' if isinstance(n.ctx, ast.Store) \
                                                         else 'DEL'), misc.filename, n, n.attr), misc=misc)
                elif original_type.inferred and (tyinstance(old_ty, Object) or tyinstance(old_ty, Class)):
                    old_member_type = old_ty.member_type(n.attr)
                    if less_precise(old_member_type, ty):
                        ty = old_member_type #We don't want to add casts that can fail in inferred code
                        
        #        if isinstance(n.ctx, ast.Del):
        #            return ((error(errmsg('TYPED_ATTR_DELETE', misc.filename, n, n.attr, ty), lineno=n.lineno), Dyn), total_cost, Dyn, False, {})
            except KeyError:
                vpat = 0
        #        if flags.CHECK_ACCESS and not flags.CLOSED_CLASSES and not isinstance(n.ctx, ast.Store):
        #            value,cost2 = cast(env, misc.cls, value, vty, vty.__class__('', {n.attr: Dyn}), 
        #                         errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
        #            total_cost += (cost2)
        #        ty = Dyn
        elif tyinstance(vty, Dyn):
            if flags.CHECK_ACCESS and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del):
                value = cast(env, misc.cls, value, vty, Record({n.attr: Dyn}), 
                             errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
        #        total_cost += (cost3)
            else:                
                value = cast(env, misc.cls, value, vty, Record({}), 
                             errmsg('NON_OBJECT_' + ('WRITE' if isinstance(n.ctx, ast.Store) \
                                                         else 'DEL'), misc.filename, n, n.attr), misc=misc)
        #        total_cost += (cost4)
        #    ty = Dyn
        #else:
        #    kind = 'WRITE' if isinstance(n.ctx, ast.Store) else ('DEL' if isinstance(n.ctx, ast.Del) else 'READ')
        #    costObj = total_cost
        #    return ((error(errmsg('NON_OBJECT_' + kind, misc.filename, n, n.attr) % static_val(vty), lineno=n.lineno), Dyn), costObj, Dyn, False, {})

        #if flags.SEMANTICS == 'MONO' and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del) and \
        #        not tyinstance(ty, Dyn):
        #    ans = ast.Call(func=ast.Name(id='retic_getattr_'+('static' if ty.static() else 'dynamic'), 
        #                                 ctx=ast.Load(), lineno=n.lineno),
        #                   args=[value, ast.Str(s=n.attr), ty.to_ast()],
        #                keywords=[], starargs=None, kwargs=None, lineno=n.lineno)
        #    costObj = total_cost
        #    return ((ans, ty), costObj, ty, vpat, vsub)        
        pat = 2

        pat = patternMeet(pat, vpat)
        ans = ast.Attribute(value=value, attr=n.attr, ctx=n.ctx, lineno=n.lineno)
        #if not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del):
        #    ans = check(ans, ty, errmsg('ACCESS_CHECK', misc.filename, n, n.attr, ty), ulval=value)
        #costObj = total_cost
        
        #pdb.set_trace()
        return ((ans, ty), ty, pat)
        

    def visitSubscript(self, n, env, misc, functionEnv):
        ((value, vty), val_ty, vpat) = self.dispatch(n.value, env, misc, functionEnv)
        if tyinstance(vty, InferBottom):
            return ((n, Dyn), val_ty, vpat)
        #pdb.set_trace()
        ((slice, ty), sty, spat) = self.dispatch(n.slice, env, vty, misc, n.lineno, functionEnv)
        ans = ast.Subscript(value=value, slice=slice, ctx=n.ctx, lineno=n.lineno)
        if not isinstance(n.ctx, ast.Store):
            ans = check(ans, ty, errmsg('SUBSCRIPT_CHECK', misc.filename, n, ty), ulval=value)
            #NOTE: probably need to add cost for check

        
        pat = patternMeet(vpat, spat)
        return ((ans, ty),  ty, pat)

    def visitIndex(self, n, env, extty, misc, lineno, functionEnv):
        ((value, vty), val_ty, vpat) = self.dispatch(n.value, env, misc, functionEnv)
        err = errmsg('BAD_INDEX', misc.filename, lineno, extty, Int)
        pat = vpat
        #finalTy = vty.type if tyinstance(vty, List) else Dyn
        pat = patternMeet(pat, vpat)
        def collection_type(extty):
            if tyinstance(extty, List):
                value_new = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                return value_new, extty.type
            elif tyinstance(extty, Bytes):
                value_new = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                return value_new, Float #Int
            elif tyinstance(extty, String):
                value_new = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                return value_new, String
            elif tyinstance(extty, Tuple):
                value_new = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                return value_new, Dyn
            elif tyinstance(extty, Dict):
                value_new = cast(env, misc.cls, value, vty, extty.keys, errmsg('BAD_INDEX', misc.filename, lineno, extty, extty.keys), misc=misc)
                return value_new, extty.values
            elif tyinstance(extty, Object):
                # Expand
                return value, Dyn
            elif tyinstance(extty, Class):
                # Expand
                return value, Dyn
            elif tyinstance(extty, Dyn):
                return value, Dyn
            elif tyinstance(extty, Choice):
                _, tyL = collection_type(selL(extty.name, extty)) #ignore creating variational values because it would break things
                _, tyR = collection_type(selR(extty.name, extty))
                return value, Choice(extty.name, tyL, tyR)
            else:
                return value, Dyn
        value, ty = collection_type(extty)
        #return error(errmsg('NON_INDEXABLE', misc.filename, lineno, extty), lineno=lineno), Dyn
        # More cases...?
        #need to return extty        
        return ((ast.Index(value=value), ty), ty, pat)

    def visitSlice(self, n, env, extty, misc, lineno, functionEnv):
        err = errmsg('BAD_INDEX', misc.filename, lineno, extty, Int)

        ((lower, lty), loty, lpat) = self.dispatch(n.lower, env, misc, functionEnv) if n.lower else ((None, Void), Dyn, 2)
        ((upper, uty), upty, upat) = self.dispatch(n.upper, env, misc, functionEnv) if n.upper else ((None, Void), Dyn, 2)
        ((step, sty), stty, spat) = self.dispatch(n.step, env, misc, functionEnv) if n.step else ((None, Void), Dyn, 2)
        pat1 = patternMeet(lpat, upat)
        pat = patternMeet(spat, pat1)
        pat = patternMeet(spat, pat1)
        #pat = patternMeet(lopat, pat)
        #pat = patternMeet(uppat, pat)
        #pat = patternMeet(stpat, pat)

        ty = extty.type if tyinstance(extty, List) else Dyn
        
        return ((ast.Slice(lower=lower, upper=upper, step=step), ty), ty, pat)

    def visitExtSlice(self, n, env, extty, misc, lineno, functionEnv):
        dims = [(dim, ty, pat) for ((dim,_), ty, pat) in [self.dispatch(dim2, env, extty, misc, lineno, functionEnv) for dim2 in n.dims]]
        dims = [x[0] for x in dims]
        pats = [x[1] for x in dims]
        pat = reduce(lambda a, b: patternMeet(a, b), pats, 2)
        #NOTE: finish slice
        return ((ast.ExtSlice(dims=dims), Dyn), Dyn, pat)

    def visitEllipsis(self, n, env, functionEnv, *args): 
        #Yes, this looks stupid, but Ellipses are different kinds of things in Python 2 and 3 and if we ever
        #support them meaningfully this distinction witll be crucial
        if flags.PY_VERSION == 2: 
            extty = args[0]
            return ((n, Dyn), Dyn, 2)
        elif flags.PY_VERSION == 3:
            return ((n, Dyn), Dyn, 2)

    def visitStarred(self, n, env, misc, functionEnv):
        ((value, _), ty, pat) = self.dispatch(n.value, env, misc, functionEnv)
        return ((ast.Starred(value=value, ctx=n.ctx, lineno=n.lineno), Dyn), ty, pat)

    # Literal stuff
    def visitNum(self, n, env, misc, functionEnv):
        ty = Dyn
        v = n.n
        if type(v) == int or (flags.PY_VERSION == 2 and type(v) == long):
            ty = Int
            #ty = Float
        elif type(v) == float:
            ty = Float
        elif type(v) == complex:
            ty = Complex
        return ((n, ty if flags.TYPED_LITERALS else Dyn), ty, 2)

    def visitStr(self, n, env, misc, functionEnv):
        return ((n, String if flags.TYPED_LITERALS else Dyn), String, 2)

    def visitBytes(self, n, env, misc, functionEnv):
        return ((n, Bytes if flags.TYPED_LITERALS else Dyn), Bytes, 2)
