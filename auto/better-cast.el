(TeX-add-style-hook
 "better-cast"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("acmart" "acmsmall" "review" "anonymous")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("subcaption" "labelformat=simple") ("algorithm2e" "ruled") ("vlc" "noshare") ("enumitem" "inline") ("inputenc" "utf8") ("babel" "english")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "errd"
    "macros"
    "acmart"
    "acmart10"
    "graphicx"
    "caption"
    "subcaption"
    "natbib"
    "algorithm2e"
    "hyperref"
    "cc"
    "lambda"
    "vlc"
    "proof"
    "pdfpages"
    "multirow"
    "xargs"
    "mathpartir"
    "mathtools"
    "xcolor"
    "calc"
    "paralist"
    "tikz"
    "booktabs"
    "pifont"
    "enumitem"
    "inputenc"
    "babel"
    "scalerel"
    "amssymb"
    "amsmath"
    "stmaryrd"
    "wrapfig")
   (TeX-add-symbols
    '("gbkgray" 1)
    '("gbkg" 1)
    '("tightbox" 1)
    "cL"
    "cross"
    "msquare")
   (LaTeX-add-labels
    "sec:intro"
    "code:callescape:unexpected"
    "code:callescape:expected"
    "code:ftwobody"
    "code:callfone"
    "code:callftwo:correct"
    "code:ftwoerror"
    "code:callftwoerror"
    "code:slow:begin"
    "code:castonz"
    "code:slow:end"
    "fig:intro:apps"
    "sec:intro:problem"
    "sec:intro:solution"
    "code:callfone:translated"
    "code:callftwo:correct:translated"
    "code:callftwo:error:translated"
    "fig:intro:fast"
    "sec:informal"
    "sec:informal:find"
    "code:wdf:then"
    "code:wdf:else"
    "code:adjust:ftwo"
    "code:adjust:fone"
    "code:adjust:notB"
    "fig:informal:source"
    "fig:informal:types"
    "sec:informal:disc"
    "code:adjust:ftwo:translated"
    "code:adjust:fone:translated"
    "code:adjust:notB:translated"
    "fig:informal:target"
    "sec:background"
    "sec:background:vt"
    "code:vpaf"
    "code:vpas"
    "code:vpab"
    "fig:bg:pattern"
    "sec:finding"
    "sec:finding:typing"
    "fig:finding:syntaxandrules"
    "sec:finding:finding"
    "thm:notboth"
    "sec:finding:types"
    "sec:ci"
    "sec:ci:overview"
    "fig:ci:cast-insertion"
    "sec:ci:basic"
    "sec:ci:app"
    "code:escape:call"
    "code:call:fthree"
    "code:ffour:begin"
    "code:ffour:end"
    "fig:ci:tricky-programs"
    "sec:ci:eliminating"
    "sec:eval"
    "sec:evaluation:experiment"
    "fig:eval:benchmarks"
    "sec:eval:configs"
    "fig:eval:histogram"
    "fig:eval:ratios"
    "sec:eval:untyped"
    "sec:rw"
    "sec:conclusion"
    "sec:app:rightmost")
   (LaTeX-add-bibliographies
    "paper")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("ceil" "")
    '("floor" "")))
 :latex)

