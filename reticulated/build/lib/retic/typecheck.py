from __future__ import print_function
import ast
from .vis import Visitor
from .gatherers import FallOffVisitor, WILL_RETURN
from .inference import InferVisitor
from .typing import *
from .relations import *
from .exc import StaticTypeError, UnimplementedException
from .errors import errmsg, static_val
from .astor.misc import get_binop
from . import typing, utils, flags, rtypes, reflection, annotation_removal, logging, runtime, ast_trans, selectionVisitors
from functools import reduce
#from sympy import *
import sympy 
import ast
import collections
import pdb

firstLetter = (ord('A') - 1)
patEnv = {}
functionEnv = collections.OrderedDict()
localEnv = {}
class_name = None
number_of_casts = 0

environment_choices = []
environment_tvars = []

def sub_inferred(sub):
    for dom, cod in sub.items():
        cod.inferred = True
        
def applyEnv(env, sub):
    if (sub == {}):
        return env
    newEnv = env.copy()
    for f in newEnv:
        t = newEnv[f]
        newTy = applySub(t, sub)
        newEnv[f] = newTy
    return newEnv

def updateEnv(env, sub):
    if (sub == {}):
        return env
    for f in env:
        t = env[f]
        newTy = applySub(t, sub)
        #newTy.inferred = True
        env[f] = newTy
    return env

def inc_char():
    global firstLetter 
    firstLetter += 1
    return chr(firstLetter)


def fixup(n, lineno=None, col_offset=None):
    if isinstance(n, list) or isinstance(n, tuple):
        return [fixup(e, lineno if lineno else e.lineno) for e in n]
    else:
        if lineno != None:
            n.lineno = lineno
        if col_offset != None:
            n.col_offset = None
        return ast.fix_missing_locations(n)

##Cast insertion functions##
#Normal casts
def cast(env, ctx, val, src, trg, msg, cast_function='retic_cast', misc=None):
    global number_of_casts
    number_of_casts += 1
    if flags.SEMANTICS == 'MGDTRANS':
        from . import mgd_typecheck
        return mgd_typecheck.cast(env, ctx, val, src, trg, msg, misc=misc)

    if flags.SEMI_DRY:
        return val
    if flags.SQUELCH_MESSAGES:
        msg = ''
    assert hasattr(val, 'lineno'), ast.dump(val)
    lineno = str(val.lineno)

    if hasTvar(src):
        src = relations.tvars_to_dyn(src)
    if hasTvar(trg):
        trg = relations.tvars_to_dyn(trg)
    merged = merge(src, trg)
    #NOTE: Removing top_free and subcompat checks
    #pdb.set_trace()
    #if not trg.top_free() or not subcompat(src, trg, env, ctx):
    #    return error(msg % static_val(src), lineno)
    if src == merged:
        return val
    if not flags.OPTIMIZED_INSERTION:
        msg = '\n' + msg
        logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
        return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                              args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                              keywords=[], starargs=None, kwargs=None), val.lineno)
    else:
        msg = '\n' + msg
        if flags.SEMANTICS == 'MONO':
            logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
            return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                  args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                  keywords=[], starargs=None, kwargs=None), val.lineno)
        elif flags.SEMANTICS == 'TRANS':
            if not tyinstance(trg, Dyn):
                args = [val]
                cast_function = 'check_type_'
                if tyinstance(trg, Int):
                    cast_function += 'int'
                elif tyinstance(trg, Float):
                    cast_function += 'float'
                elif tyinstance(trg, String):
                    cast_function += 'string'
                elif tyinstance(trg, List):
                    cast_function += 'list'
                elif tyinstance(trg, Complex):
                    cast_function += 'complex'
                elif tyinstance(trg, Tuple):
                    cast_function += 'tuple'
                    args += [ast.Num(n=len(trg.elements))]
                elif tyinstance(trg, Dict):
                    cast_function += 'dict'
                elif tyinstance(trg, Bool):
                    cast_function += 'bool'
                elif tyinstance(trg, Set):
                    cast_function += 'set'
                elif tyinstance(trg, Function):
                    cast_function += 'function'
                elif tyinstance(trg, Void):
                    cast_function += 'void'
                elif tyinstance(trg, Class):
                    cast_function += 'class'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                elif tyinstance(trg, Object):                    
                    if len(trg.members) == 0:
                                                return (val) #NOTE: temporary work around
                    cast_function += 'object'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                else:
                    logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
                    #NOTE: need to return a cost for transient return 0 in meantime
                    return fixup(ast.Call(func=ast.Name(id='retic_cast', ctx=ast.Load()),
                                          args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                           keywords=[], starargs=None, kwargs=None), val.lineno)
                #NOTE: need to return a cost for transient return 0 in meantime
                return (fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                       args=args, keywords=[], starargs=None, kwargs=None)))
            else: return val #NOTE: need to return a cost for transient return 0 in meantime
        elif flags.SEMANTICS == 'MGDTRANS':
            raise Exception('Should not be invoking this version of cast()')
        elif flags.SEMANTICS == 'GUARDED':            
            #NOTE: import guarded to call retic_cast directly
            from . import guarded
            #print("Val = %s" % val)
            #print("Src = %s" % src)
            #print("Trg = %s" % merged)
                        #print("Cost = %s" % cost)
            logging.warn('Inserting cast at line %s: %s => %s' % (lineno, src, trg), 2)
            #NOTE inital cost returned by cast in guarded
            #NOTE: Removing calls that actually insert casts!
            #return val, costObj
            return (fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                  args=[val, src.to_ast(), merged.to_ast(), ast.Str(s=msg)],
                                   keywords=[], starargs=None, kwargs=None), val.lineno))
        elif flags.SEMANTICS == 'NOOP':
            return val
        else: raise UnimplementedException('Efficient insertion unimplemented for this semantics')

# Casting with unknown source type, as in cast-as-assertion 
# function return values at call site
def check(val, trg, msg, check_function='retic_check', lineno=None, ulval=None):
    msg = '\n' + msg
    if flags.SEMI_DRY:
        return val
    if flags.SQUELCH_MESSAGES:
        msg = ''
    assert hasattr(val, 'lineno')
    lineno = str(val.lineno)

    if not flags.OPTIMIZED_INSERTION:
        logging.warn('Inserting check at line %s: %s' % (lineno, trg), 2)
        return fixup(ast.Call(func=ast.Name(id=check_function, ctx=ast.Load()),
                              args=[val, trg.to_ast(), ast.Str(s=msg)],
                              keywords=[], starargs=None, kwargs=None), val.lineno)
    else:
        if flags.SEMANTICS == 'TRANS':
            if not tyinstance(trg, Dyn):
                args = [val]
                cast_function = 'check_type_'
                if tyinstance(trg, Int):
                    cast_function += 'int'
                elif tyinstance(trg, Float):
                    cast_function += 'float'
                elif tyinstance(trg, String):
                    cast_function += 'string'
                elif tyinstance(trg, List):
                    cast_function += 'list'
                elif tyinstance(trg, Complex):
                    cast_function += 'complex'
                elif tyinstance(trg, Tuple):
                    cast_function += 'tuple'
                    args += [ast.Num(n=len(trg.elements))]
                elif tyinstance(trg, Dict):
                    cast_function += 'dict'
                elif tyinstance(trg, Bool):
                    cast_function += 'bool'
                elif tyinstance(trg, Void):
                    cast_function += 'void'
                elif tyinstance(trg, Set):
                    cast_function += 'set'
                elif tyinstance(trg, Function):
                    cast_function += 'function'
                elif tyinstance(trg, Class):
                    cast_function += 'class'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                elif tyinstance(trg, Object):
                    if len(trg.members) == 0:
                        return val
                    cast_function += 'object'
                    args += [ast.List(elts=[ast.Str(s=x) for x in trg.members], ctx=ast.Load())]
                else:
                    logging.warn('Inserting check at line %s: %s' % (lineno, trg), 2)
                    return fixup(ast.Call(func=ast.Name(id=check_function, ctx=ast.Load()),
                                          args=[val, trg.to_ast(), ast.Str(s=msg)],
                                          keywords=[], starargs=None, kwargs=None), val.lineno)

                return fixup(ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                                      args=args, keywords=[], starargs=None, kwargs=None))
                # return fixup(ast.IfExp(test=ast.Call(func=ast.Name(id=cast_function, ctx=ast.Load()),
                #                                      args=args, keywords=[], starargs=None, kwargs=None),
                #                        body=val,
                #                        orelse=ast.Call(func=ast.Name(id='retic_error', ctx=ast.Load()),
                #                                        args=[ast.Str(s=msg)], keywords=[], starargs=None,
                #                                        kwargs=None)), val.lineno)
            else: return val
        elif flags.SEMANTICS == 'MGDTRANS':
            raise Exception('Should not be invoking this version of cast()')
            
        else: return val

# Check, but within an expression statement
def check_stmtlist(val, trg, msg, check_function='retic_check', lineno=None):
    if flags.SEMI_DRY:
        return []
    assert hasattr(val, 'lineno'), ast.dump(val)
    chkval = check(val, trg, msg, check_function, val.lineno)
    if not flags.OPTIMIZED_INSERTION:
        return [ast.Expr(value=chkval, lineno=val.lineno)]
    else:
        if flags.SEMANTICS not in ['TRANS', 'MGDTRANS'] or chkval == val or tyinstance(trg, Dyn):
            return []
        else: return [ast.Expr(value=chkval, lineno=val.lineno)]

# Insert a call to an error function if we've turned off static errors
def error(msg, lineno, error_function='retic_error'):
    if flags.STATIC_ERRORS or flags.SEMI_DRY:
        raise StaticTypeError(msg)
    else:
        logging.warn('Static error detected at line %d' % lineno, 0)
        return fixup(ast.Call(func=ast.Name(id=error_function, ctx=ast.Load()),
                              args=[ast.Str(s=msg+' (statically detected)')], keywords=[], starargs=None,
                              kwargs=None), lineno)

# Error, but within an expression statement
def error_stmt(msg, lineno, error_function='retic_error'):
    if flags.STATIC_ERRORS or flags.SEMI_DRY:
        raise StaticTypeError(msg)
    else:
        return [ast.Expr(value=error(msg, lineno, error_function), lineno=lineno)]

class Typechecker(Visitor):
    falloffvisitor = FallOffVisitor()

    def dispatch_debug(self, tree, *args):
        ret = super().dispatch(tree, *args)
        print('results of %s:' % tree.__class__.__name__)
        if isinstance(ret, tuple):
            if isinstance(ret[0], ast.AST):
                print(ast.dump(ret[0]))
            if isinstance(ret[1], PyType):
                print(ret[1])
        if isinstance(ret, ast.AST):
            print(ast.dump(ret))
        return ret

    if flags.DEBUG_VISITOR:
        dispatch = dispatch_debug

    def typecheck(self, n, env, misc,  local_env={}):
        global functionEnv
        global localEnv
        localEnv.update(local_env)
        #oldEnv = env
        env = env.copy()
        
        n = fixup(n)
        env.update(typing.initial_environment())
        #addTypesToEnv(env)
        #pdb.set_trace()
        tn = self.preorder(n, env, misc)
        tn0 = tn[0]
        ty = tn[1]
        pat = tn[2]
        sub = tn[3]
        #tn = [x[0] for x in tn]
        tn0 = fixup(tn0)
        #pdb.set_trace()
        #updateEnv(oldEnv, sub)
        if flags.DRY_RUN:
            return n
        
        return (tn0, env), ty, pat, sub, functionEnv

    def visitlist(self, n, env, misc):
        global localEnv
        body = [] 
        types = []
        pats = []
        sub = {}
        pat = 2
        ty = rtypes.Dyn
        #newEnv = env.copy()
        for s in n:
            stmts, ty, pat1, sub1 = self.dispatch(s, env, misc)            
            body += stmts
            if not(isinstance(s, ast.FunctionDef)):
                #pdb.set_trace()                
                pat = patternMeet(pat, pat1)
                if (sub1 != {}):
                    #pdb.set_trace()
                    sub = compose(sub1, sub)
                    #pdb.set_trace()
                    updateEnv(localEnv, sub)
                    # can I remove updating the environment until after a function definition?
                    #updateEnv(env, sub)
                #costs.append(cost)
                #types.append(ty)
                #pats.append(pat)
        return body, ty, pat, sub
        
    def visitModule(self, n, env, misc):
        body,  ty, pat, sub = self.dispatch(n.body, env, misc)
        return ast.Module(body=body), ty, pat, sub

    def default(self, n, *args):
        if isinstance(n, ast.expr):
            return ((n, Dyn), Dyn, 2, {})
        elif isinstance(n, ast.stmt):
            return ([n], Dyn, 2, {})
        else: (n, Dyn, 2, {})

## STATEMENTS ##
    # Import stuff
    def visitImport(self, n, env, misc):
        return ([n], Dyn, 2, {})
            
    def visitImportFrom(self, n, env, misc):
        return ([n], Dyn, 2, {})

    # Function stuff
    def visitFunctionDef(self, n, env, misc): #TODO: check defaults, handle varargs and kwargs
        global patEnv
        global functionEnv
        global localEnv
        global class_name
        global number_of_casts
        global environment_choices
        global environment_tvars
        
        for f in functionEnv:
            if isinstance(f, Var):
                env[f] = functionEnv[f]
            else:
                env[Var(f)] = functionEnv[f]
        try:
            nty = env[Var(n.name)] #NOTE: is the function already in the env
        except KeyError as e :
            assert False, ('%s at %s:%d' % (e ,misc.filename, n.lineno))
            
        #NOTE: gives the function type's domain and codomain Dyn unless it's annotated        
        froms = nty.froms if hasattr(nty, 'froms') else DynParameters#[Dyn] * len(argnames)
        #new_params = []
        #if (isinstance(froms, NamedParameters)):
        #    paramList = froms.parameters
        #    for i, j in paramList:
        #        if j == Dyn:
        #            new_params.append((i, rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())))
        #        else:
        #            new_params.append((i, j))
        #froms = NamedParameters(new_params)

        to = nty.to if (hasattr(nty, 'to') and nty.to != Dyn) else Dyn#rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())
        fty = Function(froms, to)
        if not misc.methodscope and not nty.self_free():
            error(errmsg('UNSCOPED_SELF', misc.filename, n), lineno=n.lineno)

        name = n.name if n.name not in rtypes.TYPES else n.name + '_'
        if class_name != None:
            lookup_name = class_name + "_" + name #prepend class name to methods in the env
        else:
            lookup_name = name

        
        #NOTE: VERY IMPORTANT, I've commented out the higher order casts added to each function below. The new version casts from nty to nty
        #res, cost = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), Dyn, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)
        res = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), nty, nty, errmsg('BAD_FUNCTION_INJECTION', misc.filename, n, nty), misc=misc)

        
        
        assign = ast.Assign(targets=[ast.Name(id=name, ctx=ast.Store(), lineno=n.lineno)], 
                            value=res,
                            lineno=n.lineno)
        ((args, argnames, specials), typ, pat, sub) = self.dispatch(n.args, env, froms, misc, n.lineno)
        
        decorator_list = n.decorator_list#[self.dispatch(dec, env, misc)[0] for dec in n.decorator_list if not is_annotation(dec)]
        # Decorators have a restricted syntax that doesn't allow function calls
        env = (misc.extenv if misc.cls else env).copy()
        
        if misc.cls:
            receiver = None if (not misc.methodscope or len(argnames) == 0) else\
                ast.Name(id=argnames[0], ctx=ast.Load())
        else: 
            receiver = None


        argtys = froms.lenmatch([Var(x) for x in argnames])        
        assert(argtys != None)
        initial_locals = dict(argtys + specials)
        localEnv = initial_locals        
        #localEnv.update(initial_locals)
        logging.debug('Function %s typechecker starting in %s' % (n.name, misc.filename), flags.PROC)

        ret_misc, (functionEnv, local_envs), bty, bpat, bsub = misc.static.typecheck(n.body, env, localEnv, typing.Misc(ret=to, cls=misc.cls, receiver=receiver, extenv=misc.extenv, gensymmer=misc.gensymmer, typenames=misc.typenames, extend=misc),  functionName=lookup_name)
        sub = compose(sub, bsub)
        pat = patternMeet(bpat, pat)
        (body, fenv) = ret_misc


        #for some reason the relation between parameters and assignments is only captured here

        arg_data = froms.lenmatch([x for x in argnames])
        newTy = fenv[Var(lookup_name)] if Var(lookup_name) in fenv else Function(NamedParameters(arg_data), rtypes.Choice(relations.fresh_choice_name(), rtypes.Dyn, relations.fresh_tvar())) #NOTE: I'm changing it here from Dyn

        
        newTy = instantiate(newTy) if (isinstance(newTy, TypeScheme)) else newTy
        newSub, newPat, newCod = unify(bty, newTy.to) 

        sub = compose(newSub, sub)
        newTy = applySub(newTy, sub)
        if (Var(lookup_name) in env):
            env.pop(Var(lookup_name))        
        nTy = generalize(newTy, env)
        
        
        #Note: this replaces free type variables with Dyn.
        #nTy = tvars_to_dyn(instantiate(nTy))
        pat = patternMeet(pat, newPat)

        #
        #Start of new code for better cast insertion
        #We begin by picking two types for the function, by using one of the best eliminators for it
        #        
        lty = allLeft(instantiate(nTy))
        normPat = reduceChoices(pat)
        eliminators = validElims(normPat)
        #there was an issue with closures and mutation in expand,
        boundChcs = boundChoices(instantiate(nTy))        
        eliminators = [[]] if not eliminators else eliminators
        elimToUse = expand(eliminators[0], boundChcs)
        rty = apply_decision(elimToUse, instantiate(nTy))

        #We now need to update the local variables for the function definitions
        if Var(lookup_name+"_fast") in local_envs.keys():
            fastEnv = local_envs[Var(lookup_name+"_fast")]
            updateEnv(fastEnv, sub)
            for name_var, ty in fastEnv.items():          
                fastEnv[name_var] = rtypes.selRAll(apply_decision(elimToUse, ty))
            #pdb.set_trace()        
        cty = rtypes.makeChoice(fresh_choice_name(), lty, rty)
        functionEnv[Var(lookup_name)] = generalize(lty, env)
        functionEnv[Var(lookup_name+"_vari")] = generalize(cty, env)
        functionEnv[Var(lookup_name+"_fast")] = generalize(rty, env)        
        env[Var(lookup_name)] = generalize(lty, env)

        
        slowLocals = dict(localEnv)
        #fastLocals = dict(localEnv)
        slowArgTys = lty.froms.lenmatch([Var(x) for x in argnames])        
        #fastArgTys = rty.froms.lenmatch([Var(x) for x in argnames])
        if hasattr(tvars_to_dyn(lty).froms, "parameters"):
            for paramName, paramType in tvars_to_dyn(lty).froms.parameters:
                slowLocals[Var(paramName)] = paramType
                localEnv[Var(paramName)] = paramType
        #do I need to change misc.extenv?
        checked_body = misc.static.typecheck(n.body, env, slowLocals, \
            typing.Misc(ret=to, cls=misc.cls, receiver=receiver, extenv=misc.extenv, gensymmer=misc.gensymmer, typenames=misc.typenames, extend=misc))
        
        slow_ret_misc, (functionEnv, local_envs), slow_bty, slow_bpat, slow_bsub = misc.static.typecheck(n.body, env, slowLocals, \
            typing.Misc(ret=to, cls=misc.cls, receiver=receiver, extenv=misc.extenv, gensymmer=misc.gensymmer, typenames=misc.typenames, extend=misc)) 
        #env[Var(name+"_fast")] = generalize(rty, env)
        #for paramName, paramType in tvars_to_dyn(rty).froms.parameters:
        #    fastLocals[Var(paramName)] = paramType
        #    localEnv[Var(paramName)] = paramType
        #fast_ret_misc, fast_body_cost, fast_bty, fast_bpat, fast_bsub = misc.static.typecheck(n.body, env, fastLocals, \
        #    typing.Misc(ret=to, cls=misc.cls, receiver=receiver, extenv=misc.extenv, gensymmer=misc.gensymmer, typenames=misc.typenames, extend=misc), costEnv)        

        slowBody, slowEnv = slow_ret_misc
        #fastBody, fastEnv = fast_ret_misc

        logging.debug('Function %s typechecker finished in %s' % (n.name, misc.filename), flags.PROC)
        
        force_checks = tyinstance(froms, DynParameters)

        slowArgChecks = sum((check_stmtlist(ast.Name(id=arg.var, ctx=ast.Load(), lineno=n.lineno), ty, 
                                        errmsg('ARG_CHECK', misc.filename, n, arg.var, ty), \
                                            lineno=n.lineno) for (arg, ty) in slowArgTys), [])
        #fastArgChecks = sum((check_stmtlist(ast.Name(id=arg.var, ctx=ast.Load(), lineno=n.lineno), ty, 
        #                                errmsg('ARG_CHECK', misc.filename, n, arg.var, ty), \
        #                                    lineno=n.lineno) for (arg, ty) in fastArgTys), [])

        logging.debug('Returns checker starting in %s' % misc.filename, flags.PROC)
        fo = self.falloffvisitor.dispatch_statements(body) #NOTE: probably need to find costs here
        logging.debug('Returns checker finished in %s' % misc.filename, flags.PROC)
        #NOTE: taking off this check, just make sure to unify type against void
        sub = compose(slow_bsub, sub)
        if to != Dyn and to != Void and fo != WILL_RETURN:
            subNew, patNew, tyNew = unify(to, Void)
            sub = compose(subNew, sub)
            pat = patternMeet(patNew, pat)
            fty = applySub(fty, sub)
            #updateEnv(env, sub)
            #return ((error_stmt(errmsg('FALLOFF', misc.filename, n, n.name, to), n.lineno)), Dyn, pat, sub)
        #pdb.set_trace()        
        patEnv[Var(lookup_name)] = pat
        
        #pdb.set_trace()
        #print("")

        #functionEnv[name] = env[Var(name)]
        updateEnv(functionEnv, sub)
        localEnv = {}
        #pdb.set_trace()
        #if instantiate(nTy) == Dyn:
        #    pdb.set_trace()
        #print("The type of the function %s is: %s" % (name,  nTy))
        #print("The typing pattern of the function %s is: %s" % (name, pat))        
        #print("The inferred type is: ", rty)

        if (name == "main"):
            print("The number of casts inserted is: %s" % number_of_casts)
            #for f in env:
            #    env[f] = reduceChoices(env[f])
                
            #print("=====================================\n")
            #print("The number of parameters in the program is: ", misc.static.numParams)
            #print("=====================================\n")
            #minpat = pat
            
            
        #+ [ast_trans.FunctionDef(name=name+"_fast", args=args,
        #                              body=fastArgChecks+fastBody, decorator_list=decorator_list,
        #                              returns=(n.returns if hasattr(n, 'returns') else None)
        #                           lineno=n.lineno), assign]

        sub_inferred(sub)
        return ([ast_trans.FunctionDef(name=name, args=args,
                                      body=slowArgChecks+slowBody, decorator_list=decorator_list,
                                      returns=(n.returns if hasattr(n, 'returns') else None),
                                       lineno=n.lineno), assign], nTy, pat, sub)

    def visitarguments(self, n, env, nparams, misc, lineno):
        def argextract(arg):
            if flags.PY_VERSION == 3 and flags.PY3_VERSION >= 4:
                return arg.arg
            else: return arg
        specials = []
        if n.vararg:
            specials.append(Var(argextract(n.vararg), n))
        if n.kwarg:
            specials.append(Var(argextract(n.kwarg), n))
        if flags.PY_VERSION == 3 and n.kwonlyargs:
            specials += [Var(arg.arg, n) for arg in n.kwonlyargs]
        
        checked_args = nparams.lenmatch(n.args)
        #pdb.set_trace()
        assert checked_args != None, '%s <> %s, %s, %d' % (nparams, ast.dump(n), misc.filename, lineno)
        checked_args = checked_args[-len(n.defaults):]

        defaults = []
        sub = {}
        pat = 2
        tys = []
        for val, (k, ty) in zip(n.defaults, checked_args):
            ((val, vty), ptys, ppat, psubs) = self.dispatch(val, env, misc)
            res = cast(env, misc.cls, val, vty, ty, errmsg('DEFAULT_MISMATCH', misc.filename, lineno, k, ty), misc=misc)
            sub = compose(psubs, sub)
            pat = patternMeet(pat, ppat)
            tys.append(ptys)
            defaults.append(res)
        #NOTE: typecheck each arg
        args, argns = tuple(zip(*[(self.visitarg(arg, env, misc))[0] for arg in n.args])) if\
            len(n.args) > 0 else ([], [])

        args = list(args)
        argns = list(argns)

        assert len(defaults) == len(n.defaults)

        if flags.PY_VERSION == 3:
            kw_defaults = [(fixup(self.dispatch(d, env, misc)[0], lineno) if d else None) for d in n.kw_defaults]

            nargs = dict(args=args, vararg=n.vararg,
                         kwonlyargs=n.kwonlyargs, kwarg=n.kwarg,
                         defaults=defaults, kw_defaults=kw_defaults)

            if flags.PY3_VERSION < 4:
                nargs['kwargannotation'] = Nonenn
                nargs['varargannotation'] = n.varargannotation
        elif flags.PY_VERSION == 2:
            nargs = dict(args=args, vararg=n.vararg, kwarg=None, defaults=defaults)
        sub_inferred(sub)
        return ((ast.arguments(**nargs), argns, [(k, Dyn) for k in specials]), tys, pat, sub)

    def visitarg(self, n, env, misc):
        #NOTE: Function to handle if annotation is present?
        def annotation(n):
            if misc.cls:
                if isinstance(n, ast.Name) and n.id == misc.cls.name:
                    if misc.receiver:
                        return (ast.Attribute(value=misc.receiver, attr='__class__', ctx=ast.Load()))
                    else: return None
                elif isinstance(n, ast.Attribute):
                    return (ast.Attribute(value=annotation(n.value), attr=n.attr, ctx=n.ctx))
            return n
        if flags.PY_VERSION == 3:
            return ((ast.arg(arg=n.arg, annotation=annotation(n.annotation)), n.arg), Dyn, 2, {})
        else: return ((n, n.id), Dyn, 2, {}) 
            
    def visitReturn(self, n, env, misc):
        if n.value:
            ((value, ty), exp_ty, exp_pat, exp_sub) = self.dispatch(n.value, env, misc)
            value = cast(env, misc.cls, value, ty, misc.ret, errmsg('RETURN_ERROR', misc.filename, n, misc.ret), misc=misc)
            #pdb.set_trace()
            #total_cost += cost + exp_cost
            #costObj = total_cost
        else:
            value = None
            exp_ty = Dyn
            exp_pat = 2
            exp_sub = {}
            #if not subcompat(Void, misc.ret):
            #    return (error_stmt(errmsg('RETURN_NONEXISTANT', misc.filename, n, misc.ret), lineno=n.lineno), costObj, Dyn, 0, {})
        sub_inferred(exp_sub)
        return ([ast.Return(value=value, lineno=n.lineno)], exp_ty, exp_pat, exp_sub)

    # Assignment stuff
    def visitAssign(self, n, env, misc):
        ((val, vty), val_ty, val_pat, val_sub) = self.dispatch(n.value, env, misc)
        ttys = []
        tyList = []
        targets = []
        attrs = []
        #total_cost = cost1
        #pdb.set_trace()
        #newCostEnv = [] + costEnv
        pat = val_pat
        sub = val_sub
        for target in n.targets:
            ((ntarget, tty), cty, cpat, csub) = self.dispatch(target, env, misc)
            #pdb.set_trace()
            tyList.append(tty)
            #tty = makeChoice(fresh_choice_name(), Dyn, fresh_tvar())
            pat = patternMeet(pat, cpat)
            sub = compose(csub, sub)
            #check if n.value is in costEnv, make new temp costEnv for assigns
            #Mutate costEnv for now, but this is probably incorrect and may cause problems
            if (not isinstance(target, ast.Tuple) and isinstance(n.value, ast.Name) and hasattr(n, "id") and Var(n.value.id) in costEnv):
                pass
                #costEnv[Var(target.id)] = costEnv[Var(n.value.id)]
            elif (not isinstance(target, ast.Tuple) and isinstance(n.value, ast.Attribute) and hasattr(n, "attr") and Var(n.value.attr) in costEnv ):
                pass
                #costEnv[Var(target.id)] = costEnv[Var(n.value.attr)]
            if flags.SEMANTICS == 'MONO' and isinstance(target, ast.Attribute) and \
                    not tyinstance(tty, Dyn):
                attrs.append((ntarget, tty))
            else:
                ttys.append(tty)                
                targets.append(ntarget)
        assign_sub, assign_pat, assign_ty = unify(tyList[0], val_ty) #NOTE: I'm assuming targets has one element
        sub = compose(assign_sub, sub)
        stmts = []
        if targets:
            meet = n_info_join(ttys)
            if len(targets) == 1:
                err = errmsg('SINGLE_ASSIGN_ERROR', misc.filename, n, meet)
            else:
                err = errmsg('MULTI_ASSIGN_ERROR', misc.filename, n, ttys)
                
            val = cast(env, misc.cls, val, vty, meet, err, misc=misc)            
            stmts.append(ast.Assign(targets=targets, value=val, lineno=n.lineno))
        for target, tty in attrs:
            lval = cast(env, misc.cls, val, vty, tty, errmsg('SINGLE_ASSIGN_ERROR', misc.filename, n, tty), misc=misc)
            stmts.append(ast.Expr(ast.Call(func=ast.Name(id='retic_setattr_'+\
                                                             ('static' if \
                                                                  tty.static() else 'dynamic'), 
                                                         ctx=ast.Load()),
                                           args=[target.value, ast.Str(s=target.attr), lval, tty.to_ast()],
                                           keywords=[], starargs=None, kwargs=None),
                                  lineno=n.lineno))
        #        #pdb.set_trace()
        #if (len(tyList) >= 1):
        #    newSub, newPat, newTy = unify(applySub(tyList[0], sub), applySub(vty, sub))
        #finalSub = compose(newSub, sub)
        #finalPat = patternMeet(pat, newPat)
        #updateEnv(env, finalSub)
        sub_inferred(sub)
        return (stmts, val_ty, pat, sub)

    def visitAugAssign(self, n, env, misc):
        optarget = utils.copy_assignee(n.target, ast.Load())
        assignment = ast.Assign(targets=[n.target], 
                                value=ast.BinOp(left=optarget,
                                                op=n.op,
                                                right=n.value,
                                                lineno=n.lineno),
                                lineno=n.lineno)        
        return self.dispatch(assignment, env, misc)

    def visitDelete(self, n, env, misc):
        targets = []
        pat = 2
        sub = {}
        for t in n.targets:
            ((value, ty), val_ty, val_pat, val_sub) = self.dispatch(t, env, misc)
            targets.append(utils.copy_assignee(value, ast.Load()))
            pat = patternMeet(pat, val_pat)
            sub = compose(val_sub, sub)
        sub_inferred(sub)
        return ([ast.Expr(targ, lineno=n.lineno) for targ in targets] + \
                    [ast.Delete(targets=n.targets, lineno=n.lineno)], Dyn, 2, sub)

    # Control flow stuff
    def visitIf(self, n, env, misc):
        #pdb.set_trace()
        ((test, tty), cond_ty, cond_pat, cond_sub) = self.dispatch(n.test, env, misc)
        newSub, pat, newty = unify(tty, rtypes.Bool)
        #pdb.set_trace()
        #updateEnv(env, newSub)
        newEnv1 = env
        
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, newEnv1, misc)        
        (orelse,  else_ty, else_pat, else_sub) = self.dispatch(n.orelse, env, misc) if n.orelse else ([], Dyn, 2, {})
       
        flowName = fresh_choice_name()
        #misc.static.flowChoices.append(flowName)
        #cF = makeChoice(flowName, body_ty, else_ty, rtypes.VariType.FLOW)        
        #branchSubs = relations.mergeSubs(flowName, body_sub, else_sub, rtypes.VariType.FLOW)        
        #sub1 = compose(body_sub, cond_sub)
        #sub = compose(else_sub, sub1)
        #sub = compose(newSub, sub)
        cF = Dyn if (body_ty != else_ty and n.orelse) else body_ty
        sub = compose(newSub, cond_sub)
        #sub = compose(branchSubs, sub1)
        #pat1 = patternMeet(cond_pat, body_pat)
        #pat2 = patternMeet(else_pat, pat1)
        #pat2 = makeChoice(flowName, body_pat, else_pat, rtypes.VariType.FLOW)        
        pat3 = patternMeet(patternMeet(body_pat, else_pat), cond_pat)
        #branchSub, branchPat, branchTy = unify(applySub(body_ty, sub), applySub(else_ty, sub))
        finalSub = compose(sub, else_sub)
        finalPat = pat3
        #updateEnv(env, finalSub)
                
        #if statement will now return a choice with the type of its branches
        sub_inferred(sub)
        return ([ast.If(test=test, body=body, orelse=orelse, lineno=n.lineno)], cF, finalPat, finalSub) 

    def visitFor(self, n, env, misc):
        ((target, tty), targ_ty, targ_pat, targ_sub) = self.dispatch(n.target, env, misc)
        ((iter, ity), iter_ty, iter_pat, iter_sub) = self.dispatch(n.iter, env, misc)
        sub1 = compose(iter_sub, targ_sub)
        pat1 = patternMeet(targ_pat, iter_pat)
        #if our iteration variable is Dyn try to infer a type based on the expression being iterated over
        #if (tty == Dyn):
        #    tty = fresh_tvar()
        #    env[Var(target.id)] = tty
            #unfortunately the above doesn't cause the updated env to have the new type by the next pass
        if isinstance(ity, rtypes.List):
            sub, pat, ty = unify(rtypes.List(tty), ity)
        elif isinstance(ity, rtypes.Dict):
            sub, pat, ty = unify(ity.keys, tty)
        elif tyinstance(ity, Iterable):
            #pdb.set_trace()
            sub, pat, ty = unify(rtypes.Iterable(tty), ity)
        elif isinstance(ity, rtypes.Choice):
            sub, pat, ty = unify(rtypes.Iterable(tty), ity)            
        else:            
            sub, pat, ty = unify(rtypes.Iterable(tty), rtypes.Iterable(Dyn))#{}, 2, Dyn
        
        pat2 = patternMeet(pat, pat1)
        sub2 = compose(sub, sub1)
        #updateEnv(env, sub)
        newEnv1 = env
        #infer types over the body of the loop
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, newEnv1, misc)
        #infer types for the else expression if it exists in the program
        (orelse, else_ty, else_pat, else_sub) = self.dispatch(n.orelse, newEnv1, misc) if n.orelse else ([], Dyn, 2, {})
        branchSubs = compose(body_sub, sub2)
        branchPat = body_pat
        cF = body_ty
        if n.orelse:
            #old code for dealing with creating flow variations is commented out below
            #flowName = fresh_choice_name()
            #misc.static.flowChoices.append(flowName)
            #branchSubs = relations.mergeSubs(flowName, body_sub, else_sub, VariType.FLOW)
            branchSubs = compose(else_sub, branchSubs)
            branchPat = patternMeet(body_pat, else_pat) #makeChoice(flowName, body_pat, else_pat, VariType.FLOW)
            #cF = makeChoice(flowName, body_ty, else_ty, rtypes.VariType.FLOW)
            cF = Dyn if body_ty != else_ty else body_ty
        finalSub = branchSubs        
        finalPat = patternMeet(pat2, branchPat)
        #pdb.set_trace()
        #costObj = 0
        #costObj.prodCoeff = iter
        #costObj.mult(total_cost)
        #pdb.set_trace()
                #NOTE: not sure if I need to get a cost from below
        targcheck = check_stmtlist(utils.copy_assignee(target, ast.Load()),
                                   tty, errmsg('ITER_CHECK', misc.filename, n, tty), lineno=n.lineno)
        def check_ity(ity):
            #pdb.set_trace()
            if tyinstance(ity, Choice):
                left = check_ity(selL(ity.name, ity))                
                right = check_ity(selR(ity.name, ity))
                return makeChoice(ity.name, left, right, ity.kind)
            elif tyinstance(ity, List):
                iter_ty = List(tty)
            elif tyinstance(ity, Dict):
                iter_ty = Dict(tty, ity.values)
            elif tyinstance(ity, Tuple):
                iter_ty = Tuple(*([tty] * len(ity.elements)))            
            else: iter_ty = Dyn
            exp = cast(env, misc.cls, iter, ity, iter_ty,
                            errmsg('ITER_ERROR', misc.filename, n, iter_ty), misc=misc)
            return exp
        #Do not add the cost below!
        res =cast(env, misc.cls, iter, ity, iter_ty,
                            errmsg('ITER_ERROR', misc.filename, n, iter_ty), misc=misc)
        #cost = check_ity(applySub(ity, finalSub))
        #NOTE: Very important to add objects here to keep track of iteration costs and not just constant cost
        sub_inferred(finalSub)
        return ([ast.For(target=target, iter=res,
                         body=targcheck+body, orelse=orelse, lineno=n.lineno)], applySub(cF, finalSub), finalPat, finalSub)
        
    def visitWhile(self, n, env, misc):
        #pdb.set_trace()
        ((test, tty), test_ty, test_pat, test_sub) = self.dispatch(n.test, env, misc)
        newEnv = env
        #updateEnv(env, test_sub)
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, newEnv, misc)
        #pat1 = patternMeet(body_pat, test_pat)
        #sub, patt, typ = unify(test_ty, rtypes.Bool)
        #pat1 = patternMeet(pat1, patt)
        #subb = compose(test_sub, sub)
        #sub1 = compose(body_sub, test_sub)
        (orelse, else_ty, else_pat, else_sub) = self.dispatch(n.orelse, newEnv, misc) if n.orelse else ([], Dyn, 2, {})
        branchSubs = compose(body_sub, test_sub)
        branchPat = body_pat
        cF = body_ty
        if n.orelse:
            #flowName = fresh_choice_name()
            #misc.static.flowChoices.append(flowName)
            #branchSubs = relations.mergeSubs(flowName, body_sub, else_sub, VariType.FLOW)
            #branchPat = makeChoice(flowName, body_pat, else_pat, VariType.FLOW)
            #cF = makeChoice(flowName, body_ty, else_ty, rtypes.VariType.FLOW)
            branchSubs = compose(else_sub, branchSubs)
            branchPat = patternMeet(branchPat, else_pat)
            cF = Dyn if body_ty != else_ty else body_ty
            
        finalSub = branchSubs #need to factor out useless assignments       
        finalPat = patternMeet(test_pat, branchPat)        
        
        #updateEnv(env, finalSub)
        
        sub_inferred(finalSub)
        return ([ast.While(test=test, body=body, orelse=orelse, lineno=n.lineno)], applySub(cF, finalSub), finalPat, finalSub)

    def visitWith(self, n, env, misc):
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, env, misc)
        if flags.PY_VERSION == 3 and flags.PY3_VERSION >= 3:
            items = [self.dispatch(item, env, misc) for item in n.items]
            items = [x[0] for x in items]            
            sub_inferred(body_sub)
            return ([ast.With(items=items, body=body, lineno=n.lineno)], body_ty, body_pat, body_sub)
        else:
            ((context_expr, _),  context_ty, context_pat, context_sub) = self.dispatch(n.context_expr, env, misc)
            ((optional_vars, _),  op_ty, op_pat, op_sub) = self.dispatch(n.optional_vars, env, misc) if\
                               n.optional_vars else ((None, Dyn), Dyn, 2, {})
            pat1 = patternMeet(context_pat, body_pat)
            finalPat = patternMeet(op_pat, pat1)
            sub1 = compose(context_sub, body_sub)
            finalSub = compose(op_sub, sub1)
            sub_inferred(finalSub)
            return ([ast.With(context_expr=context_expr, optional_vars=optional_vars, 
                              body=body, lineno=n.lineno)], body_ty, finalPat, finalSub)
    
    def visitwithitem(self, n, env, misc):
        ((context_expr, _),  context_ty, context_pat, context_sub) = self.dispatch(n.context_expr, env, misc)
        ((optional_vars, _),  op_ty, op_pat, op_sub) = self.dispatch(n.optional_vars, env, misc) if\
                           n.optional_vars else ((None, Dyn), Dyn, 2, {})
        pat = patternMeet(context_pat, op_pat)
        sub = compose(op_sub, context_sub)
        sub_inferred(sub)
        return (ast.withitem(context_expr=context_expr, optional_vars=optional_vars), context_ty, pat, sub)
        

    # Class stuff
    def visitClassDef(self, n, env, misc): #Keywords, kwargs, etc
        global functionEnv
        global class_name
        old_class_name = class_name
        bases = [ast.Call(func=ast.Name(id='retic_actual', ctx=ast.Load()), args=[base], 
                          kwargs=None, starargs=None, keywords=[]) for\
                base in [self.dispatch(base, env, misc) for base in n.bases]] #NOTE: need to add costs in class
        keywords = [] # Will be ignored if py2
        if flags.PY_VERSION == 3:
            metaclass_handled = flags.SEMANTICS != 'MONO'
            for keyword in n.keywords:
                ((kval, _),  kty, kpat, ksub) = self.dispatch(keyword.value, env, misc)
                if flags.SEMANTICS == 'MONO' and keyword.arg == 'metaclass':
                    metaclass_handled = True
                keywords.append(ast.keyword(arg=keyword.arg, value=kval))
                #costObj += (cost)
            if not metaclass_handled:
                logging.warn('Adding Monotonic metaclass to classdef at line %s: <%s>' % (n.lineno,
                                                                                  n.name), 1)
                keywords.append(ast.keyword(arg='metaclass', 
                                            value=ast.Name(id=runtime.Monotonic.__name__,
                                                           ctx=ast.Load())))
        nty = env[Var(n.name)]
        #pdb.set_trace()
        oenv = misc.extenv if misc.cls else env.copy()
        env = env.copy()
        
        initial_locals = {Var(n.name, n): nty}

        stype = ast.Assign(targets=[ast.Name(id='retic_class_type', ctx=ast.Store(), 
                                             lineno=n.lineno)],
                           value=nty.to_ast(), lineno=n.lineno)

        

        class_name = n.name

        if tyinstance(nty, Class):
            new_members = dict(nty.members)
            for member_name, member_type in nty.members.items():
                fast_name = class_name + member_name + "_fast"
                if Var(fast_name) in functionEnv:
                    new_members[fast_name] = functionEnv[Var(fast_name)]
            nty.members = new_members
                    
        
            
        logging.debug('Class %s typechecker starting in %s' % (n.name, misc.filename), flags.PROC)
        ((rest, _),  ty, pat, sub) = misc.static.typecheck(n.body, env, initial_locals, 
                                        typing.Misc(ret=Void, cls=nty, gensymmer=misc.gensymmer, typenames=misc.typenames,
                                                    methodscope=True, extenv=oenv, extend=misc))
        #costObj += (cost1)
        if flags.SEMANTICS not in ['MGDTRANS', 'TRANS']:
            body = [stype] + rest
        else:
            body = rest
        logging.debug('Class %s typechecker finished in %s' % (n.name, misc.filename), flags.PROC)

        name = n.name if n.name not in rtypes.TYPES else n.name + '_'
        #NOTE: I am removing the cast to the class type for now, to prevent the creation of class proxies
        #
        #res, cost = cast(env, misc.cls, ast.Name(id=name, ctx=ast.Load(), lineno=n.lineno), Dyn, nty, 
        #                               errmsg('BAD_CLASS_INJECTION', misc.filename, n, nty), misc=misc)
        #costObj += (cost)
        assign = ast.Assign(targets=[ast.Name(id=name, ctx=ast.Store(), lineno=n.lineno)], 
                            value=res, lineno=n.lineno)
        sub_inferred(sub)
        class_name = old_class_name
        return ([ast_trans.ClassDef(name=name, bases=bases, keywords=keywords,
                                   starargs=(n.starargs if hasattr(n, 'starargs') else None),
                                   kwargs=(n.kwargs if hasattr(n, 'kwargs') else None), body=body,
                                    decorator_list=n.decorator_list, lineno=n.lineno), assign], nty, 2, {})

    # Exception stuff
    # Python 2.7, 3.2
    def visitTryExcept(self, n, env, misc):
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, env, misc)
        handlers = []
        sub = body_sub
        pat = body_pat
        for handler in n.handlers:
            (handler,  hty, hpat, hsub) = self.dispatch(handler, env, misc)
            handlers.append(handler)
            pat = patternMeet(pat, hpat)
            sub = compose(sub, hsub)
        (orelse,  else_ty, else_pat, else_sub) = self.dispatch(n.orelse, env, misc) if n.orelse else ([], Dyn, 2, {})
        finalPat = patternMeet(pat, else_pat)
        finalSub = compose(sub, else_sub)
        sub_inferred(finalSub)
        return ([ast.TryExcept(body=body, handlers=handlers, orelse=orelse, lineno=n.lineno)], body_ty, finalPat, finalSub)

    # Python 2.7, 3.2
    def visitTryFinally(self, n, env, misc):
        (body,  body_ty, body_pat, body_sub) = self.dispatch(n.body, env, misc)
        (finalbody, final_ty, final_pat, final_sub) = self.dispatch(n.finalbody, env, misc)
        sub = compose(final_sub, body_sub)
        pat = patternMeet(final_pat, body_pat)
        sub_inferred(sub)
        return ([ast.TryFinally(body=body, finalbody=finalbody, lineno=n.lineno)], body_ty, pat, sub)
    
    # Python 3.3
    def visitTry(self, n, env, misc):
        (body, body_ty, body_pat, body_sub) = self.dispatch(n.body, env, misc)
        handlers = []
        sub = body_sub
        pat = body_pat
        for handler in n.handlers:
            (handler, hty, hpat, hsub) = self.dispatch(handler, env, misc)
            handlers.append(handler)
            pat = patternMeet(pat, hpat)
            sub = compose(sub, hsub)
        (orelse, ety, epat, esub) = self.dispatch(n.orelse, env, misc) if n.orelse else ([],  Dyn, 2, {})
        sub = compose(sub, esub)
        pat = patternMeet(pat, epat)
        (finalbody, fty, fpat, fsub) = self.dispatch(n.finalbody, env, misc)
        sub = compose(sub, fsub)
        pat = patternMeet(pat, fpat)
        sub_inferred(sub)
        return ([ast.Try(body=body, handlers=handlers, orelse=orelse, finalbody=finalbody, lineno=n.lineno)], body_ty, pat, sub)

    def visitExceptHandler(self, n, env, misc):
        ((type, tyty), ety, epat, esub) = self.dispatch(n.type, env, misc) if n.type else ((None, Dyn),  Dyn, 2, {})
        (body, bty, bpat, bsub) = self.dispatch(n.body, env, misc)
        sub = compose(bsub, esub)
        pat = patternMeet(bpat, epat)
        if flags.PY_VERSION == 2 and n.name and type:
            ((name, nty), ntype, npat, nsub) = self.dispatch(n.name, env, misc)
            type= cast(env, misc.cls, type, tyty, nty, errmsg('EXCEPTION_ERROR', misc.filename, n, n.name, nty, n.name), misc=misc)
            sub = compose(sub, nsub)
            pat = compose(pat, npat)
        else: 
            name = n.name
            sub_inferred(sub)
        return (ast.ExceptHandler(type=type, name=name, body=body, lineno=n.lineno), ety, pat, sub)

    def visitRaise(self, n, env, misc):
        if flags.PY_VERSION == 3:
            ((exc, _), ety, epat, esub) = self.dispatch(n.exc, env, misc) if n.exc else ((None, Dyn), Dyn, 2, {})
            ((cause, _), cty, cpat, csub) = self.dispatch(n.cause, env, misc) if n.cause else ((None, Dyn), Dyn, 2, {})
            pat = patternMeet(epat, cpat)
            sub = compose(csub, esub)
            sub_inferred(sub)
            return ([ast.Raise(exc=exc, cause=cause, lineno=n.lineno)], ety, pat, sub)
        elif flags.PY_VERSION == 2:
            ((type, _), tty, tpat, tsub) = self.dispatch(n.type, env, misc) if n.type else ((None, Dyn))
            ((inst, _), ity, ipat, isub) = self.dispatch(n.inst, env, misc) if n.inst else ((None, Dyn))
            ((tback, _), bty, bpat, bsub) = self.dispatch(n.tback, env, misc) if n.tback else ((None, Dyn))            
            sub1 = compose(tsub, isub)
            pat1 = patternMeet(tpat, ipat)
            sub = compose(bsub, sub1)
            pat = patternMeet(pat1, bpat)
            sub_inferred(sub)
            return ([ast.Raise(type=type, inst=inst, tback=tback, lineno=n.lineno)], tty, pat, sub)

    def visitAssert(self, n, env, misc):
        ((test, _), tty, tpat, tsub) = self.dispatch(n.test, env, misc)
        ((msg, _),  mty, mpat, msub) = self.dispatch(n.msg, env, misc) if n.msg else ((None, Dyn), Dyn, 2, {})
        sub = compose(msub, tsub)
        pat = patternMeet(tpat, mpat)
        sub_inferred(sub)
        return ([ast.Assert(test=test, msg=msg, lineno=n.lineno)], tty, pat, sub)

    # Declaration stuff
    def visitGlobal(self, n, env, misc):
                return ([n], Dyn, 2, {})

    def visitNonlocal(self, n, env, misc):
                return ([n], Dyn, 2, {})

    # Miscellaneous
    def visitExpr(self, n, env, misc):
        ((value, ty), ety, epat, esub) = self.dispatch(n.value, env, misc)
        sub_inferred(esub)
        return ([ast.Expr(value=value, lineno=n.lineno)], ety, epat, esub)

    def visitPass(self, n, env, misc):
                return ([n], Dyn, 2, {})

    def visitBreak(self, n, env, misc):
                return ([n], Dyn, 2, {})

    def visitContinue(self, n, env, misc):
                return ([n], Dyn, 2, {})

    def visitPrint(self, n, env, misc):
        ((dest, _),  dty, dpat, dsub) = self.dispatch(n.dest, env, misc) if n.dest else ((None, Void), Void, 2, {})
        (values) = [self.dispatch(val, env, misc) for val in n.values]
        values = [x[0] for x in values]
        val_tys = [x[1] for x in values]
        val_pats = [x[2] for x in values]
        val_subs = [x[3] for x in values]
        pat = reduce(patternMeet, val_pats, 2)
        sub = reduce(lambda x, y: compose(x, y), val_subs, {})
        sub_inferred(sub)
        return ([ast.Print(dest=dest, values=values, nl=n.nl, lineno=n.lineno)], Void, pat, sub)

    def visitExec(self, n, env, misc):
        ((body, _),  bty, bpat, bsub) = self.dispatch(n.body, env, misc)
        ((globals, _),  gty, gpat, gsub) = self.dispatch(n.globals, env, misc) if n.globals else ((None, Void), Dyn, 2, {})
        ((locals, _), lty, lpat, lsub) = self.dispatch(n.locals, env, misc) if n.locals else ((None, Void),  Dyn, 2, {})
        sub1 = compose(gsub, bsub)
        pat1 = patternMeet(bpat, gpat)
        sub = compose(lsub, sub1)
        pat = patternMeet(lpat, pat1)
        sub_inferred(sub)
        return ([ast.Exec(body=body, globals=globals, locals=locals, lineno=n.lineno)], bty, pat, sub)

### EXPRESSIONS ###
    # Op stuff
    def visitBoolOp(self, n, env, misc):
        values = []
        tys = []
        for value in n.values: 
            ((value, ty),  vty, vpat, vsub) = self.dispatch(value, env, misc)
            values.append(value)
            tys.append(ty)
            #total_cost += (value_cost)
        ty = tyjoin(tys)
        sub_inferred(vsub)
        return ((ast.BoolOp(op=n.op, values=values, lineno=n.lineno), ty), vty, vpat, vsub)

    def visitBinOp(self, n, env, misc):
        ((left, lty),  l_ty, lpat, lsub) = self.dispatch(n.left, env, misc)
        newEnv = env
        #updateEnv(env, lsub)
        ((right, rty),  r_ty, rpat, rsub) = self.dispatch(n.right, newEnv, misc)
        sub = compose(lsub, rsub)
        pat = patternMeet(lpat, rpat)
        #newSub, newPat, ty = binop_type(l_ty, n.op, r_ty)
        isub, ipat, ity = infer_binop_type(l_ty, n.op, r_ty)
        sub = compose(isub, sub)
        pat = patternMeet(ipat, pat)
        ty = applySub(ity, sub)
        #updateEnv(env, sub)
        #if not ty.top_free():
            #return ((error(errmsg('BINOP_INCOMPAT', misc.filename, n, lty, rty, get_binop(n.op)), lineno=n.lineno), Dyn), ty, pat, sub)
        node = ast.BinOp(left=left, op=n.op, right=right, lineno=n.lineno)
        sub_inferred(sub)
        return ((node, ty), ty, pat, sub)

    def visitUnaryOp(self, n, env, misc):
        ((operand, ty),  rty, rpat, rsub) = self.dispatch(n.operand, env, misc)
        node = ast.UnaryOp(op=n.op, operand=operand, lineno=n.lineno)
        if isinstance(n.op, ast.Invert):            
            sub, pat, ty = unify(ty, Int)#primjoin([ty], Int, Int)
        elif any([isinstance(n.op, op) for op in [ast.UAdd, ast.USub]]):
            sub, pat, tyn = unify(ty, Float)#ty = primjoin([ty])
        elif isinstance(n.op, ast.Not):
            sub, pat, ty = unify(ty, Bool)
        finalSub = compose(sub, rsub)
        finalPat = patternMeet(pat, rpat)        
        sub_inferred(finalSub)
        return ((node, ty), ty, finalPat, finalSub)

    def visitCompare(self, n, env, misc):        
        ((left, _), lty, lpat, lsub) = self.dispatch(n.left, env, misc)
        if (isinstance(n.ops[0], ast.Eq) or isinstance(n.ops[0], ast.NotEq)):
            #pdb.set_trace()
            comparators = [(comp, cty, cpat, csub) for ((comp, _), cty, cpat, csub) in [self.dispatch(ocomp, env, misc) for ocomp in n.comparators]]        
            comp_pats = [x[2] for x in comparators]
            comp_subs = [x[3] for x in comparators]
        
            pat = reduce(patternMeet, comp_pats, 2)
            sub = reduce(lambda x, y: compose(x, y), comp_subs, {})

            sub = compose(sub, lsub)
            pat = patternMeet(pat, lpat)
            comparators = [x[0] for x in comparators]
            sub_inferred(sub)
            return ((ast.Compare(left=left, ops=n.ops, comparators=comparators, lineno=n.lineno), Bool), Bool, pat, sub)
        else:
            (left_sub, left_pat, left_ty) = ({}, 2, lty) if lty in [Dyn, Int, Float] else unify(lty, Float)
            #lsub, lpat, lty = unify(lty, Float)
            comparators = [(comp, cty, cpat, csub) for ((comp, _), cty, cpat, csub) in [self.dispatch(ocomp, env, misc) for ocomp in n.comparators]]        
            comp_pats = [x[2] for x in comparators]
            comp_subs = [x[3] for x in comparators]
        
            pat = reduce(patternMeet, comp_pats, 2)
            sub = reduce(lambda x, y: compose(x, y), comp_subs, {})
            rty = (comparators[0])[2]
        
            (right_sub, right_pat, right_ty) = ({}, 2, rty) if rty in [Dyn, Int, Float, Complex] else unify(rty, Float)
            sub = compose(sub, lsub)        
            sub = compose(sub, left_sub)
            sub = compose(sub, right_sub)
            pat = patternMeet(pat, lpat)
            pat = patternMeet(pat, left_pat)
            pat = patternMeet(pat, right_pat)        
            comparators = [x[0] for x in comparators]
            sub_inferred(sub)
            return ((ast.Compare(left=left, ops=n.ops, comparators=comparators, lineno=n.lineno), Bool), Bool, pat, sub)

    # Collections stuff    
    def visitList(self, n, env, misc):
        eltdata = [self.dispatch(x, env, misc) for x in n.elts]
        elttys = [ty for ((elt, ty),  ety, epat, esub) in eltdata] 
        elts = [elt for ((elt, ty), _, _, _) in eltdata]
        #el_costs = [cost for ((elt, ty),  _, _, _) in eltdata]
        elpats = [epat for ((elt, ty),  ety, epat, esub) in eltdata]
        elsubs = [esub for ((elt, ty), ety, epat, esub) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        sub = reduce(lambda x, y: compose(x, y), elsubs, {})
        if isinstance(n.ctx, ast.Store):
            ty = Tuple(*elttys)
        else:
            inty = tyjoin(elttys)
            ty = List(inty) if flags.TYPED_LITERALS else Dyn
        sub_inferred(sub)
        return ((ast.List(elts=elts, ctx=n.ctx, lineno=n.lineno), ty), ty, pat, sub)

    def visitTuple(self, n, env, misc):
        eltdata = [self.dispatch(x, env, misc) for x in n.elts]
        tys = [ty for ((elt, ty),  ety, epat, esub) in eltdata]
        elts = [elt for ((elt, ty),  ety, epat, esub) in eltdata]
        elpats = [epat for ((elt, ty),  ety, epat, esub) in eltdata]
        elsubs = [esub for ((elt, ty),  ety, epat, esub) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        sub = reduce(lambda x, y: compose(x, y), elsubs, {})
        if isinstance(n.ctx, ast.Store):
            ty = Tuple(*tys)
        else:
            ty = Tuple(*tys) if flags.TYPED_LITERALS else Dyn
        sub_inferred(sub)
        return ((ast.Tuple(elts=elts, ctx=n.ctx, lineno=n.lineno), ty), ty, pat, sub)

    def visitDict(self, n, env, misc):
        keys = [self.dispatch(key, env, misc) for key in n.keys]
        vals = [self.dispatch(val, env, misc) for val in n.values]
        keydata = [x[0] for x in keys]
        valdata = [x[0] for x in vals]
        keypats = [x[2] for x in keys]
        valpats = [x[2] for x in vals]
        keysubs = [x[3] for x in keys]
        valsubs = [x[3] for x in vals]
        pat1 = reduce(patternMeet, keypats, 2)
        sub1 = reduce(lambda x, y: compose(x, y), keysubs, {})
        pat2 = reduce(patternMeet, valpats, 2)
        sub2 = reduce(lambda x, y: compose(x, y), valsubs, {})
        pat = patternMeet(pat1, pat2)
        sub = compose(sub1, sub2)
        keys, ktys = list(zip(*keydata)) if keydata else ([], [])
        values, vtys = list(zip(*valdata)) if valdata else ([], [])
                #pdb.set_trace()
        #A weird thing happens when the last entry in a list to tyjoin is Int and the previous entries were Float
        sub_inferred(sub)
        return ((ast.Dict(keys=list(keys), values=list(values), lineno=n.lineno),\
                    Dict(tyjoin(list(ktys)), tyjoin(list(vtys)))), Dict(tyjoin(list(ktys)), tyjoin(list(vtys))), pat, sub)

    def visitSet(self, n, env, misc):
        eltdata = [self.dispatch(x, env, misc) for x in n.elts]
        elttys = [ty for ((elt, ty),  _, _, _) in eltdata]
        elpats = [pat for ((elt, ty),  _, pat, _) in eltdata]
        elsubs = [sub for ((elt, ty),  _, _, sub) in eltdata]
        pat = reduce(patternMeet, elpats, 2)
        sub = reduce(lambda x, y: compose(x, y), elsubs, {})
        ty = tyjoin(elttys)
        elts = [elt for ((elt, ty),  _, _, _) in eltdata]
        sub_inferred(sub)
        return ((ast.Set(elts=elts, lineno=n.lineno), Set(ty) if flags.TYPED_LITERALS else opTy), opTy, pat, sub)

    def visitListComp(self, n, env, misc):
        disp = [self.dispatch(generator, env, misc, n.lineno) for generator in n.generators]
        disp_pats = [x[2] for x in disp]
        disp_subs = [x[3] for x in disp]
        pat = reduce(patternMeet, disp_pats, 2)
        sub = reduce(lambda x, y: compose(x, y), disp_subs, {})
        disp_tys = [x[2] for x in disp]
        #NOTE:I'm assuming generators has one elment...I might be wrong.
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety),  elt_ty, elt_pat, elt_sub) = self.dispatch(n.elt, lenv, misc)
        pat = patternMeet(pat, elt_pat)
        sub = compose(elt_sub, sub)
        finalTy = List(applySub(elt_ty, sub))
        #pdb.set_trace()
        sub_inferred(sub)
        return ((check(ast.ListComp(elt=elt, generators=list(generators), lineno=n.lineno), finalTy, errmsg('COMP_CHECK', misc.filename, n, finalTy)),\
                 (finalTy if flags.TYPED_LITERALS else Dyn)), finalTy, pat, sub)

    def visitSetComp(self, n, env, misc):
        disp = [self.dispatch(generator, env, misc, n.lineno) for generator in n.generators]
        disp_pats = [x[2] for x in disp]
        disp_subs = [x[3] for x in disp]
        pat = reduce(patternMeet, disp_pats, 2)
        sub = reduce(lambda x, y: compose(x, y), disp+subs, {})
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety),  elt_ty, elt_pat, elt_sub) = self.dispatch(n.elt, lenv, misc)
        pat = patternMeet(pat, elt_pat)
        sub = compose(elt_sub, sub)
        sub_inferred(sub)
        return ((check(ast.SetComp(elt=elt, generators=list(generators), lineno=n.lineno), Set(ety), errmsg('COMP_CHECK', misc.filename, n, Set(ety))), \
                 (Set(ety) if flags.TYPED_LITERALS else Dyn)), Set(elt_ty), pat, sub)
    
    def visitDictComp(self, n, env, misc):
        disp = [self.dispatch(generator, env, misc, n.lineno) for generator in n.generators]
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv,[])))
        ((key, kty),  key_ty, key_pat, key_sub) = self.dispatch(n.key, lenv, misc)
        ((value, vty), val_ty, val_pat, val_sub) = self.dispatch(n.value, lenv, misc)
        pat = patternMeet(val_pat, key_pat)
        sub = compose(val_sub, key_sub)
        sub_inferred(sub)
        return ((check(ast.DictComp(key=key, value=value, generators=list(generators), lineno=n.lineno), Dict(kty, vty), errmsg('COMP_CHECK', misc.filename, n, Dict(kty, vty))), \
                 (Dict(kty, vty) if flags.TYPED_LITERALS else Dyn)), Dict(key_ty, val_ty), pat, sub)

    def visitGeneratorExp(self, n, env, misc):
        disp = [self.dispatch(generator, env, misc, n.lineno) for generator in n.generators]
        disp = [x[0] for x in disp]
        generators, genenv = zip(*disp) if disp else ([], [])
        lenv = env.copy()
        lenv.update(dict(sum(genenv, [])))
        ((elt, ety),  elty, pat, sub) = self.dispatch(n.elt, lenv, misc)
        sub_inferred(sub)
        return ((check(ast.GeneratorExp(elt=elt, generators=list(generators), lineno=n.lineno), Dyn, errmsg('COMP_CHECK', misc.filename, n, Dyn)), Dyn), Dyn, pat, sub)

    def visitcomprehension(self, n, env, misc, lineno):
        ((iter, ity), iter_ty, ipat, isub) = self.dispatch(n.iter, env, misc)
        ifs = [if_ for ((if_, _),_,_,_) in [self.dispatch(if2, env, misc) for if2 in n.ifs]]        
        ((target, tty), tty, tpat, tsub) = self.dispatch(n.target, env, misc)
        pat = patternMeet(ipat, tpat)
        sub = compose(tsub, isub)
        ety = Dyn

        if tyinstance(ity, List):
            ety = ity.type
        elif tyinstance(ity, Tuple):
            ety = tyjoin(*ity.elements)
        elif tyinstance(ity, Dict):
            ety = ity.keys

        assignments = [(target, ety)]
        new_assignments = []
        while assignments:
            k, v = assignments[0]
            del assignments[0]
            if isinstance(k, ast.Name):
                new_assignments.append((Var(k.id),v))
            elif isinstance(k, ast.Tuple) or isinstance(k, ast.List):
                if tyinstance(v, Tuple):
                    assignments += (list(zip(k.elts, v.elements)))
                elif tyinstance(v, Iterable) or tyinstance(v, List):
                    assignments += ([(e, v.type) for e in k.elts])
                elif tyinstance(v, Dict):
                    assignments += (list(zip(k.elts, v.keys)))
                else: assignments += ([(e, Dyn) for e in k.elts])
        
        usub, upat, uty = unify(ity, Iterable(tty))
        sub = compose(usub, sub)
        pat = patternMeet(upat, pat)
        #NOTE: Below, the Iterable(tty) was commented out and replaced by Dyn. I'm adding it back
        iter_target = Iterable(tty)#Dyn #Iterable(tty)
        finalTy = applySub(tty, sub)
        res = cast(env, misc.cls, iter, ity, iter_target, errmsg('ITER_ERROR', misc.filename, lineno, iter_target), misc=misc)
        sub_inferred(sub)
        return ((ast.comprehension(target=target, iter=res, 
                                   ifs=ifs), new_assignments), finalTy, pat, sub)

    # Control flow stuff
    def visitYield(self, n, env, misc):
        ((value, _),  vty, vpat, vsub) = self.dispatch(n.value, env, misc) if n.value else ((None, Void),  Void, 2, {})
        sub_inferred(vsub)
        return ((ast.Yield(value=value, lineno=n.lineno), Dyn), Dyn, vpat, vsub)

    def visitYieldFrom(self, n, env, misc):
        ((value, _), vty, vpat, vsub) = self.dispatch(n.value, env, misc)
        sub_inferred(vsub)
        return ((ast.YieldFrom(value=value, lineno=n.lineno), Dyn), vty, vpat, vsub)

    def visitIfExp(self, n, env, misc):
        ((test, _),  tty, tpat, tsub) = self.dispatch(n.test, env, misc)
        ((body, bty),  bty, bpat, bsub) = self.dispatch(n.body, env, misc)
        ((orelse, ety),  ety, epat, esub)  = self.dispatch(n.orelse, env, misc)
        #pat1 = patternMeet(tpat, bpat)
        #sub1 = compose(tsub, bsub)
        #pat = patternMeet(pat1, epat)
        #sub = compose(sub1, esub)
        testSub, testPat, testTy = unify(tty, Bool)
        #sub = compose(testSub, sub)
        #pat = patternMeet(testPat, pat)
        #finalSub, finalPat, finalTy = unify(applySub(bty, sub), applySub(ety, sub))
        #finalSub = compose(finalSub, sub)
        #finalPat = patternMeet(finalPat, pat)
        #flowName = fresh_choice_name()
        #misc.static.flowChoices.append(flowName)
        #cF = makeChoice(flowName, bty, ety, rtypes.VariType.FLOW)
        cF = Dyn if bty != ety else bty
        #branchSubs = relations.mergeSubs(flowName, bsub, esub, rtypes.VariType.FLOW)
        branchSubs = compose(esub, bsub)
        #cPat = makeChoice(flowName, bpat, epat, rtypes.VariType.FLOW)
        cPat = patternMeet(bpat, epat)
        sub = compose(branchSubs, testSub)
        pat = patternMeet(cPat, tpat)

                #updateEnv(env, sub)
        
        #used to return cF
        sub_inferred(sub)
        return ((ast.IfExp(test=test, body=body, orelse=orelse, lineno=n.lineno), cF), cF, pat, sub)

    # Function stuff
    def visitCall(self, n, env, misc):
        global functionEnv
                
        selV = selectionVisitors.SelVisitor()
        
        if reflection.is_reflective(n):
            return (reflection.reflect(n, env, misc, self), Dyn, 2, {})

        # Python3.5 gets rid of .kwargs and .starargs, instead has a Starred value in the args 
        # and a keyword arg with no key (i.e. kwarg in n.keywords; kwarg = keyword(arg=None, value=[w/e]))
        def has_starargs(n):
            if flags.PY3_VERSION >= 5:
                return (any(isinstance(e, ast.Starred) for e in n.args))
            else: return (n.starargs is not None)
        def has_kwargs(n):
            if flags.PY3_VERSION >= 5:
                return (any(e.arg is None for e in n.keywords))
            else: return (n.kwargs is not None)

        project_needed = [False] # Python2 doesn't have nonlocal
        class BadCall(Exception):
            def __init__(self, msg):
                self.msg = msg
        #NOTE: probably need to change result of cast_args to return all costs
        def cast_args(argdata, fun, funty):
            vs, ss = zip(*argdata) if argdata else ([], [])
            vs = list(vs)
            ss = list(ss)
            if tyinstance(funty, Dyn):
                if n.keywords or has_kwargs(n) or has_starargs(n):
                    targparams = DynParameters
                else: targparams = AnonymousParameters(ss)
                #pdb.set_trace()
                res = cast(env, misc.cls, fun, Dyn, Function(targparams, Dyn),
                                errmsg('FUNC_ERROR', misc.filename, n, Function(targparams, Dyn)), misc=misc)
                #pdb.set_trace()
                return ((vs, res, Dyn), Dyn, 2, {})
            elif tyinstance(funty, Function):
                argcasts = funty.froms.lenmatch(argdata)
                # Prototype implementation for type variables

                if argcasts != None:
                    substs = []
                    casts = []
                    for (v, s), t in argcasts:
                        if isinstance(t, TypeVariable):
                            substs.append((t.name, s))
                            casts.append(v)
                        else:
                            loopres = cast(env, misc.cls, v, s, t, errmsg('ARG_ERROR', misc.filename, n, t), misc=misc)
                            casts.append(loopres)
                    to = funty.to
                    for var,rep in substs:
                        # Still need to merge in case of multiple approaches
                        to = to.substitute(var, rep, False)
                    return ((casts, fun, to), to, 2, {})

                    # return ([cast(env, misc.cls, v, s, t, errmsg('ARG_ERROR', misc.filename, n, t)) for \
                    #             (v, s), t in argcasts],
                    #         fun, funty.to)
                else:
                    #return (([], fun, funty.to), 0, funty.to, 2, {})
                    #pdb.set_trace()
                    raise BadCall(errmsg('BAD_ARG_COUNT', misc.filename, n, funty.froms.len(), len(argdata)))
            elif tyinstance(funty, Class):
                project_needed[0] = True
                if '__init__' in funty.members:
                    inst = funty.instance()
                    funty = funty.member_type('__init__')
                    if tyinstance(funty, Function):
                        funty = funty.bind()
                        funty.to = inst
                else:
                    funty = Function(DynParameters, funty.instance())
                return cast_args(argdata, fun, funty)
            elif tyinstance(funty, Object):
                if '__call__' in funty.members:
                    funty = funty.member_type('__call__')
                    return cast_args(argdata, fun, funty)
                else:
                    mfunty = Function(DynParameters, Dyn)
                    res= cast(env, misc.cls, fun, funty, Record({'__call__': mfunty}), 
                                                   errmsg('OBJCALL_ERROR', misc.filename, n), misc=misc)
                    ret = cast_args(argdata, res,
                                     mfunty)
                    return ret, Dyn, 2, {}
            elif tyinstance(funty, Choice):
                retL,  tyL, patL, subL = cast_args(argdata, fun, selL(funty.name, funty))
                retR,  tyR, patR, subR = cast_args(argdata, fun, selR(funty.name, funty))
                ty = Choice(funty.name, tyL, tyR, funty.kind)
                pat = Choice(funty.name, patL, patR, funty.kind)
                sub = relations.mergeSubs(funty.name, subL, subR, funty.kind)
                return retL, ty, pat, sub
            else: raise BadCall(errmsg('BAD_CALL', misc.filename, n, funty))
            #end of cast_args definition

        #def costEnvHelper(arg):
        #    if isinstance(arg, ast.Attribute):
        #        if (Var(arg.attr) in costEnv):
        #            return costEnv[Var(arg.attr)]
        #    if isinstance(arg, ast.Name):
        #        if (Var(arg.id) in costEnv):
        #            return costEnv[Var(arg.id)]
        #    return 0
        
        #NOTE: self.dispatch calls will eventually return costs     
        ((func, ty),  fty, fpat, fsub) = self.dispatch(n.func, env, misc)        
            
        
        
        if tyinstance(fty, InferBottom):
            return ((n, Dyn), Dyn, 0, {})

        args_new = [self.dispatch(x, env, misc) for x in n.args]
        argdata = [x[0] for x in args_new]
        #arg_tys = [x[1] for x in argdata] #types of arguments?
        arg_tys = [x[1] for x in args_new] #types of arguments?
        arg_subs = [x[3] for x in args_new]
        arg_pats = [x[2] for x in args_new]
        arg_sub = reduce(lambda x, y: compose(x,y), arg_subs, {})
        arg_pat = reduce(patternMeet, arg_pats, 2)
        retty = Dyn
        #retty = ty.to

    
        
        #see if a variable is being used in the function spot
        if getattr(func, "id", None) != None and not tyinstance(fty, Class):
            #uncomment lines below to debug a specific function call

            dom, cod, pat, sub = liftToFunc(fty)
            oty = ty #the original type, not lifted
            ty = Function(dom, cod)
            fpat = patternMeet(fpat, pat)
            fsub = compose(sub, fsub)
            subb = compose(arg_sub, fsub)
            patt = patternMeet(arg_pat, fpat)
            unifierTy = Function(AnonymousParameters(arg_tys), fresh_tvar())
            sub, pat, newTy = unify(ty, unifierTy)
            sub = compose(sub, subb)
            pat = patternMeet(patt, pat)        
            ty = applySub(ty, sub)
            newTy = applySub(newTy, sub)
            #pdb.set_trace()
            #if (sub != {}):
            #    updateEnv(env, sub)
            argdata = list(map(lambda el: (el[0], applySub(el[1], sub)), argdata))
            retty = ty.to
            #pdb.set_trace()
            val_tup,  nty, npat, nsub = cast_args(argdata, func, ty)
            #check if we can replace the function with the fast pass version
            if(Var(func.id+"_vari") in functionEnv and tyinstance(tvars_to_dyn(instantiate(functionEnv[Var(func.id+"_vari")])), Choice)):
                fun_ty = instantiate(functionEnv[Var(func.id+"_vari")])                
                unifierTy = Function(AnonymousParameters(arg_tys), fresh_tvar())
                sub, pat, newTy = unify(fun_ty, unifierTy)
                sub = compose(sub, subb)
                pat = patternMeet(patt, pat)
                fun_ty = tvars_to_dyn(applySub(fun_ty, sub))
                ldom, lcod, lpatF, lsub = liftToFunc(selL(fun_ty.name, fun_ty))
                rdom, rcod, rpatF, rsub = liftToFunc(selR(fun_ty.name, fun_ty))
                #lpat = less_precise_params(ldom, arg_tys)
                #rpat = less_precise_params(rdom, arg_tys)
                #if (rpat):
                #    newVar = ast.Name(id=func.id+"_fast", ctx=ast.Load(), lineno=n.lineno)
                    # now we cast the arguments based on the fast pass type
                    #val_tup, cost, nty, npat, nsub = cast_args(argdata, newVar, selR(fun_ty.name, fun_ty))
                    
            #pat = patternMeet(pat, npat)
            #sub = compose(nsub, sub)
        else:
            #we have a method call
            oty = ty
            sub = compose(arg_sub, fsub)
            ty = Dyn
            pat = patternMeet(arg_pat, fpat)
            argdata = list(map(lambda el: (el[0], applySub(el[1], sub)), argdata))            
            val_tup,  nty, npat, nsub = cast_args(argdata, func, ty) #used to have oty as last argument
            #use original opaque type for method call
            #opaqueName = fresh_choice_name()
            retty = oty
        
        
        
        #args = argdata
        try:
            (args, func, _) = val_tup
        except BadCall as e:
            if flags.REJECT_WEIRD_CALLS or not (n.keywords or has_kwargs(n) or has_starargs(n)):
                return ((error(e.msg, lineno=n.lineno), Dyn), oty, pat, sub)
            else:
                logging.warn('Function calls with keywords, starargs, and kwargs are not typechecked. Using them may induce a type error in file %s (line %d)' % (misc.filename, n.lineno), 0)
                args = n.args
                retty = Dyn
            
        call = ast_trans.Call(func=func, args=args, keywords=n.keywords,
                              starargs=getattr(n, 'starargs', None),
                              kwargs=getattr(n, 'kwargs', None), lineno=n.lineno)
        #NOTE: erase the line below when check has been instrumented with casts and change assignment to use cost2
        if project_needed[0]:
            call = cast(env, misc.cls, call, Dyn, retty, errmsg('BAD_OBJECT_INJECTION', misc.filename, n, retty, ty), misc=misc)
        else: call = check(call, retty, errmsg('RETURN_CHECK', misc.filename, n, retty))
        sub_inferred(sub)
        return ((call, retty), retty, pat, sub)

    def visitLambda(self, n, env, misc):
        ((args, argnames, specials),  aty, apat, asub) = self.dispatch(n.args, env, DynParameters, misc, n.lineno)
        #params = [rtypes.Choice(fresh_choice_name(), Dyn, fresh_tvar())] * len(argnames)
        params = [Dyn] * len(argnames)
        env = env.copy()
        env.update(dict(list(zip(argnames, params))))
        env.update(dict(specials))
        ((body, rty),  bty, bpat, bsub) = self.dispatch(n.body, env, misc)
        if n.args.vararg:
            ffrom = DynParameters
        elif n.args.kwarg:
            ffrom = DynParameters
        elif flags.PY_VERSION == 3 and n.args.kwonlyargs:
            ffrom = DynParameters
        elif n.args.defaults:
            ffrom = DynParameters
        else: ffrom = NamedParameters(list(zip(argnames, params)))
        
        ty = Function(ffrom, rty) if flags.TYPED_LAMBDAS else Dyn
        sub = compose(asub, bsub)
        sub_inferred(sub)
        return ((ast.Lambda(args=args, body=body, lineno=n.lineno), ty), ty, bpat, sub)

    # Variable stuff
    def visitName(self, n, env, misc):
        global patEnv
        global localEnv
        global functionEnv        
        #NOTE: I think we need a second environment with costs
        if isinstance(n.ctx, ast.Param): # Compatibility with 2.7
            return (n.id, Dyn, 2, {})
        id = n.id if n.id not in rtypes.TYPES else n.id + '_'
        astExp = ast.Name(id=id, ctx=n.ctx, lineno=n.lineno)
        try:
            #ty = env[Var(n.id)]
            ty = localEnv[Var(n.id)] if Var(n.id) in localEnv else env[Var(n.id)]
            if Var(n.id) in functionEnv:
                ty = instantiate(functionEnv[Var(n.id)])
            if isinstance(ty, TypeScheme):
                ty = instantiate(ty)
            
                #astExp = makeChoice(vty.name, ast.Name(id=id+"_slow", ctx=n.ctx, lineno=n.lineno), ast.Name(id=id+"_fast", ctx=n.ctx, lineno=n.lineno))
                #astExp = ast.Name(id=id+"_fast", ctx = n.ctx, lineno=n.lineno)
            
            if isinstance(n.ctx, ast.Del) and not tyinstance(ty, Dyn) and flags.REJECT_TYPED_DELETES:
                return ((error(errmsg('TYPED_VAR_DELETE', misc.filename, n, n.id, ty)), Dyn), Dyn, 0, {})
        except KeyError:
            ty = Dyn

        #pdb.set_trace()
        pat = patEnv[Var(n.id)] if Var(n.id) in patEnv else 2 #used to be True in else but seemed wrong
        

        return ((astExp, ty), ty, pat, {})

    def visitNameConstant(self, n, env, misc):
        if flags.TYPED_LITERALS:
            if n.value is True or n.value is False:
                return ((n, Bool), Bool, 2, {})
            elif n.value is None:
                #return ((n, Void), Void, 2, {})
                #NOTE: I'm going to avoid inferring Void in this project
                return ((n, Dyn), Dyn, 2, {})
        return ((n, Dyn), Dyn, 2, {})

    def visitAttribute(self, n, env, misc):
        #pdb.set_trace()
        ((value, vty),  val_ty, vpat, vsub) = self.dispatch(n.value, env, misc)

        if tyinstance(vty, InferBottom):
            #return ((n, Dyn), val_ty, vpat, vsub)
            vpat = 0
        
        if isinstance(vty, Structural) and isinstance(n.ctx, ast.Load):
            vty = vty.structure()
        assert vty is not None, n.value
        #if (isinstance(vty, TypeVariable) and vty.opaque):
            #return ((n, Dyn), Dyn, 2, vsub)
        #    vpat = patternMeet(vpat,  2)
        
        ty = vty        
        if tyinstance(vty, Self):
            try:
                ty = misc.cls.instance().member_type(n.attr)
            except KeyError:
                vpat = 0
            #    if flags.CHECK_ACCESS and not flags.CLOSED_CLASSES and not isinstance(n.ctx, ast.Store):
            #        value, cost = cast(env, misc.cls, value, misc.cls.instance(), Object(misc.cls.name, {n.attr: Dyn}), errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
            #        total_cost += (cost)
            #    ty = Dyn
            #if isinstance(value, ast.Name) and value.id == misc.receiver.id:
            #    if flags.SEMANTICS == 'MONO' and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del) and \
            #            not tyinstance(ty, Dyn):
            #        ans = ast.Call(func=ast.Name(id='retic_getattr_'+('static' if ty.static() else 'dynamic'), 
            #                                     ctx=ast.Load(), lineno=n.lineno),
            #                       args=[value, ast.Str(s=n.attr), ty.to_ast()],
            #                       keywords=[], starargs=None, kwargs=None, lineno=n.lineno)
            #        costObj = total_cost
            #        return ((ans, ty), costObj, ty, True, {})
            #    else:
            #           costObj = total_cost
            #           return ((ast.Attribute(value=value, attr=n.attr, lineno=n.lineno, ctx=n.ctx), ty), costObj, ty, True, {})
            #if isinstance(n.ctx, ast.Store):
            #    costObj = total_cost
            #    return ((ast.Attribute(value=value, attr=n.attr, lineno=n.lineno, ctx=n.ctx), ty), costObj, Dyn, True, {})
            #return ((ast.Call(func=ast.Name(id='retic_bindmethod', ctx=ast.Load()),
            #                args=[ast.Attribute(value=misc.receiver, attr='__class__', ctx=ast.Load()),
            #                      value, ast.Str(s=n.attr)], keywords=[], starargs=None, kwargs=None, 
            #                lineno=n.lineno), \
            #                      ty), total_cost)
        elif tyinstance(vty, Object) or tyinstance(vty, Class):
            try:
                ty = vty.member_type(n.attr)
        #        if isinstance(n.ctx, ast.Del):
        #            return ((error(errmsg('TYPED_ATTR_DELETE', misc.filename, n, n.attr, ty), lineno=n.lineno), Dyn), total_cost, Dyn, False, {})
            except KeyError:
                vpat = 0
        elif tyinstance(vty, TypeVariable):
            ty = unify(vty, Record({n.attr: fresh_tvar()}))
        #        if flags.CHECK_ACCESS and not flags.CLOSED_CLASSES and not isinstance(n.ctx, ast.Store):
        #            value,cost2 = cast(env, misc.cls, value, vty, vty.__class__('', {n.attr: Dyn}), 
        #                         errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
        #            total_cost += (cost2)
        #        ty = Dyn
        #elif tyinstance(vty, Dyn):
        #    if flags.CHECK_ACCESS and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del):
        #        value, cost3 = cast(env, misc.cls, value, vty, Record({n.attr: Dyn}), 
        #                     errmsg('WIDTH_DOWNCAST', misc.filename, n, n.attr), misc=misc)
        #        total_cost += (cost3)
        #    else:                
        #        value, cost4 = cast(env, misc.cls, value, vty, Record({}), 
        #                     errmsg('NON_OBJECT_' + ('WRITE' if isinstance(n.ctx, ast.Store) \
        #                                                 else 'DEL'), misc.filename, n, n.attr), misc=misc)
        #        total_cost += (cost4)
        #    ty = Dyn
        #else:
        #    kind = 'WRITE' if isinstance(n.ctx, ast.Store) else ('DEL' if isinstance(n.ctx, ast.Del) else 'READ')
        #    costObj = total_cost
        #    return ((error(errmsg('NON_OBJECT_' + kind, misc.filename, n, n.attr) % static_val(vty), lineno=n.lineno), Dyn), Dyn, False, {})

        #if flags.SEMANTICS == 'MONO' and not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del) and \
        #        not tyinstance(ty, Dyn):
        #    ans = ast.Call(func=ast.Name(id='retic_getattr_'+('static' if ty.static() else 'dynamic'), 
        #                                 ctx=ast.Load(), lineno=n.lineno),
        #                   args=[value, ast.Str(s=n.attr), ty.to_ast()],
        #                keywords=[], starargs=None, kwargs=None, lineno=n.lineno)
        #    costObj = total_cost
        #    return ((ans, ty), costObj, ty, vpat, vsub)        
        sub = {}
        pat = 2

        pat = patternMeet(pat, vpat)
        sub = compose(sub, vsub)
        #updateEnv(env, sub)
        ans = ast.Attribute(value=value, attr=n.attr, ctx=n.ctx, lineno=n.lineno)
        #if not isinstance(n.ctx, ast.Store) and not isinstance(n.ctx, ast.Del):
        #    ans = check(ans, ty, errmsg('ACCESS_CHECK', misc.filename, n, n.attr, ty), ulval=value)
        #costObj = total_cost
        
        #pdb.set_trace()
        sub_inferred(sub)
        return ((ans, ty), ty, pat, sub)
        

    def visitSubscript(self, n, env, misc):
        ((value, vty), val_ty, vpat, vsub) = self.dispatch(n.value, env, misc)
        if tyinstance(vty, InferBottom):
            sub_inferred(vsub)
            return ((n, Dyn), val_ty, vpat, vsub)
        #pdb.set_trace()
        ((slice, ty), sty, spat, ssub) = self.dispatch(n.slice, env, vty, misc, n.lineno)
        ans = ast.Subscript(value=value, slice=slice, ctx=n.ctx, lineno=n.lineno)
        if not isinstance(n.ctx, ast.Store):
            ans = check(ans, ty, errmsg('SUBSCRIPT_CHECK', misc.filename, n, ty), ulval=value)
            #NOTE: probably need to add cost for check

        
        sub = compose(vsub, ssub)
        pat = patternMeet(vpat, spat)
        #usub, upat, uty = unify(applySub(val_ty, sub), Iterable(fresh_tvar()))
        #sub = compose(sub, usub)
        #pat = patternMeet(pat, upat)
        #ty = applySub(ty, sub)
        sub_inferred(sub)
        return ((ans, ty), ty, pat, sub)

    def visitIndex(self, n, env, extty, misc, lineno):
        #pdb.set_trace()
        ((value, vty),  val_ty, vpat, vsub) = self.dispatch(n.value, env, misc)
        #pdb.set_trace()
        err = errmsg('BAD_INDEX', misc.filename, lineno, extty, Int)
        pat = vpat        
        sub = vsub
        def collection_type(extty):
            if tyinstance(extty, List):
                list_sub, list_pat, list_ty = unify(vty, Int)
                return value, list_sub, list_pat, extty.type
            elif tyinstance(extty, Bytes):
                value_new, _ = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                byte_sub, byte_pat, btye_ty = unify(vty, Int)
                return value_new, byte_sub, byte_pat, Int
            elif tyinstance(extty, String):
                value_new, _ = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                string_sub, string_pat, string_ty = unify(vty, Int)
                return value_new, string_sub, string_pat, String
            elif tyinstance(extty, Tuple):
                value_new, _ = cast(env, misc.cls, value, vty, Int, err, misc=misc)
                tuple_sub, tuple_pat, tuple_ty = unify(vty, Int)
                return value_new, tuple_sub, tuple_pat, Dyn
            elif tyinstance(extty, Dict):
                value_new, _ = cast(env, misc.cls, value, vty, extty.keys, errmsg('BAD_INDEX', misc.filename, lineno, extty, extty.keys), misc=misc)
                dict_sub, dict_pat, dict_ty = unify(vty, extty.keys)
                return value_new, dict_sub, dict_pat, extty.values
            elif tyinstance(extty, Object):
                # Expand
                return value, {}, 2, Dyn
            elif tyinstance(extty, Class):
                # Expand
                return value, {}, 2, Dyn
            elif tyinstance(extty, Dyn):
                return value, {}, 2, Dyn
            elif tyinstance(extty, Choice):
                _, left_sub, left_pat, left_ty = collection_type(selL(extty.name, extty)) #ignore creating variational values because it would break things
                _, right_sub, right_pat, right_ty = collection_type(selR(extty.name, extty))
                merged_sub = mergeSubs(extty.name, left_sub, left_sub, extty.kind)
                merged_pat = makeChoice(extty.name, left_pat, right_pat)
                return value, merged_sub, pat, makeChoice(extty.name, left_ty, right_ty)
            else:
                return value, {}, 2, Dyn
        value, collection_sub, collection_pat,  collection_ty = collection_type(extty)
        sub = compose(collection_sub, sub)
        pat = patternMeet(collection_pat, pat)
        ty = applySub(collection_ty, sub)
        #pdb.set_trace()
        sub_inferred(sub)
        return ((ast.Index(value=value), ty), ty, pat, sub)

    def visitSlice(self, n, env, extty, misc, lineno):
        err = errmsg('BAD_INDEX', misc.filename, lineno, extty, Int)

        ((lower, lty),  loty, lpat, lsub) = self.dispatch(n.lower, env, misc) if n.lower else ((None, Void), Dyn, 2, {})
        ((upper, uty),  upty, upat, usub) = self.dispatch(n.upper, env, misc) if n.upper else ((None, Void),  Dyn, 2, {})
        ((step, sty),  stty, spat, ssub) = self.dispatch(n.step, env, misc) if n.step else ((None, Void),  Dyn, 2, {})
        pat1 = patternMeet(lpat, upat)
        pat = patternMeet(spat, pat1)
        sub1 = compose(lsub, usub)
        pat = patternMeet(spat, pat1)
        sub = compose(ssub, sub1)
        (losub, lopat, _) = unify(loty, Int) if n.lower else ({}, 2, Int) 
        (upsub, uppat, _) = unify(upty, Int) if n.upper else ({}, 2, Int)
        (stsub, stpat, _) = unify(stty, Int) if n.step else ({}, 2, Int )

        sub = compose(sub, losub)
        sub = compose(sub, upsub)
        sub = compose(sub, stsub)

        pat = patternMeet(lopat, pat)
        pat = patternMeet(uppat, pat)
        pat = patternMeet(stpat, pat)

        ty = extty.type if tyinstance(extty, List) else Dyn
        
        sub_inferred(sub)
        return ((ast.Slice(lower=lower, upper=upper, step=step), ty), ty, pat, sub)

    def visitExtSlice(self, n, env, extty, misc, lineno):
        dims = [(dim, ty, pat, sub) for ((dim,_), ty, pat, sub) in [self.dispatch(dim2, env, extty, misc, lineno) for dim2 in n.dims]]
        dims = [x[0] for x in dims]
        pats = [x[2] for x in dims]
        subs = [x[3] for x in dims]
        pat = reduce(lambda a, b: patternMeet(a, b), pats, 2)
        sub = reduce(lambda a, b: compose(a, b), subs, {})
        #NOTE: finish slice
        sub_inferred(sub)
        return ((ast.ExtSlice(dims=dims), Dyn), Dyn, pat, sub)

    def visitEllipsis(self, n, env, *args): 
        #Yes, this looks stupid, but Ellipses are different kinds of things in Python 2 and 3 and if we ever
        #support them meaningfully this distinction witll be crucial
        if flags.PY_VERSION == 2: 
            extty = args[0]
            return ((n, Dyn), Dyn, 2, {})
        elif flags.PY_VERSION == 3:
            return ((n, Dyn), Dyn, 2, {})

    def visitStarred(self, n, env, misc):
        ((value, _),  ty, pat, sub) = self.dispatch(n.value, env, misc)
        sub_inferred(sub)
        return ((ast.Starred(value=value, ctx=n.ctx, lineno=n.lineno), Dyn), ty, pat, sub)

    # Literal stuff
    def visitNum(self, n, env, misc):
        ty = Dyn
        v = n.n
        if type(v) == int or (flags.PY_VERSION == 2 and type(v) == long):
            #ty = Int
            ty = Float
        elif type(v) == float:
            ty = Float
        elif type(v) == complex:
            ty = Complex
        return ((n, ty if flags.TYPED_LITERALS else Dyn), ty, 2, {})

    def visitStr(self, n, env, misc):
        return ((n, String if flags.TYPED_LITERALS else Dyn), String, 2, {})

    def visitBytes(self, n, env, misc):
        return ((n, Bytes if flags.TYPED_LITERALS else Dyn), Bytes, 2, {})
